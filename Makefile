C=clang -Wall -fPIC -Oz
CPP=clang++ -Wall -fPIC -Oz
A=ar
NOLINK=-c
#DEBUG=-g
CFLAGS= $(DEBUG) -std=c99
CPPFLAGS= $(DEBUG) -std=c++17 -static
INCDIR= -I/usr/local/include

B64=b64/cencode.o

PAGES=pages/index.o \
      pages/global.o \
      pages/built.o \
      pages/xwingcgi.o \
      pages/ships.o \
      pages/ship.o \
      pages/pilots.o \
      pages/pilot.o \
      pages/upgrades.o \
      pages/upgrade.o \
      pages/conditions.o \
      pages/condition.o \
      pages/releases.o \
      pages/release.o \
      pages/damagedecks.o \
      pages/damagedeck.o \
      pages/validator.o \
      pages/pagetoken2.o \
      pages/xwingcgi2.o \
      pages/ships2.o \
      pages/ship2.o \
      pages/pilots2.o \
      pages/pilot2.o \
      pages/upgrades2.o \
      pages/upgrade2.o \
      pages/conditions2.o \
      pages/condition2.o \
      pages/environments2.o \
      pages/environment2.o \
      pages/releases2.o \
      pages/release2.o \
      pages/damagedecks2.o \
      pages/damagedeck2.o \
      pages/validator2.o \
      pages/quickbuilds2.o \
      pages/dialcomp.o \
      pages/squad2.o \
      pages/colors2.o \
      pages/search2.o \
      pages/costs2.o \
      pages/timeline2.o \
      pages/remotes2.o \
      pages/ai2.o \
      pages/inventory2.o \
      pages/test2.o

all: .depend xwing.cgi

.depend:
	$(CPP) -E -MM *.cpp > .depend

xwing.cgi: main.cpp libxwing/libxwing.a jsoncpp.o $(B64) $(PAGES)
	$(CPP) $(CPPFLAGS) $(INCDIR) main.cpp -o xwing.cgi jsoncpp.o $(B64) $(PAGES) libxwing/libxwing.a -lxwing2 -L/usr/local/lib -lkcgi -lkcgihtml -lkcgixml -lz
	chmod 711 xwing.cgi

install: xwing.cgi
	doas install -o www -g www -m 0500 xwing.cgi /var/www/cgi-bin
	doas install -o www -g www -m 0500 xwingcgi.css /var/www/htdocs	

clean:
	rm -rf *~ *.o xwing.cgi pages/*~ pages/*.o b64/*~ b64/*.o

.SUFFIXES: .cpp .o
.cpp.o:
	$(CPP) $(CPPFLAGS) $(INCDIR) -c $< -o $@

.SUFFIXES: .c .o
.c.o:
	$(C) $(CFLAGS) $(INCDIR) -c $< -o $@
