#include "pages/index.h"
#include "pages/built.h"
#include "pages/ships.h"
#include "pages/ship.h"
#include "pages/pilots.h"
#include "pages/pilot.h"
#include "pages/upgrades.h"
#include "pages/upgrade.h"
#include "pages/conditions.h"
#include "pages/condition.h"
#include "pages/releases.h"
#include "pages/release.h"
#include "pages/damagedecks.h"
#include "pages/damagedeck.h"
#include "pages/validator.h"
#include "pages/ships2.h"
#include "pages/ship2.h"
#include "pages/pilots2.h"
#include "pages/pilot2.h"
#include "pages/upgrades2.h"
#include "pages/upgrade2.h"
#include "pages/conditions2.h"
#include "pages/condition2.h"
#include "pages/environments2.h"
#include "pages/environment2.h"
#include "pages/releases2.h"
#include "pages/release2.h"
#include "pages/damagedecks2.h"
#include "pages/damagedeck2.h"
#include "pages/validator2.h"
#include "pages/quickbuilds2.h"
#include "pages/dialcomp.h"
#include "pages/squad2.h"
#include "pages/colors2.h"
#include "pages/search2.h"
#include "pages/costs2.h"
#include "pages/timeline2.h"
#include "pages/remotes2.h"
#include "pages/ai2.h"
#include "pages/inventory2.h"
// add new stuff here
#include "pages/test2.h"



enum page {
  PAGE_INDEX,
  PAGE_BUILT,
  PAGE_SHIPS,
  PAGE_SHIP,
  PAGE_PILOTS,
  PAGE_PILOT,
  PAGE_UPGRADES,
  PAGE_UPGRADE,
  PAGE_CONDITIONS,
  PAGE_CONDITION,
  PAGE_RELEASES,
  PAGE_RELEASE,
  PAGE_DAMAGEDECKS,
  PAGE_DAMAGEDECK,
  PAGE_VALIDATOR,
  PAGE_SHIPS2,
  PAGE_SHIP2,
  PAGE_PILOTS2,
  PAGE_PILOT2,
  PAGE_UPGRADES2,
  PAGE_UPGRADE2,
  PAGE_CONDITIONS2,
  PAGE_CONDITION2,
  PAGE_ENVIRONMENTS2,
  PAGE_ENVIRONMENT2,
  PAGE_RELEASES2,
  PAGE_RELEASE2,
  PAGE_DAMAGEDECKS2,
  PAGE_DAMAGEDECK2,
  PAGE_VALIDATOR2,
  PAGE_QUICKBUILDS2,
  PAGE_DIALCOMP,
  PAGE_SQUAD2,
  PAGE_COLORS2,
  PAGE_SEARCH2,
  PAGE_COSTS2,
  PAGE_TIMELINE2,
  PAGE_REMOTES2,
  PAGE_AI2,
  PAGE_INVENTORY2,
  // add new stuff here
  PAGE_TEST2,
  PAGE__MAX
};

enum key {
  KEY_INTEGER,
  KEY_FILE,
  KEY_PAGECOUNT,
  KEY_PAGESIZE,
  KEY__MAX
};

static const struct kvalid keys[KEY__MAX] = {
  { kvalid_int, "integer" }, /* KEY_INTEGER */
  { NULL, "file" }, /* KEY_FILE */
  { kvalid_uint, "count" }, /* KEY_PAGECOUNT */
  { kvalid_uint, "size" }, /* KEY_PAGESIZE */
};

static const char *const pages[PAGE__MAX] = {
  "index",
  "built",
  "ships",
  "ship",
  "pilots",
  "pilot",
  "upgrades",
  "upgrade",
  "conditions",
  "condition",
  "releases",
  "release",
  "damagedecks",
  "damagedeck",
  "validator",
  "ships2",
  "ship2",
  "pilots2",
  "pilot2",
  "upgrades2",
  "upgrade2",
  "conditions2",
  "condition2",
  "environments2",
  "environment2",
  "releases2",
  "release2",
  "damagedecks2",
  "damagedeck2",
  "validator2",
  "quickbuilds2",
  "dialcomp",
  "squad2",
  "colors2",
  "search2",
  "costs2",
  "timeline2",
  "remotes2",
  "ai2",
  "inventory2",
  // add new stuff here
  "test2",
};

typedef void (*disp)(struct kreq *);

static const disp disps[PAGE__MAX] = {
  index,
  built,
  ships,
  ship,
  pilots,
  pilot,
  upgrades,
  upgrade,
  conditions,
  condition,
  releases,
  release,
  damagedecks,
  damagedeck,
  validator,
  ships2,
  ship2,
  pilots2,
  pilot2,
  upgrades2,
  upgrade2,
  conditions2,
  condition2,
  environments2,
  environment2,
  releases2,
  release2,
  damagedecks2,
  damagedeck2,
  validator2,
  quickbuilds2,
  dialcomp,
  squad2,
  colors2,
  search2,
  costs2,
  timeline2,
  remotes2,
  ai2,
  inventory2,
  // add new stuff here
  test2,
};



int main(int argc, char *argv[]) {
  //if (-1 == pledge("stdio", NULL)) {
  //  err(EXIT_FAILURE, "pledge");
  //}

  struct kreq r;

  if(KCGI_OK != khttp_parse(&r, keys, KEY__MAX, pages, PAGE__MAX, PAGE_INDEX)) {
    return EXIT_FAILURE;
  }


  if (KMETHOD_OPTIONS == r.method) {
    /*                                                                                                                                              
     * Indicate that we accept GET and POST methods.                                                                                                
     * This isn't really needed.                                                                                                                    
     */
    khttp_head(&r, kresps[KRESP_STATUS],
	       "%s", khttps[KHTTP_200]);
    khttp_head(&r, kresps[KRESP_ALLOW],
	       "OPTIONS GET POST");
    khttp_body(&r);
  } else if (KMETHOD_GET != r.method &&
	     KMETHOD_POST != r.method) {
    /*                                                                                                                                              
     * Don't accept non-GET and non-POST methods.                                                                                                   
     */
    resp_open(&r, KHTTP_405);
  } else if (PAGE__MAX == r.page ||
	     KMIME_TEXT_HTML != r.mime) {
    /*                                                                                                                                              
     * We've been asked for an unknown page or something                                                                                            
     * with an unknown extension.                                                                                                                   
     */
    resp_open(&r, KHTTP_404);
    khttp_puts(&r, "Page not found.");
  } else {
    /*                                                                                                                                              
     * Route to page handler.                                                                                                                       
     */
    (*disps[r.page])(&r);
  }

  khttp_free(&r);
  return(EXIT_SUCCESS);
}
