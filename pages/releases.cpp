#include "releases.h"

using namespace libxwing;



std::string GetWaveString(RelGroup r) {
  switch(r) {
  case RelGroup::CoreSets: return "Core Set";
  case RelGroup::Epic:     return "Epic";
  case RelGroup::Aces:     return "Aces";
  case RelGroup::Wave1:    return "Wave I";
  case RelGroup::Wave2:    return "Wave II";
  case RelGroup::Wave3:    return "Wave III";
  case RelGroup::Wave4:    return "Wave IV";
  case RelGroup::Wave5:    return "Wave V";
  case RelGroup::Wave6:    return "Wave VI";
  case RelGroup::Wave7:    return "Wave VII";
  case RelGroup::Wave8:    return "Wave VIII";
  case RelGroup::Wave9:    return "Wave IX";
  case RelGroup::Wave10:   return "Wave X";
  case RelGroup::Wave11:   return "Wave XI";
  case RelGroup::Wave12:   return "Wave XII";
  case RelGroup::Wave13:   return "Wave XIII";
  case RelGroup::Wave14:   return "Wave XIV";
  default:                 return "";
  }
}

void releases(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  // print list of releases
  khtml_elem(&r, KELEM_TABLE);
  for(Release rel : Release::GetAllReleases()) {
    khtml_elem(&r, KELEM_TR);

    { // sku 
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
      khtml_puts(&r, rel.GetSku().c_str());
      khtml_closeelem(&r, 1);
    }
    { // wave
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "", KATTR__MAX);
      khtml_puts(&r, GetWaveString(rel.GetGroup()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // expansion name
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "", KATTR__MAX);
      std::string url = std::string(req->pname)+"/release?sku=" + rel.GetSku();
      khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
      if(rel.IsUnreleased()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "unreleased", KATTR__MAX);
	khtml_puts(&r, "[UNRELEASED] ");
	khtml_closeelem(&r, 1); // span
      }
      khtml_puts(&r, rel.GetName().c_str());
      khtml_closeelem(&r, 2);
    }
    khtml_closeelem(&r, 1); // tr
  }
  khtml_closeelem(&r, 1); // tab

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
