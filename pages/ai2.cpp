#include "ai2.h"
#include "pagetoken2.h"
#include <libxwing2/ai.h>

using namespace libxwing2;

void ai2(struct kreq *kr) {

  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    css.Ai();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
    }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));
  /*
  // gather data
  const std::map<Shp,std::vector<AiCard>> data = AiCard::GetAllAiCardsByShip();
  
  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);

  for(const std::pair<const Shp, std::vector<AiCard>> &s : data) {
    bool drawShip = true;
    Shp shp = s.first;
    const std::vector<AiCard> cards = s.second;
    for(const AiCard &aiCard : cards) {
      khtml_elem(&hr, KELEM_TR);
      if(drawShip) {
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(cards.size()).c_str(), KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, Ship::GetShip(shp).GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, ShipGlyph::GetGlyph(shp).c_str());
	khtml_closeelem(&hr, 2); // span td
	drawShip=false;
      }
      { // name
	khtml_elem(&hr, KELEM_TD);
	khtml_puts(&hr, aiCard.GetName().c_str());
	khtml_closeelem(&hr, 1); // td
      }
      { // maneuvers
	khtml_elem(&hr, KELEM_TD);
	std::vector<std::string> segments = aiCard.GetManeuverText().GetMarkedSegments();
	bool first=true;
	for(const std::string &seg : segments) {
	  if(!first) {
	    khtml_elem(&hr, KELEM_BR);
	  }
	  IconifyText(hr, seg.c_str());
	  first = false;
	}
	khtml_closeelem(&hr, 1); // td
      }
      { // actions
	khtml_elem(&hr, KELEM_TD);
	std::vector<AiAction> actions = aiCard.GetActions();
	bool first=true;

	for(const AiAction &aia : actions) {
	  if(!first) {
	    khtml_elem(&hr, KELEM_BR);
	  }

	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "aiAction", KATTR__MAX);
	  khtml_puts(&hr, libxwing2::IconGlyph::GetGlyph(aia.act).c_str());
	  khtml_closeelem(&hr, 1);
	  if(aia.instruction != "") {
	    khtml_puts(&hr, " - ");
	    IconifyText(hr, aia.instruction);
	  }
	  first = false;
	}

	khtml_closeelem(&hr, 1); // td
      }

      khtml_closeelem(&hr, 1); // tr
    }
  }

  khtml_closeelem(&hr, 1); // table
  */
  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
