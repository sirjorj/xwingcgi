#include "built.h"

void built(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_elem(&r, KELEM_HTML);

  // head
  khtml_elem(&r, KELEM_HEAD);
  khtml_attr(&r, KELEM_META, KATTR_CHARSET, "utf-8", KATTR__MAX);
  khtml_elem(&r, KELEM_TITLE); khtml_puts(&r, req->pname); khtml_closeelem(&r, 1);
  khtml_attr(&r, KELEM_LINK, KATTR_REL, "stylesheet", KATTR_TYPE, "text/css", KATTR_HREF, "/xwingcgi.css", KATTR__MAX);
  khtml_closeelem(&r, 1); // head

  // body
  khtml_attr(&r, KELEM_BODY, KATTR_CLASS, "built", KATTR__MAX);
  
  khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "lastbuild", KATTR__MAX);
  khtml_puts(&r, GetBuildDateTimeString().c_str());
  khtml_closeelem(&r, 1);
  
  khtml_closeelem(&r, 1); // body
  khtml_closeelem(&r, 1); // html
  khtml_close(&r);
}
