#include "remotes2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void remotes2(struct kreq *kr) {

  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
    }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));
  /*
  // list of conditions
  khtml_elem(&hr, KELEM_TABLE);

  for(const Remote &remote : Remote::GetAllRemotes()) {
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);

      { // condition name
	khtml_puts(&hr, remote.GetName().c_str());
      }
      khtml_closeelem(&hr, 2); // td tr
  }

  khtml_closeelem(&hr, 1); // table
  */
  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
