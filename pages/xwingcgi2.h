#pragma once
#include "global.h"
#include "pagetoken2.h"
// libxwing
#include <libxwing2/attack.h>
#include <libxwing2/color.h>
#include <libxwing2/condition.h>
#include <libxwing2/converter.h>
#include <libxwing2/damagedeck.h>
#include <libxwing2/environment.h>
#include <libxwing2/glyph.h>
#include <libxwing2/pilot.h>
#include <libxwing2/quickbuild.h>
#include <libxwing2/release.h>
#include <libxwing2/ship.h>
// other
#include <ctime>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

namespace libxwing2 {

std::function<void()> GetPointsListForBreadcrumbs(khtmlreq &hr, PtL ptl, const PageToken& pt);

void PrintManeuvers(struct khtmlreq &hr, Maneuvers maneuvers);

void PrintShipName(       struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Ship &s);
void PrintPilotName(      struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Pilot &p);
void PrintUpgradeName(    struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Upgrade &u);
void PrintConditionName(  struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Condition &c);
void PrintEnvironmentName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Environment &e);
void PrintReleaseName(    struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Release &rel);

void PrintActionBar(                       khtmlreq &hr, const ActionBar& ab);
void PrintUpgradeBar(                      khtmlreq &hr, const UpgradeBar& ub);

void PrintShipRow(        struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Ship& s);
void PrintPilotRow(       struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Pilot& p);
void PrintUpgradeRow(     struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Upgrade& u);

void FactionsTD( struct khtmlreq &hr, std::vector<Fac> f, int sides=1);
void IconifyText(struct khtmlreq &hr, std::string s);

void PrintPilotCard(struct khtmlreq &hr, const PageToken& pt, const Pilot& p, float scale=1.0);
void PrintUpgradeCard(struct khtmlreq &hr, const PageToken& pt, const Upgrade& u, float scale=1.0);

// css
class CSS {
public:
  CSS();

  void MainMenu();
  void DamageDeck();
  void Maneuvers();
  void ShipTitle();
  void ShipRow();
  void PilotRow();
  void UpgradeRow();
  void Pilots();
  void PilotInfo();
  void Upgrades();
  void UpgradeInfo();
  void Releases();
  void ReleaseInfo();
  void QuickBuilds();
  void DialComp();
  void Search();
  void Costs();
  void Inventory();
  void Ai();

  void PilotCard();
  void UpgradeCard();

  std::string GetCSS();
  
private:
  std::stringstream ss;
};

}
