#include "condition2.h"

using namespace libxwing2;

Condition GetCondition(const PageToken& pt) {
  std::string s;
  if     ((s = pt.Get(PageToken::Name)) != "") { return Condition::GetCondition(s); }
  else if((s = pt.Get(PageToken::XWS))  != "") { return Condition::GetCondition(converter::xws::GetCondition(s)); }
  throw ConditionNotFound("");
}

void condition2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  Condition condition = GetCondition(pt);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + condition.GetName() + "]", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			    {LinkBuilder(pt,"conditions2").AddArg(PageToken::PointList).Get(), "conditions2"}
    }, condition.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

  khtml_elem(&hr, KELEM_TABLE);

  { // name
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Name"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, condition.GetName().c_str()); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
  }
  { // text
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Text"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TD, KATTR__MAX); IconifyText(hr, condition.GetText()); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
  }
  { // availability
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Availability"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
    bool first=true;
    for(Rel rel : Release::GetAllRels()) {
      Release release = Release::GetRelease(rel);
      for(Cnd c : release.GetConditions()) {
	if(c == condition.GetCnd()) {
	  if(first) { first = false; }
	  else      { khtml_elem(&hr, KELEM_BR); }
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,release.GetSku()).Get().c_str(), KATTR__MAX);
	  khtml_puts(&hr, release.GetName().c_str());
	  khtml_closeelem(&hr, 1);
	  break;
	}
      }
    }
    khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 3); // tr, table, body
  khtml_close(&hr);
}
