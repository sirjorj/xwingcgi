#include "costs2.h"
#include "pagetoken2.h"
#include "../json/json.h"
#include <libxwing2/pointlist.h>

using namespace libxwing2;

void costs2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format=pt.Get(PageToken::Format);

  if(format == "") {
    struct khtmlreq hr;

    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      css.Costs();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    const std::vector<PtL> ptls = PointList::GetPtLs();

    { // pilots
      std::map<Shp,std::vector<Pilot>> data;
      for(Plt plt : Pilot::GetAllPlts()) {
	Pilot pilot = Pilot::GetPilot(plt);
	data[pilot.GetShp()].push_back(pilot);
      }
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "noWrap", KATTR__MAX);
      { // header
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	  khtml_puts(&hr, PointList::GetName(ptl).c_str());
	  khtml_closeelem(&hr, 1);
	}
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      { // announce dates
	khtml_elem(&hr, KELEM_TR);
	//khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  //khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  char date[22];
	  PLDate ad = PointList::GetAnnounceDate(ptl);
	  snprintf(date, sizeof(date), "Announced: %04d/%02d/%02d", ad.year, ad.month, ad.date);
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	  khtml_puts(&hr, date);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }
      { // effect dates
	khtml_elem(&hr, KELEM_TR);
	//khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  //khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  char date[22];
	  PLDate ed = PointList::GetEffectDate(ptl);
	  snprintf(date, sizeof(date), "Effective: %04d/%02d/%02d", ed.year, ed.month, ed.date);
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	  khtml_puts(&hr, date);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }
      
      // data
      for(const std::pair<const Shp,std::vector<Pilot>> &d1 : data) {
	bool drawShip=true;
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TH, KATTR_CLASS, "vpad", KATTR_COLSPAN, std::to_string(3+(PointList::GetPtLs().size()*4)+2).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1); // padding	
	khtml_closeelem(&hr, 1);
	for(const Pilot& p : d1.second) {
	  khtml_elem(&hr, KELEM_TR);
	  if(drawShip) {
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(d1.second.size()).c_str(), KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, Ship::GetShip(d1.first).GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, ShipGlyph::GetGlyph(d1.first).c_str());
	    khtml_closeelem(&hr, 2); // span td
	  }
	  // pilot name
	  khtml_elem(&hr, KELEM_TD); PrintPilotName(kr, hr, pt, p); khtml_closeelem(&hr, 1);
	  // values
	  std::optional<bool> lastHyp;
	  std::optional<Cost> lastCost;
	  std::optional<UpgradeBar> lastUb;
	  for(PtL ptl : ptls) {
	    { // padding
	      khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_closeelem(&hr, 1);
	    }
	    { // hyperspace
	      std::optional<bool> hyp = PointList::GetHyperspace(p.GetPlt(), ptl);
	      std::string css;
	      if(hyp) {
		if(lastHyp && (*lastHyp != *hyp)) {
		  if(*hyp) { css = "wentDown"; }
		  else     { css = "wentUp"; }
		}
	      }
	      lastHyp = hyp;
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	      khtml_puts(&hr, hyp ? (*hyp ? "Yes" : "No") : "");
	      khtml_closeelem(&hr, 1);
	    }
	    { // cost
	      std::optional<Cost> cost = PointList::GetCost(p.GetPlt(), ptl);
	      std::string css;
	      if(cost) {
		if(lastCost && !(*lastCost == *cost)) {
		  if(*cost < *lastCost) { css = "wentDown"; }
		  else                  { css = "wentUp"; }
		}
	      }
	      lastCost = cost;
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	      khtml_puts(&hr, cost ? std::to_string(cost->GetCosts()[0]).c_str() : "");
	      khtml_closeelem(&hr, 1);
	    }
	    { // upgrades
	      std::optional<UpgradeBar> ub = PointList::GetUpgradeBar(p.GetPlt(), ptl);
	      std::string css;
	      if(ub) {
		if(lastUb) {
		  if(ub->size() < lastUb->size()) { css = "wentUp"; }
		  else if(*ub != *lastUb)         { css = "wentDown"; }
		}
	      }
	      lastUb = ub;
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	      if(ub) {
		for(const UpgradeSlot& s : *ub) {
		  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgrade", KATTR_TITLE, UpgradeType::GetUpgradeType(s.GetTypes()[0]).GetName().c_str(), KATTR__MAX);
		  khtml_puts(&hr, IconGlyph::GetGlyph(s.GetTypes()[0]).c_str());
		  khtml_closeelem(&hr, 1); // span
		}
	      }
	      khtml_closeelem(&hr, 1);
	    }
    	  }
	  { // padding
	    khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_closeelem(&hr, 1);
	  }
	  // pilot name (again)
	  khtml_elem(&hr, KELEM_TD); PrintPilotName(kr, hr, pt, p); khtml_closeelem(&hr, 1);
	  // ship icon (again)
	  if(drawShip) {
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(d1.second.size()).c_str(), KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, Ship::GetShip(d1.first).GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, ShipGlyph::GetGlyph(d1.first).c_str());
	    khtml_closeelem(&hr, 2); // span td
	    drawShip=false;
	  }
  	  khtml_closeelem(&hr, 1); // tr
	}
      }
      khtml_closeelem(&hr, 1); // table
    }

    { // upgrades
      std::map<UpT,std::vector<Upgrade>> data;
      for(Upg upg : Upgrade::GetAllUpgs()) {
	Upgrade upgrade = Upgrade::GetUpgrade(upg);
	data[upgrade.GetUpT()].push_back(upgrade);
      }
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "nowrap", KATTR__MAX);
      { // header
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	  khtml_puts(&hr, PointList::GetName(ptl).c_str());
	  khtml_closeelem(&hr, 1);
	}
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pad", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR_ROWSPAN, "3", KATTR__MAX); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      { // announce dates
	khtml_elem(&hr, KELEM_TR);
	//khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  //khtml_attr(&hr, KELEM_TH, KATTR_CLASS, "pad", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  char date[22];
	  PLDate ad = PointList::GetAnnounceDate(ptl);
	  snprintf(date, sizeof(date), "Announced: %04d/%02d/%02d", ad.year, ad.month, ad.date);
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	  khtml_puts(&hr, date);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }
      { // effect dates
	khtml_elem(&hr, KELEM_TR);
	//khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_closeelem(&hr, 1);
	for(PtL ptl : ptls) {
	  //khtml_attr(&hr, KELEM_TH, KATTR_CLASS, "pad", KATTR__MAX); khtml_closeelem(&hr, 1); // padding
	  char date[22];
	  PLDate ed = PointList::GetEffectDate(ptl);
	  snprintf(date, sizeof(date), "Effective: %04d/%02d/%02d", ed.year, ed.month, ed.date);
	  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	  khtml_puts(&hr, date);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }

      // data
      for(const std::pair<const UpT,std::vector<Upgrade>> &d1 : data) {
	bool drawType=true;
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TH, KATTR_CLASS, "vpad", KATTR_COLSPAN, std::to_string(3+(PointList::GetPtLs().size()*3)+2).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1); // padding	
	khtml_closeelem(&hr, 1);
	for(const Upgrade& u : d1.second) {
	  khtml_elem(&hr, KELEM_TR);
	  if(drawType) {
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "upgHeader", KATTR_ROWSPAN, std::to_string(d1.second.size()).c_str(), KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, u.GetUpgradeType().GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, IconGlyph::GetGlyph(u.GetUpT()).c_str());
	    khtml_closeelem(&hr, 2); // span td
	  }
	  // upgrade name
	  khtml_elem(&hr, KELEM_TD); PrintUpgradeName(kr, hr, pt, u); khtml_closeelem(&hr, 1);
	  // values
	  std::optional<bool> lastHyp;
	  std::optional<Cost> lastCost;
	  for(PtL ptl : ptls) {
	    { // padding
	      khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_closeelem(&hr, 1);
	    }
	    { // hyperspace
	      std::optional<bool> hyp = PointList::GetHyperspace(u.GetUpg(), ptl);
	      std::string css;
	      std::string hypStr;
	      if(hyp) {
		if(lastHyp && (*lastHyp != *hyp)) {
		  if(*hyp) { css = "wentDown"; }
		  else     { css = "wentUp"; }
		}
		hypStr = *hyp ? "Yes" : "No";
	      }
	      lastHyp = hyp;
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	      khtml_puts(&hr, hypStr.c_str());
	      khtml_closeelem(&hr, 1);
	    }
	    { // cost
	      std::optional<Cost> cost = PointList::GetCost(u.GetUpg(), ptl);
	      std::string css;
	      std::string costStr;
	      if(cost) {
		if(lastCost && (*lastCost != *cost)) {
		  if(*cost < *lastCost) { css = "wentDown"; }
		  else                  { css = "wentUp"; }
		}
		costStr = cost->GetCostStr();
	      }
	      lastCost = cost;
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	      khtml_puts(&hr, costStr.c_str());
	      khtml_closeelem(&hr, 1);
	    }
	  } // for ptl
	  { // padding
	    khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_closeelem(&hr, 1);
	  }
	  // upgrade name
	  khtml_elem(&hr, KELEM_TD); PrintUpgradeName(kr, hr, pt, u); khtml_closeelem(&hr, 1);
	  // upgradetype icon (again)
	  if(drawType) {
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "upgHeader", KATTR_ROWSPAN, std::to_string(d1.second.size()).c_str(), KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, u.GetUpgradeType().GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, IconGlyph::GetGlyph(u.GetUpT()).c_str());
	    khtml_closeelem(&hr, 2); // span td
	    drawType=false;
	  }
	  khtml_closeelem(&hr, 1); // tr
	}
      }
      khtml_closeelem(&hr, 1); // table
    }

    /*
    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      { // json
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TD);
	khtml_puts(&hr, "[");
	khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"costs2").AddArg(PageToken::PointList).AddArg(PageToken::FMT,"json").Get().c_str(), KATTR__MAX);
	khtml_puts(&hr, "json");
	khtml_closeelem(&hr, 1); // a
	khtml_puts(&hr, "]");
	khtml_closeelem(&hr, 2); // td tr
      }
      khtml_closeelem(&hr, 1); // table
    }
    */
    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else if(format == "json") {
    int plc=0;
    Json::Value pLists;
    const std::vector<PtL> ptls = PointList::GetPtLs();

    //const std::vector<PointList> pls = libxwing2::PointList::GetAllPointLists();
    //for(const PointList& pl : pls) {
    for(PtL ptl : ptls) {
      pLists[plc]["name"] = PointList::GetName(ptl);
      {
	PLDate ad = PointList::GetAnnounceDate(ptl);
	pLists[plc]["announceDate"]["year"]  = ad.year;
	pLists[plc]["announceDate"]["month"] = ad.month;
	pLists[plc]["announceDate"]["date"]  = ad.date;
      }
      {
	PLDate ed = PointList::GetEffectDate(ptl);
	pLists[plc]["effectDate"]["year"]  = ed.year;
	pLists[plc]["effectDate"]["month"] = ed.month;
	pLists[plc]["effectDate"]["date"]  = ed.date;
      }
      { // pilots
	const std::vector<Plt> plts = Pilot::GetAllPlts();
	int pc = 0;
	for(Plt plt : plts) {
	  Pilot pilot = Pilot::GetPilot(plt);
	  std::optional<Cost> c = PointList::GetCost(plt, ptl);
	  std::optional<bool> h = PointList::GetHyperspace(plt, ptl);
	  std::optional<UpgradeBar> u = PointList::GetUpgradeBar(plt, ptl);
	  std::optional<std::string> x = converter::xws::GetPilot(plt);
	  if(c && h && u && x) {
	    pLists[plc]["pilot"][pc]["name"] = pilot.GetName();
	    pLists[plc]["pilot"][pc]["xws"] = *x;
	    pLists[plc]["pilot"][pc]["cost"] = c->GetCostStr();
	    pLists[plc]["pilot"][pc]["hyperspace"] = *h ? "yes" : "no";
	    int uc = 0;
	    for(const UpgradeSlot& us : *u) {
	      pLists[plc]["pilot"][pc]["upgradebar"][uc] = UpgradeType::GetUpgradeType(us.GetTypes()[0]).GetName();
	      uc++;
	    }
	    pc++;
	  }
	}
      }
      { // upgrades
	const std::vector<Upg> upgs = Upgrade::GetAllUpgs();
	int uc = 0;
	for(Upg upg : upgs) {
	  Upgrade upgrade = Upgrade::GetUpgrade(upg);
	  std::optional<Cost> c = PointList::GetCost(upg, ptl);
	  std::optional<bool> h = PointList::GetHyperspace(upg, ptl);
	  std::optional<std::string> x = converter::xws::GetUpgrade(upg);
	  if(c && h && x) {
	    pLists[plc]["upgrade"][uc]["name"] = upgrade.GetName();
	    pLists[plc]["upgrade"][uc]["xws"] = *x;
	    pLists[plc]["upgrade"][uc]["cost"] = c->GetCostStr();
	    pLists[plc]["upgrade"][uc]["hyperspace"] = *h ? "yes" : "no";
	    uc++;
	  }
	}
      }
      plc++;
    }

    std::string jsonStr;
    std::stringstream ss;
    ss << pLists;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }
}
