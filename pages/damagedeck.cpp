#include "damagedeck.h"

using namespace libxwing;



void damagedeck(struct kreq *req) {
  struct khtmlreq  r;

  std::string ddstr;
  for(int i=0; i<req->fieldsz; i++) {
    if(strcmp(req->fields[i].key, "dd") == 0) {
      ddstr = req->fields[i].val;
    }
  }

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  try {
    Deck dd = Deck::GetDeck(ddstr);
    // head
    PrintHead(req, r, std::string(req->pname) + req->fullpath + " [" + dd.GetName() + "]");

    // body
    khtml_elem(&r, KELEM_BODY);
    PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}, {std::string(req->pname)+"/damagedecks", "damagedecks"}}, dd.GetName());

    khtml_elem(&r, KELEM_TABLE);

    for(DamageCard dc : DamageCard::GetAllDamageCards(dd.GetType())) {
      khtml_elem(&r, KELEM_TR);

      { // name
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
	khtml_puts(&r, dc.GetName().c_str());
	khtml_closeelem(&r, 1); // td
      }
      { // trait
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "text", KATTR__MAX);
	khtml_puts(&r, dc.GetTrait().GetName().c_str());
	khtml_closeelem(&r, 1); // td
      }
      { // text
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "text", KATTR__MAX);
	IconifyText(r, dc.GetText());
	khtml_closeelem(&r, 1); // td
      }
      
      khtml_closeelem(&r, 1); // tr
    }
  }
  catch(DeckNotFound dnf) {
    khtml_puts(&r, dnf.what());
  }

  khtml_closeelem(&r, 1); // tab

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
