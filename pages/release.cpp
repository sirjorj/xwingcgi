#include "release.h"

using namespace libxwing;



void release(struct kreq *req) {
  struct khtmlreq  r;

  std::string skustr;
  for(int i=0; i<req->fieldsz; i++) {
    if(strcmp(req->fields[i].key, "sku") == 0) {
      skustr = req->fields[i].val;
    }
  }

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  try {
    Release release = Release::GetRelease(skustr);

    // head
    PrintHead(req, r, std::string(req->pname) + req->fullpath + " [" + release.GetName() + "]");

    // body
    khtml_elem(&r, KELEM_BODY);
    PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}, {std::string(req->pname)+"/releases", "releases"}}, release.GetName());

    { // header
      khtml_elem(&r, KELEM_DIV);
      { // name
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "HeaderText", KATTR__MAX);
	khtml_puts(&r, release.GetName().c_str());
	khtml_closeelem(&r, 1);
      }
       khtml_closeelem(&r, 1);
    }
    // info
    khtml_elem(&r, KELEM_TABLE);
    { // sku
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "SKU"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, release.GetSku().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // isbn
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "ISBN"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, release.GetIsbn().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // announced
      char date[11];
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Announced"); khtml_closeelem(&r, 1);
      snprintf(date, sizeof(date), "%04d.%02d.%02d", release.GetAnnounceDate().year, release.GetAnnounceDate().month, release.GetAnnounceDate().date);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, date); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // released
      char date[11];
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Released"); khtml_closeelem(&r, 1);
      snprintf(date, sizeof(date), "%04d.%02d.%02d", release.GetReleaseDate().year, release.GetReleaseDate().month, release.GetReleaseDate().date);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, date); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // articles
      std::vector<std::pair<std::string, std::string>> urls;
      if(release.GetAnnouncementUrl() != "")         { urls.push_back({"Announcement", release.GetAnnouncementUrl()}); }
      for(std::string pv : release.GetPreviewUrls()) { urls.push_back({"Preview", pv}); }
      if(release.GetReleaseUrl() != "")              { urls.push_back({"Release", release.GetReleaseUrl()}); }
      bool printedLabel = false;
      for(auto url : urls) {
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(urls.size()).c_str(), KATTR__MAX);
	  khtml_puts(&r, "Articles");
	  khtml_closeelem(&r, 1);
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	khtml_attr(&r, KELEM_A, KATTR_HREF, url.second.c_str(), KATTR__MAX);
	khtml_puts(&r, url.first.c_str());
	khtml_closeelem(&r, 3);
      }
    }
    { // ships
      bool printedLabel = false;
      for(auto sh : release.GetShips()) {
	Ship ship = Ship::GetShip(sh.ship);
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetShips().size()).c_str(), KATTR__MAX);
	  khtml_puts(&r,release.GetShips().size() > 1 ? "Ships" : "Ship");
	  khtml_closeelem(&r, 1);
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	std::string url = std::string(req->pname)+"/ship?ship=" + ship.GetXws();
	khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	khtml_puts(&r, ship.GetName().c_str());
	khtml_closeelem(&r, 3);
      }
    }
    { // pilots
      bool printedLabel = false;
      for(RelPilot rp : release.GetPilots()) {
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetPilots().size()).c_str(), KATTR__MAX);
	  khtml_puts(&r, "Pilots");
	  khtml_closeelem(&r, 1); // td (label)
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	try {
	  Pilot pilot = Pilot::GetPilot(rp.name, Faction::GetFaction(rp.faction).GetXws(), Ship::GetShip(rp.ship).GetXws());
	  std::string url = std::string(req->pname)+"/pilot?name=" + pilot.GetXws() + "&faction=" + pilot.GetFaction().GetXws() + "&ship=" + pilot.GetShip().GetXws();
	  khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	  if(pilot.IsUnique()) {
	    khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
	  }
	  khtml_puts(&r, pilot.GetName().c_str());
	  khtml_closeelem(&r, 2);
	}
	catch(PilotNotFound pnf) {
	  PrintError(r, pnf.what());
	  khtml_closeelem(&r, 1);
	}
	khtml_closeelem(&r, 1); // tr
      }
    }
    { // upgrades
      bool printedLabel = false;
      for(RelUpgrade ru : release.GetUpgrades()) {
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetUpgrades().size()).c_str(), KATTR__MAX);
	  khtml_puts(&r, "Upgrades");
	  khtml_closeelem(&r, 1);
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	try {
	  Upgrade upgrade = Upgrade::GetUpgrade(UpgradeType::GetUpgradeType(ru.type).GetXws(), ru.name);
	  std::string url = std::string(req->pname)+"/upgrade?type=" + upgrade.GetType().GetXws() + "&name=" + upgrade.GetXws();
	  khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	  if(upgrade.IsUnique()) {
	    khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
	  }
	  khtml_puts(&r, upgrade.GetName().c_str());
	  khtml_closeelem(&r, 2);
	}
	catch (UpgradeNotFound unf) {
	  PrintError(r, unf.what());
	  khtml_closeelem(&r, 1);
	}
	khtml_closeelem(&r, 1); // tr
      }
    }
    { // conditions
      bool printedLabel = false;
      for(Cnd c : release.GetConditions()) {
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetConditions().size()).c_str(), KATTR__MAX);
	  khtml_puts(&r, release.GetConditions().size() > 1 ? "Conditions" : "Condition");
	  khtml_closeelem(&r, 1);
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	try {
	  Condition condition = Condition::GetCondition(c);
	  std::string url = std::string(req->pname)+"/condition?name=" + condition.GetXws();
	  khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	  khtml_puts(&r, condition.GetName().c_str());
	  khtml_closeelem(&r, 1);
	}
	catch(ConditionNotFound cnf) {
	  PrintError(r, cnf.what());
	  khtml_closeelem(&r, 1);
	}
      }
    }
    { // tokens
      bool printedLabel = false;
      for(auto tt : release.GetTokens().GetTokenTypes()) {
	khtml_elem(&r, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetTokens().GetTokenTypes().size()).c_str(), KATTR__MAX);
	  khtml_puts(&r, "Tokens");
	  khtml_closeelem(&r, 1); // a
	  printedLabel = true;
	}
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	std::string tokStr = "x" + std::to_string(release.GetTokens().GetTokenCount(tt)) + " " + Token::GetToken(tt).GetName();
	if(tt == Tok::TargetLock) {
	  tokStr += " (";
	  bool first = true;
	  for(auto tl : release.GetTokens().GetTargetLockTokens()) {
	    if(!first) { tokStr += ", "; }
	    tokStr += tl.first + "/" + tl.second;
	    first = false;
	  }
	  tokStr += ")";
	}
	else if(tt == Tok::ID) {
	  tokStr += " (";
	  bool first = true;
	  for(auto id : release.GetTokens().GetIdTokens()) {
	    if(!first) { tokStr += ", "; }
	    tokStr += std::to_string(id);
	    first = false;
	  }
	  tokStr += ")";
	}
	khtml_puts(&r, tokStr.c_str());
	khtml_closeelem(&r, 2); // td tr
      }
    }
  }
  catch(ReleaseNotFound rnf) {
    khtml_puts(&r, rnf.what());
  }

  khtml_closeelem(&r, 1); // table
  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
