#include "upgrades2.h"
#include "pagetoken2.h"
#include "../json/json.h"
#include <iostream>
#include <iomanip>

using namespace libxwing2;

static std::vector<Upgrade> GetUpgrades(const std::string& restriction) {
  std::vector<Upgrade> data;
  for(Upg upg : Upgrade::GetAllUpgs()) {
    Upgrade upgrade = Upgrade::GetUpgrade(upg);
    if(restriction == "hyperspace") {
      if((upgrade.GetHyperspace() == std::nullopt) || (*upgrade.GetHyperspace() == false)) {
	continue;
      }
    }
    data.push_back(upgrade);
  }
  return data;
}

static std::string CSV(std::string txt) {
  std::stringstream ss;
  bool enclose = false;
  for(const char c : txt) {
    if(c == ',') {
      enclose = true;
    }
    if(c == '"') {
      enclose = true;
      ss << '"' << '"';
    } else {
      ss << c;
    }
  }
  return (enclose ? "\"" : "") + ss.str() + (enclose ? "\"" : "");
}

static std::string GetCost(const std::optional<Cost>& cost) {
  std::stringstream ss;
  if(cost) {
    switch(cost->GetCsT()) {
    // none
    case CsT::Const: {
      ss << std::to_string(cost->GetCosts()[0]);
      break;
    }
    // base size
    case CsT::Base: {
      std::vector<std::string> bs = { "S:", "M:", "L:", "H:" };
      int i=0;
      bool first = true;
      for(uint16_t c : cost->GetCosts()) {
	if(first) { first = false; }
	else      { ss << " "; }
	ss << bs[i] << std::to_string(c);
	i++;
      }
      break;
    }
    // agi
    case CsT::Agi: {
      std::vector<std::string> agi = { "0", "1", "2", "3" };
      int i=0;
      bool first = true;
      for(uint16_t c : cost->GetCosts()) {
	if(first) { first = false; }
	else      { ss << " "; }
	ss << agi[i] << ":" << std::to_string(c);
	i++;
      }
      break;
    }
    // init
    case CsT::Init: {
      std::vector<std::string> init = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
      int i=0;
      bool first = true;
      for(uint16_t c : cost->GetCosts()) {
	if(first) { first = false; }
	else      { ss << " "; }
	ss << init[i] << ":" << std::to_string(c);
	i++;
      }
      break;
    }
    }
  }
  return ss.str();
}

static std::string GetHyperspace(const std::optional<bool>& hyp) {
  std::stringstream ss;
  if(hyp) {
    ss << (*hyp ? "Yes" : "No");
  }
  return ss.str();
}

static std::string GetSlots(const std::vector<UpT> &upts) {
  std::stringstream ss;
  bool first = true;
  for(UpT upt : upts) {
    if(first) { first = false; }
    else      { ss << ", "; }
    ss << UpgradeType::GetUpgradeType(upt).GetName();
  }
  return ss.str();
}

static std::string GetChargeable(const Chargeable ch) {
  std::stringstream ss;
  if(ch.GetCapacity()) {
    ss << std::to_string(ch.GetCapacity());
    for(int r=0; r<ch.GetRecurring(); r++) {
      ss << "^";
    }
  }
  return ss.str();
}

static std::string GetAttack(const SecAttack &at) {
  std::stringstream ss;
  ss << FiringArc::GetFiringArc(at.arc).GetName();
  ss << " " << std::to_string(at.value);
  if(at.minRange == at.maxRange) {
    ss << " (" << std::to_string(at.minRange) << ")";
  } else {
    ss << " (" << std::to_string(at.minRange) << "-" << std::to_string(at.maxRange) << ")";
  }
  if(at.ordnance) {
    ss << " [ORD]";
  }
  return ss.str();
}

static std::string GetModifiers(const Modifier &mod) {
  std::stringstream ss;
  const std::string sep = " ";
  bool empty=true;
  // actions
  if(mod.GetActAdd().size()) {
    if(!empty) { ss << sep; } empty = false;
    bool first = true;
    ss << "{Actions:";
    for(auto a : mod.GetActAdd()) {
      if(first) { first = false; }
      else      { ss << " "; }
      bool next = false;
      ss << "[";
      for(SAct sa : a) {
	if(next) {
	  ss << ">";
	}
	std::string dif;
	switch(sa.difficulty) {
	case Dif::White:  dif = "(W)"; break;
	case Dif::Red:    dif = "(R)"; break;
	case Dif::Purple: dif = "(P)"; break;
	default:          dif = "";  break;
	}
	ss << dif << Action::GetAction(sa.action).GetName();
	next = true;
      }
      ss << "]";
    }
    ss << "}";
  }
  // agility
  if(mod.GetAgility()) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{Agility:" << ((mod.GetAgility()>0) ? "+":"") << std::to_string(mod.GetAgility()) << "}";
  }
  // attack mod
  if(mod.GetAttackMod().val) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{AttackMod:" << FiringArc::GetFiringArc(mod.GetAttackMod().arc).GetName() << " " << ((mod.GetAttackMod().val>0) ? "+":"") << std::to_string(mod.GetAttackMod().val) << "}";
  }
  // Energy
  if(mod.GetEnergy()) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{Energy:" << ((mod.GetEnergy()>0) ? "+":"") << std::to_string(mod.GetEnergy()) << "}";
  }
  // Force
  if(mod.GetForceAf() != FAf::None) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{Force:" << ForceAffiliation::GetForceAffiliation(mod.GetForceAf()).GetName() << "}";
  }
  // Hull
  if(mod.GetHull()) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{Hull:" << ((mod.GetHull()>0) ? "+":"") << std::to_string(mod.GetHull()) << "}";
  }
  // Shield
  if(mod.GetShield()) {
    if(!empty) { ss << sep; } empty = false;
    ss << "{Shield:" << ((mod.GetShield()>0) ? "+":"") << std::to_string(mod.GetShield()) << "}";
  }
  return ss.str();
}



void upgrades2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format=pt.Get(PageToken::Format);
  std::string restriction=pt.Get(PageToken::Restriction);

  if(format == "") {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_elem(&hr, KELEM_HTML);

    { // head
      CSS css;
      css.Upgrades();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

    // get types sorted from fewest to most
    std::vector<UpgradeType> upTypes;
    std::map<UpT, uint16_t> uptc;
    for(Upg upg : Upgrade::GetAllUpgs()) {
      Upgrade upgrade = Upgrade::GetUpgrade(upg);
      if(restriction=="hyperspace") {
	if((upgrade.GetHyperspace() == std::nullopt) || (*upgrade.GetHyperspace() == false)) {
      	  continue;
      	}
      }
      uptc[upgrade.GetUpT()]++;
    }
    std::vector<std::pair<UpT,uint16_t>> uptcv;
    for(const std::pair<const UpT,uint16_t> &p : uptc) {
      uptcv.push_back(p);
    }
    std::sort(uptcv.begin(), uptcv.end(),
	      [](std::pair<UpT,uint16_t> const &a, std::pair<UpT,uint16_t> const &b) {
		if(a.second == b.second) {
		  return UpgradeType::GetUpgradeType(a.first).GetName() < UpgradeType::GetUpgradeType(b.first).GetName();
		}
		else {
		  return a.second < b.second;
		}
	      });
    for(const std::pair<UpT,uint16_t> &p : uptcv) {
      upTypes.push_back(UpgradeType::GetUpgradeType(p.first));
    }

    // list of upgrades
    for(const UpgradeType& ut : upTypes) {
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
      { // upgrade type
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "upgHeader", KATTR_COLSPAN, "2", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ut.GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(ut.GetUpT()).c_str());
	khtml_closeelem(&hr, 3); // span td tr
      }

      for(Upg upg : Upgrade::GetAllUpgs()) {
	Upgrade upgrade = Upgrade::GetUpgrade(upg);
	if(restriction=="hyperspace") {
	  if((upgrade.GetHyperspace() == std::nullopt) || (*upgrade.GetHyperspace() == false)) {
	    continue;
	  }
	}

	if(upgrade.GetUpT() == ut.GetUpT()) {
	  /* this is the old xws-based code
	  // upgrade name
	  std::string xu;
	  try{
	    xu = xws::upgrade::GetUpgrade(u.GetUpg());
	  }
	  catch(UpgradeNotFound &unf) {
	    khtml_puts(&hr, std::string("CANNOT FIND UPGRADE" + std::to_string((int)u.GetUpg())).c_str());
	    continue;
	  }
	  catch(std::runtime_error &re) {
	    khtml_puts(&hr, (std::string("error: ") + re.what()).c_str());
	  }
	  catch(...) {
	    khtml_puts(&hr, "WTF?");
	    continue;
	  }

	  //std::string uUrl = std::string(kr->pname)+"/upgrade2?upgrade=" + xu;
	  */
	  khtml_attr(&hr, KELEM_TR, KATTR_CLASS, upgrade.IsUnreleased() ? "unr" :  "", KATTR__MAX);

	  // faction
	  std::vector<Fac> facs = upgrade.GetRestriction().GetAllowedFactions();
	  FactionsTD(hr, facs);

	  // pilot name
	  khtml_elem(&hr, KELEM_TD);
	  PrintUpgradeName(kr, hr, pt, upgrade);
	  khtml_closeelem(&hr, 2); // td tr
	}
      }
      khtml_closeelem(&hr, 2); // table div[floater]
    }

    khtml_closeelem(&hr, 2); // div[container]

    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "Formats"); khtml_closeelem(&hr, 2);
      { // web
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "web"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "upgrades2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // text
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "text"); khtml_closeelem(&hr, 1);
	}
	{
   	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"text").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"text").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // csv
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "csv"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"csv").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"csv").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // json
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "json"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      khtml_closeelem(&hr, 1); // table
    }

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else if(format == "json") {
    int uc=0;
    Json::Value jUpgrades;
    std::vector<Upgrade> upgrades = GetUpgrades(restriction);

    for(Upgrade upgrade : upgrades) {
      jUpgrades[uc]["name"] = upgrade.GetName();
      {
	std::string xws;
	try {
	  xws = converter::xws::GetUpgrade(upgrade.GetUpg());
	}
	catch (converter::xws::UpgradeNotFound &unf) {
	  xws = "???";
	}
	jUpgrades[uc]["xws"] = xws;
      }

      jUpgrades[uc]["limited"] = upgrade.GetLimited();
      std::optional<Cost> c = upgrade.GetCost();
      if(c) {
 	switch(c->GetCsT()) {
	case CsT::Const: {
	  jUpgrades[uc]["cost"]["variable"] = "None";
	  jUpgrades[uc]["cost"]["value"] = c->GetCosts()[0];
	  break;
	}
 	case CsT::Base: {
	  jUpgrades[uc]["cost"]["variable"] = "BaseSize";
	  jUpgrades[uc]["cost"]["small"]    = c->GetCosts()[0];
	  jUpgrades[uc]["cost"]["medium"]   = c->GetCosts()[1];
	  jUpgrades[uc]["cost"]["large"]    = c->GetCosts()[2];
	  break;
	}
	case CsT::Agi: {
	  jUpgrades[uc]["cost"]["variable"] = "Agility";
	  jUpgrades[uc]["cost"]["agi0"]     = c->GetCosts()[0];
	  jUpgrades[uc]["cost"]["agi1"]     = c->GetCosts()[1];
	  jUpgrades[uc]["cost"]["agi2"]     = c->GetCosts()[2];
	  jUpgrades[uc]["cost"]["agi3"]     = c->GetCosts()[3];
	  break;
	}
	case CsT::Init: {
	  jUpgrades[uc]["cost"]["variable"] = "initiative";
	  jUpgrades[uc]["cost"]["0"]        = c->GetCosts()[0];
	  jUpgrades[uc]["cost"]["1"]        = c->GetCosts()[1];
	  jUpgrades[uc]["cost"]["2"]        = c->GetCosts()[2];
	  jUpgrades[uc]["cost"]["3"]        = c->GetCosts()[3];
	  jUpgrades[uc]["cost"]["4"]        = c->GetCosts()[4];
	  jUpgrades[uc]["cost"]["5"]        = c->GetCosts()[5];
	  jUpgrades[uc]["cost"]["6"]        = c->GetCosts()[6];
	  break;
	}
	}
      }
      jUpgrades[uc]["hyperspace"] = (upgrade.GetHyperspace() && *(upgrade.GetHyperspace())) ? "Yes" : "No";
      int sides = upgrade.IsDualSided() ? 2 : 1;
      for(int i=0; i<sides; i++) {
	jUpgrades[uc]["side"][i]["title"] = upgrade.GetTitle();
	{
	  int ffg;
	  try {
	    ffg = converter::ffg::GetUpgrade(upgrade.GetUpg());
	  }
	  catch(converter::ffg::UpgradeNotFound(& unf)) {
	    ffg = -1;
	  }
	  jUpgrades[uc]["side"][i]["ffg"] = (ffg == -1) ? "???" : std::to_string(ffg);
	}
	{
	  std::string cardart;
	  try {
	    cardart = converter::ffg::GetArt(upgrade.GetUpg(), i);
	  }
	  catch(converter::ffg::UpgradeNotFound(& unf)) {
	    cardart = "???";
	  }
	  jUpgrades[uc]["side"][i]["cardart"] = cardart;
	}
	{
	  std::string cardimg;
	  try {
	    cardimg = converter::ffg::GetCard(upgrade.GetUpg(), i);
	  }
	  catch(converter::ffg::UpgradeNotFound(& unf)) {
	    cardimg = "???";
	  }
	  jUpgrades[uc]["side"][i]["cardimg"] = cardimg;
	}

	jUpgrades[uc]["side"][i]["type"] = upgrade.GetUpgradeType().GetName();
	int sc=0;
	for(UpT s : upgrade.GetSlots()) {
	  jUpgrades[uc]["side"][i]["slot"][sc++] = UpgradeType::GetUpgradeType(s).GetName();
	}
	if(upgrade.GetCharge().GetCapacity() > 0) {
	  jUpgrades[uc]["side"][i]["charge"]["capacity"] = upgrade.GetCharge().GetCapacity();
	  jUpgrades[uc]["side"][i]["charge"]["rechargeable"] = upgrade.GetCharge().GetRecurring();
	}
	if(upgrade.GetForce().GetCapacity() > 0) {
	  jUpgrades[uc]["side"][i]["force"]["capacity"] = upgrade.GetForce().GetCapacity();
	  jUpgrades[uc]["side"][i]["force"]["rechargeable"] = upgrade.GetForce().GetRecurring();
	}
	if(upgrade.GetAttackStats()) {
	  jUpgrades[uc]["side"][i]["attack"]["arc"]   = FiringArc::GetFiringArc(upgrade.GetAttackStats()->arc).GetName();
	  jUpgrades[uc]["side"][i]["attack"]["value"] = upgrade.GetAttackStats()->value;
	  jUpgrades[uc]["side"][i]["attack"]["ordnance"] = upgrade.GetAttackStats()->ordnance;
	  jUpgrades[uc]["side"][i]["attack"]["minrange"] = upgrade.GetAttackStats()->minRange;
	  jUpgrades[uc]["side"][i]["attack"]["maxrange"] = upgrade.GetAttackStats()->maxRange;
	}
	int ac=0;
	//for(const std::list<SAct>& sa : u.GetModifier().GetAddedActions()) { // <-- for some reason, this fails on linux but works on openbsd
	auto foo = upgrade.GetModifier().GetActAdd();
	for(auto &sa : foo) {
	  jUpgrades[uc]["side"][i]["actions"][ac]["difficulty"] = Difficulty::GetDifficulty(sa.front().difficulty).GetName();
	  jUpgrades[uc]["side"][i]["actions"][ac]["type"] = Action::GetAction(sa.front().action).GetName();
	  if(sa.size() == 2) {
	    jUpgrades[uc]["side"][i]["actions"][ac]["linked"]["difficulty"] = Difficulty::GetDifficulty(sa.back().difficulty).GetName();
	    jUpgrades[uc]["side"][i]["actions"][ac]["linked"]["type"] = Action::GetAction(sa.back().action).GetName();
	  }
	  ac++;
	}

	jUpgrades[uc]["side"][i]["restrictions"] = upgrade.GetRestriction().GetText();
	jUpgrades[uc]["side"][i][upgrade.HasAbility() ? "ability" : "text"] = upgrade.GetText().GetCleanText();

	if(upgrade.GetShipAbility()) { jUpgrades[uc]["side"][i]["shipAbility"] = upgrade.GetShipAbility()->GetName() + ": " + upgrade.GetShipAbility()->GetText().GetCleanText(); }

	upgrade.Flip();
      }
      if(upgrade.IsUnreleased()) {
	jUpgrades[uc]["unreleased"] = true;
      }
      int rc = 0;
      for(Rel rel : Release::GetByUpgrade(upgrade.GetUpg())) {
	Release release = Release::GetRelease(rel);
	jUpgrades[uc]["availability"][rc]["name"] = release.GetName();
	jUpgrades[uc]["availability"][rc]["sku"] = release.GetSku();
	rc++;
      }

      uc++;
    }

    std::string jsonStr;
    std::stringstream ss;
    ss << jUpgrades;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }


  else if(format == "text") {
    struct ds {
      std::string typ;
      std::string fac;
      std::string upg;
    };
    std::vector<ds> data;

    std::vector<Upgrade> upgds = GetUpgrades(restriction);
    for(const Upgrade &upgrade : upgds) {
      std::stringstream facStr;
      std::vector<Fac> facs = upgrade.GetRestriction().GetAllowedFactions();
      if(facs.size()) {
	bool empty = true;
	for(Fac fac : facs) {
	  Faction faction = Faction::GetFaction(fac);
	  if(!empty) {
	    facStr << "/";
	  }
	  facStr << faction.Get3Letter();
	  empty = false;
	}
      }
      data.push_back({upgrade.GetUpgradeType().GetShortName(), facStr.str(), upgrade.GetName()});
    }

    int typLen=0;
    int facLen=0;
    int upgLen=0;
    for(const ds &d : data) {
      typLen = std::max(typLen, (int)d.typ.size());
      facLen = std::max(facLen, (int)d.fac.size());
      upgLen = std::max(upgLen, (int)d.upg.size());
    }

    std::stringstream ss;
    for(const ds &d : data) {
      ss << std::setw(typLen) << std::left << d.typ;
      ss << "  ";
      ss << std::setw(facLen) << std::left << d.fac;
      ss << "  ";
      ss << std::setw(upgLen) << std::left << d.upg;
      ss << std::endl;
    }
    resp_open(kr, KHTTP_200, "text/plain");
    khttp_puts(kr, ss.str().c_str());
  }

  else if(format == "csv") {
    std::stringstream ss;
    ss << "Limit," << "Name," << "Cost," << "Hyperspace,"
       << "Slots," << "Title," << "Charge," << "Force," << "Attack," << "Restriction,"<< "Modifier,"  << "HasAbility," << "Text," << "ShipAbility";
    ss << std::endl;
    for(Upg upg : Upgrade::GetAllUpgs()) {
      Upgrade upgrade = Upgrade::GetUpgrade(upg);
      if(restriction == "hyperspace") {
	if((upgrade.GetHyperspace() == std::nullopt) || (*upgrade.GetHyperspace() == false)) {
	  continue;
	}
      }
      ss << CSV(upgrade.GetLimited() ? std::to_string(upgrade.GetLimited()) : "");
      ss << ",";
      ss << CSV(upgrade.GetName());
      ss << ","; 
      ss << CSV(GetCost(upgrade.GetCost()));
      ss << ",";
      ss << CSV(GetHyperspace(upgrade.GetHyperspace()));
      ss << ",";
      for(int i = 0; i < (upgrade.IsDualSided() ? 2 : 1); i++) {
	if(i) { ss << ",,,,"; }
	ss << CSV(GetSlots(upgrade.GetSlots()));
	ss << ",";
	ss << CSV(upgrade.GetTitle());
	ss << ",";
	ss << CSV(GetChargeable(upgrade.GetCharge()));
	ss << ",";
	ss << CSV(GetChargeable(upgrade.GetForce()));
	ss << ",";
	ss << CSV((upgrade.GetAttackStats() ? GetAttack(*upgrade.GetAttackStats()) : ""));
	ss << ",";
	ss << CSV(upgrade.GetRestriction().GetText());
	ss << ",";
	ss << CSV(GetModifiers(upgrade.GetModifier()));
	ss << ",";
	ss << CSV(upgrade.HasAbility() ? "Yes" : "No");
	ss << ",";
	ss << CSV(upgrade.GetText().GetCleanText());
	ss << ",";
	ss << CSV(upgrade.GetShipAbility() ? upgrade.GetShipAbility()->GetName() + ": " + upgrade.GetShipAbility()->GetText().GetCleanText()  : "");
	ss << std::endl;
	if(upgrade.IsDualSided()) { upgrade.Flip(); }
      }
    }
    resp_open(kr, KHTTP_200, "text/csv", "upgrades2.csv");
    khttp_puts(kr, ss.str().c_str());
  }
}
