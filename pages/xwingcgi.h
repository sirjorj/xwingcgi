#pragma once
#include "global.h"
// libxwing
#include "../libxwing/damagedeck.h"
#include "../libxwing/glyph.h"
#include "../libxwing/pilot.h"
#include "../libxwing/release.h"
#include "../libxwing/ship.h"
#include "../libxwing/squad.h"
// other
#include <ctime>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void PrintManeuvers(struct khtmlreq &r, libxwing::Maneuvers maneuvers);

void IconifyText(struct khtmlreq &r, std::string s);
