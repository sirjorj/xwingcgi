#include "conditions.h"

using namespace libxwing;



void conditions(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  // list of conditions
  khtml_elem(&r, KELEM_TABLE);

  for(Condition c : Condition::GetAllConditions()) {
    khtml_elem(&r, KELEM_TR);

    khtml_elem(&r, KELEM_TD);

    { // condition name
      std::string cUrl = std::string(req->pname)+"/condition?name=" + c.GetXws();
      khtml_attr(&r, KELEM_A, KATTR_HREF, cUrl.c_str(), KATTR__MAX);
      if(c.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str());
	khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, c.GetName().c_str());
      khtml_closeelem(&r, 1); // a
      khtml_closeelem(&r, 1); // td
    }
    khtml_closeelem(&r, 1); // tr
  }

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
