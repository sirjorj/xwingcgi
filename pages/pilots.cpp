#include "pilots.h"

using namespace libxwing;



void pilots(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  // list of pilots
  khtml_elem(&r, KELEM_TABLE);
  for(Pilot p : Pilot::GetAllPilots()) {

    khtml_elem(&r, KELEM_TR);

    { // faction
            std::string css;
      switch(p.GetFaction().GetType()) {
      case Fac::Rebel:      css = "factionRebel";      break;
      case Fac::Imperial:   css = "factionImperial";   break;
      case Fac::Scum:       css = "factionScum";       break;
      default:              css = "";
      }
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
      khtml_puts(&r, IconGlyph::GetGlyph(p.GetFaction().GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // ship icon
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "shipGlyph", KATTR__MAX);
      khtml_puts(&r, ShipGlyph::GetGlyph(p.GetShip().GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // pilot
      std::string pUrl = std::string(req->pname)+"/pilot?name=" + p.GetXws() + "&faction=" + p.GetFaction().GetXws() + "&ship=" + p.GetShip().GetXws();
      khtml_elem(&r, KELEM_TD);
      if(p.IsUnreleased()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "unreleased", KATTR__MAX);
	khtml_puts(&r, "[UNRELEASED] ");
	khtml_closeelem(&r, 1);
      }
      khtml_attr(&r, KELEM_A, KATTR_HREF, pUrl.c_str(), KATTR__MAX);
      if(p.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, p.GetName().c_str());
      khtml_closeelem(&r, 2);
    }
    khtml_closeelem(&r, 1); // tr
  }
  khtml_closeelem(&r, 1); // table
  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
