#include "damagedecks2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void damagedecks2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
    }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

  khtml_elem(&hr, KELEM_TABLE);
  for(Dck dck : Deck::GetAllDcks()) {
    Deck deck = Deck::GetDeck(dck);
    khtml_elem(&hr, KELEM_TR);
    { // Name
      //bool isHugeShip = (d.GetXws() != "core" && d.GetXws() != "core2");
      //std::string css;
      //if(isHugeShip) { css = "text"; }
      //else           { css = "bold"; }
      khtml_attr(&hr, KELEM_TD, KATTR__MAX);
      khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "damagedeck2").AddArg(PageToken::PointList).AddArg(PageToken::Deck,deck.GetDckStr()).Get().c_str(), KATTR__MAX);
      khtml_puts(&hr, deck.GetName().c_str());
      khtml_closeelem(&hr, 2); // a, td
    }
    khtml_closeelem(&hr, 1); // tr
  }
  khtml_closeelem(&hr, 1); // tab
  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
