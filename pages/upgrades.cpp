#include "upgrades.h"

using namespace libxwing;



void upgrades(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  // list of upgrades
  khtml_attr(&r, KELEM_TABLE, KATTR_CLASS, "us_tab_Upgrades", KATTR__MAX);
  for(Upgrade u : Upgrade::GetAllUpgrades()) {
    khtml_elem(&r, KELEM_TR);

    { // upgrade type
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "iconGlyph", KATTR__MAX);
      khtml_puts(&r, IconGlyph::GetGlyph(u.GetType().GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    {
      std::string uUrl = std::string(req->pname)+"/upgrade?type=" + u.GetType().GetXws() + "&name=" + u.GetXws();
      khtml_elem(&r, KELEM_TD);
      if(u.IsUnreleased()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "unreleased", KATTR__MAX);
	khtml_puts(&r, "[UNRELEASED] ");
	khtml_closeelem(&r, 1); // span
      }
      khtml_attr(&r, KELEM_A, KATTR_HREF, uUrl.c_str(), KATTR__MAX);
      if(u.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, u.GetName().c_str());
      khtml_closeelem(&r, 2);
    }
    khtml_closeelem(&r, 1); // tr
  }
  khtml_closeelem(&r, 1); // table
  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
