#include "global.h"
#include "pagetoken2.h"
#include "xwingcgi2.h"

void index(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  CSS css;
  css.MainMenu();
  PrintHead(kr, hr, kr->pname, css.GetCSS());

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {}, StripLeadingSlash(kr->pname), GetPointsListForBreadcrumbs(hr, ptl, pt));

  khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "notice", KATTR__MAX);
  khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "NOTICE:"); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR);
  khtml_elem(&hr, KELEM_TD);
  khtml_puts(&hr,"This site will no longer be updated, but it will be kept online to remember X-wing at its best.");
  khtml_elem(&hr, KELEM_BR);
  khtml_puts(&hr,"Thanks to all the other developers I worked with and to FFG for making a great game.");
  khtml_closeelem(&hr, 3); // td, tr, table

  //std::ostringstream comment;
  //comment << std::endl << std::endl << "<!--" << std::endl
  //        << "Hello World"
  //        << std::endl << "-->" << std::endl << std::endl;
  //khttp_write(kr, comment.str().c_str(), comment.str().size());

  khtml_elem(&hr, KELEM_BR);

  khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "mainmenu", KATTR__MAX);
  { // header
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "First Edition"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "Second Edition"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1); // tr
  }
  { // ships
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/ships").c_str(), KATTR__MAX); khtml_puts(&hr, "ships");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "ships2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "ships");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // pilots
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/pilots").c_str(), KATTR__MAX); khtml_puts(&hr, "pilots");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "pilots2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "pilots");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // upgrades
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/upgrades").c_str(), KATTR__MAX); khtml_puts(&hr, "upgrades");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "upgrades2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "upgrades");
    khtml_closeelem(&hr, 3); // a td tr
  }
  /*
  { // remotes
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "remotes2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "remotes");
    khtml_closeelem(&hr, 3); // a td tr
  }
  */
  { // conditions
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/conditions").c_str(), KATTR__MAX); khtml_puts(&hr, "conditions");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "conditions2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "conditions");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // environments
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "environments2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "environments");
    khtml_closeelem(&hr, 3); // a td tr
  }
  /*
  { // ai
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "ai2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "ai");
    khtml_closeelem(&hr, 3); // a td tr
  }
  */
  { // releases
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/releases").c_str(), KATTR__MAX); khtml_puts(&hr, "releases");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "releases2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "releases");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // damagedecks
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/damagedecks").c_str(), KATTR__MAX); khtml_puts(&hr, "damagedecks");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "damagedecks2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "damagedecks");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // quickbuilds
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "quickbuilds2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "quickbuilds");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // validator / squad
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/validator").c_str(), KATTR__MAX); khtml_puts(&hr, "validator");
    khtml_closeelem(&hr, 2); // a td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "squad2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "squad");
    khtml_closeelem(&hr, 3); // a td tr
  }

  { // dial compare
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR__MAX);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "dialcomp").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "dialcomp");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // colors
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "colors2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "colors");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // search
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "search2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "search");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // costs
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "costs2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "costs");
    khtml_closeelem(&hr, 3); // a td tr
  }
  { // timeline
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "timeline2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "timeline");
    khtml_closeelem(&hr, 3); // a td tr
  }
  /*
  { // inventory
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TD);
    khtml_closeelem(&hr, 1); // td
    khtml_elem(&hr, KELEM_TD);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, (std::string(kr->pname) + "/inventory2").c_str(), KATTR__MAX); khtml_puts(&hr, "inventory");
    khtml_closeelem(&hr, 3); // a td tr
  }
  */
  khtml_closeelem(&hr, 1); // table

  // uses...
  khtml_elem(&hr, KELEM_BR);
  khtml_elem(&hr, KELEM_TABLE);
  khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "xwing.cgi uses:"); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://kristaps.bsd.lv/kcgi/", KATTR__MAX); khtml_puts(&hr, "kcgi"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://xhud.sirjorj.com", KATTR__MAX); khtml_puts(&hr, "libxwing & libxwing2"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://github.com/geordanr/xwing-miniatures-font", KATTR__MAX); khtml_puts(&hr, "xwing-miniatures-font"); khtml_closeelem(&hr, 3);
  khtml_closeelem(&hr, 1); // table

  // validation...
  khtml_elem(&hr, KELEM_BR);
  khtml_elem(&hr, KELEM_TABLE);
  khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "data validated against:"); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://www.fantasyflightgames.com/en/products/x-wing-second-edition/", KATTR__MAX); khtml_puts(&hr, "FFG Points Sheets"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://github.com/guidokessels/xwing-data2", KATTR__MAX); khtml_puts(&hr, "xwing-data2"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://raithos.github.io/", KATTR__MAX); khtml_puts(&hr, "YASB 2.0"); khtml_closeelem(&hr, 3);
  khtml_closeelem(&hr, 1); // table

  // used by...
  khtml_elem(&hr, KELEM_BR);
  khtml_elem(&hr, KELEM_TABLE);
  khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "xwing.cgi is used by:"); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "http://advancedtargeting.computer/", KATTR__MAX); khtml_puts(&hr, "Advanced Targeting Computer"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "https://github.com/FreakyDug/r2-d7", KATTR__MAX); khtml_puts(&hr, "r2-d7"); khtml_closeelem(&hr, 3);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD); khtml_attr(&hr, KELEM_A, KATTR_HREF, "http://x-wing-ai.com", KATTR__MAX); khtml_puts(&hr, "X-Wing Miniatures AI 2nd Edition"); khtml_closeelem(&hr, 3);
  khtml_closeelem(&hr, 1); // table

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
