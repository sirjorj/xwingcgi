#include "timeline2.h"
#include "pagetoken2.h"
#include <kcgixml.h>

using namespace libxwing2;

static std::string GetColor(RelGroup rg) {
  if     (rg == RelGroup::FeWave14)    { return "#FF9999"; }
  else if(rg == RelGroup::CoreSet)     { return "#FFFFFF"; }
  else if(rg == RelGroup::Accessories) { return "#404040"; }
  else if(rg == RelGroup::CardPack)    { return "#808080"; }
  else if(rg == RelGroup::Huge)        { return "#C0C0C0"; }
  // https://www.rapidtables.com/web/color/RGB_Color.html
  else if(rg == RelGroup::Wave1)       { return "#FF0000"; }
  else if(rg == RelGroup::Wave2)       { return "#FF7F00"; }
  else if(rg == RelGroup::Wave3)       { return "#FFFF00"; }
  else if(rg == RelGroup::Wave4)       { return "#00FF00"; } // 7FFF00 is not noticeably different than 00FF00
  else if(rg == RelGroup::Wave5)       { return "#00FF7F"; }
  else if(rg == RelGroup::Wave6)       { return "#00FFFF"; }
  else if(rg == RelGroup::Wave7)       { return "#007FFF"; }
  else if(rg == RelGroup::Wave8)       { return "#0000FF"; }
  else if(rg == RelGroup::Wave9)       { return "#7F00FF"; }
  else if(rg == RelGroup::Wave10)      { return "#FF00FF"; }
//else if(rg == RelGroup::Wave11)      { return "#FF007F"; }
  // then repeat?
  else { return "#FFFFFF"; }
}



void timeline2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  bool svg = false;;
  for(int i=0; i<kr->fieldsz; i++) {
    if(strcmp(kr->fields[i].key, "svg")  == 0)  { svg = true; }
  }

  if(svg) {
    struct kxmlreq xr;

    // build up the data to graph
    std::vector<Release> releases;
    for(Rel rel : Release::GetAllRels()) {
      releases.push_back(Release::GetRelease(rel));
    }
    std::sort(releases.begin(),
	      releases.end(),
	      [&](const Release a, const Release b)-> bool {
		if(a.GetAnnounceDate() == b.GetAnnounceDate()) {
		  return a.GetRel() < b.GetRel();
		} else {
		  return a.GetAnnounceDate() < b.GetAnnounceDate();
		}
	      });

    // build up list of dates
    std::vector<RelDate> dates;
    RelDate startDate = { 2018, 1, 1};// releases.begin()->GetAnnounceDate();
    RelDate endDate;
    {
      std::time_t t = std::time(0);
      std::tm* now = std::localtime(&t);
      endDate.year = now->tm_year + 1900;
      endDate.month = now->tm_mon + 1;
      endDate.date =  now->tm_mday;
    }
    {
      RelDate curDate = startDate;
      while(curDate < endDate) {
	dates.push_back(curDate);
	curDate.month++;
	if(curDate. month > 12) {
	  curDate.month = 1;
	  curDate.year++;
	}
      }
    }
    

    int leftWidth = 15; // extra padding on the left
    int nameWidth = 190; // extra padding on the right

    int contentHeight = (releases.size()+1) * 10;
    int footerHeight = 20;
    
    int width  = leftWidth + GetDaysBetween(startDate, endDate) + nameWidth;
    int height = contentHeight + footerHeight;

    // page
    resp_open(kr, KHTTP_200, "image/svg+xml");

    const char* SVG_ELEMS[] = {
      "svg",
      "desc",
      "rect",
      "g",
      "line",
      "circle",
      "a",
      "text"

    };

    enum SVGs {
      SVG_SVG,
      SVG_DESC,
      SVG_RECT,
      SVG_GROUP,
      SVG_LINE,
      SVG_CIRCLE,
      SVG_A,
      SVG_TEXT,

      SVG__MAX
    };

    kxml_open(&xr, kr, SVG_ELEMS, SVG__MAX);

    // <?xml version="1.0" encoding="UTF-8"?>
    kxml_prologue(&xr);

    // <svg xmlns="http://www.w3.org/2000/svg" version="1.2" baseProfile="tiny" width="5cm" height="4cm" viewBox="0 0 100 100">
    kxml_pushattrs(&xr, SVG_SVG,
		   "xmlns",   "http://www.w3.org/2000/svg",
		   "xmlns:xlink", "http://www.w3.org/1999/xlink",
		   "version", "1.2",
		   "baseProfile", "tiny",
		   "width",  (std::to_string(width)  + "px").c_str(),
		   "height", (std::to_string(height) + "px").c_str(),
		   "viewBox", ("0 0 "+ std::to_string(width) + "px " + std::to_string(height) + "px").c_str(),
		   NULL);

    // the description
    kxml_push(&xr, SVG_DESC);
    kxml_puts(&xr, "X-wing Release Timeline");
    kxml_pop(&xr);

    // the border
    kxml_pushnullattrs(&xr, SVG_RECT,
		       "x", "0",
		       "y", "0",
		       "width",  (std::to_string(width)  + "px").c_str(),
		       "height", (std::to_string(height) + "px").c_str(),
		       "fill", "black",
		       "stroke", "#006400",
		       "stroke-width", "2",
		       NULL);

    // the background
    kxml_pushattrs(&xr, SVG_GROUP,
		   "xml:id", "background",
		   "fill", "none",
		   NULL);
    for(const RelDate& rd : dates) {
      int x = leftWidth + GetDaysBetween(startDate, rd);
      std::string color = "gray";
      // every third month is brighter and has a name printed
      if(((rd.month-1) %3) == 0) {
	color = "white";
	std::stringstream ss;
	switch(rd.month) {
	case  1: ss << "Jan"; break;
	case  4: ss << "Apr"; break;
	case  7: ss << "Jul"; break;
	case 10: ss << "Oct"; break;
	default:              break;
	}
	ss << " '";
	ss << std::to_string(rd.year % 2000);

	// label
	kxml_pushattrs(&xr, SVG_TEXT,
		       "stroke-width", "1",
		       "font-size", "10",
		       "font-family", "Arial",
		       "stroke", color.c_str(),
		       "x", std::to_string(x-10).c_str(),
		       "y", std::to_string(contentHeight+10).c_str(),
		       NULL);
	kxml_puts(&xr, ss.str().c_str());
	kxml_pop(&xr);
      }
      // line
      kxml_pushnullattrs(&xr, SVG_LINE,
			 "stroke-width", "1",
			 "stroke", color.c_str(),
			 "x1", std::to_string(x).c_str(),
			 "y1", "1",
			 "x2", std::to_string(x).c_str(),
			 "y2", std::to_string(contentHeight-2).c_str(),
			 NULL);
    }
    // website
    kxml_pushattrs(&xr, SVG_TEXT,
		   "stroke-width", "1",
		   "font-size", "12",
		   "font-family", "\"Lucida Console\", Monaco, monospace",
		   "stroke", "green",
		   "fill", "green",
		   "x", std::to_string(width-nameWidth+60).c_str(), //std::to_string(leftWidth).c_str(), 
		   "y", std::to_string(contentHeight+10).c_str(), //std::to_string(contentHeight-50).c_str(),
		   NULL);
    kxml_puts(&xr, "xhud.sirjorj.com");
    kxml_pop(&xr);

    kxml_pop(&xr);

    // the release lines    
    int c=0;
    for(const Release& r : releases) {
      
      std::string sPoint; // start point (beginning of thin line)
      std::string mPoint; // middle point (end of thin line/start of thick line)
      std::string ePoint; // end point (end of thick line - empty if not released yet)
      std::string nPoint; // the release name starts here
      {
	sPoint = std::to_string(leftWidth + GetDaysBetween(startDate, r.GetAnnounceDate())).c_str();
	if((r.GetReleaseDate().year == 0) || (r.GetReleaseDate() > endDate)) {
	  // not yet released, so no ePoint
	  int val = leftWidth + GetDaysBetween(startDate, endDate);
	  mPoint = std::to_string(val);
	  nPoint = std::to_string(val + 10);
	} else {
	  // released
	  mPoint = std::to_string(leftWidth + GetDaysBetween(startDate, r.GetReleaseDate()));
	  int val = leftWidth + GetDaysBetween(startDate, endDate);
	  ePoint = std::to_string(val);
	  nPoint = std::to_string(val + 10);
	}
      }

      std::to_string(leftWidth + GetDaysBetween(startDate, (r.GetReleaseDate().year == 0) ? endDate : r.GetReleaseDate())).c_str();

      kxml_pushattrs(&xr, SVG_GROUP,
		     "xml:id", "foo",//r.GetName().c_str(),
		     "fill", GetColor(r.GetGroup()).c_str(),//"none",
		     "stroke", GetColor(r.GetGroup()).c_str(),
		     NULL);

      // announce to release
      kxml_pushnullattrs(&xr, SVG_LINE,
			 "stroke-width", "1",
			 "x1", sPoint.c_str(),
			 "y1", std::to_string(c*10+5).c_str(),
			 "x2", mPoint.c_str(),
			 "y2", std::to_string(c*10+5).c_str(),
			 NULL);

      // release to present
      if(ePoint != "") {
	kxml_pushnullattrs(&xr, SVG_LINE,
			   "stroke-width", "9",
			   "x1", mPoint.c_str(),
			   "y1", std::to_string(c*10+5).c_str(),
			   "x2", ePoint.c_str(),
			   "y2", std::to_string(c*10+5).c_str(),
			   NULL);
      }

      // articles
      std::string artRad = "2";

      { // announcement
	std::string aPoint = std::to_string(leftWidth+GetDaysBetween(startDate,r.GetAnnouncementArticle().date));
	//kxml_pushattrs(&xr, SVG_A,
	//	       "xlink:href", r.GetAnnouncementArticle().url.c_str(),
	//	       "target", "_blank",
	//	       NULL);
	kxml_pushnullattrs(&xr, SVG_CIRCLE,
			   "cx", aPoint.c_str(),
			   "cy", std::to_string(c*10+5).c_str(),
			   "r", artRad.c_str(),
			   NULL);
	//kxml_pop(&xr);
      }
      { // previews
	for(const Article& a : r.GetPreviewArticles()) {
	  std::string pPoint = std::to_string(leftWidth+GetDaysBetween(startDate,a.date));
	  //kxml_pushattrs(&xr, SVG_A,
	  //		 "xlink:href", a.url.c_str(),
	  //		 NULL);
	  kxml_pushnullattrs(&xr, SVG_CIRCLE,
			     "cx", pPoint.c_str(),
			     "cy", std::to_string(c*10+5).c_str(),
			     "r", artRad.c_str(),
			     NULL);
	  //kxml_pop(&xr);
	}
      }
      { // release
	if(r.GetReleaseArticle().date <= endDate) {
	  std::string rPoint = std::to_string(leftWidth+GetDaysBetween(startDate,r.GetReleaseArticle().date));
	  //kxml_pushattrs(&xr, SVG_A,
	  //	       "xlink:href", r.GetReleaseArticle().url.c_str(),
	  //	       NULL);
	  kxml_pushnullattrs(&xr, SVG_CIRCLE,
			     "cx", rPoint.c_str(),
			     "cy", std::to_string(c*10+5).c_str(),
			     "r", artRad.c_str(),
			     NULL);
	  //kxml_pop(&xr);
	}
      }

      // label
      kxml_pushattrs(&xr, SVG_TEXT,
		     "stroke-width", "0.5",
		     "font-size", "8",
		     "font-family", "Arial",
		     "x", nPoint.c_str(),
		     "y", std::to_string(c*10+9).c_str(),
		     NULL);
      kxml_puts(&xr, r.GetName().c_str());
      kxml_pop(&xr);


      kxml_pop(&xr);
      c++;
    }


    kxml_pop(&xr);

    kxml_close(&xr);
  }

  else {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);
    // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));
    // the image
    khtml_attr(&hr, KELEM_IMG, KATTR_SRC, LinkBuilder(pt,pt.GetPath()).AddArg("svg","1").Get().c_str(), KATTR__MAX);
    khtml_closeelem(&hr, 1); // img

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }
}
