#include "conditions2.h"
#include "pagetoken2.h"
#include "../json/json.h"

using namespace libxwing2;

void conditions2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format = pt.Get(PageToken::Format);

  if(format == "" || format == "table") {  
    struct khtmlreq hr;

    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    // list of conditions
    khtml_elem(&hr, KELEM_TABLE);

    for(Cnd cnd : Condition::GetAllCnds()) {
      Condition condition = Condition::GetCondition(cnd);
      khtml_elem(&hr, KELEM_TR);

      khtml_elem(&hr, KELEM_TD);

      { // condition name
	PrintConditionName(kr, hr, pt, condition);
      }
      khtml_closeelem(&hr, 2); // td tr
    }
    khtml_closeelem(&hr, 1); // table

    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      khtml_puts(&hr, "[");
      khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "conditions2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").Get().c_str(), KATTR__MAX);
      khtml_puts(&hr, "json");
      khtml_closeelem(&hr, 1); // a
      khtml_puts(&hr, "]");
      khtml_closeelem(&hr, 1); // rd

      khtml_closeelem(&hr, 2); // tr, table
    }

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }
  else if(format == "json") {
    int cc=0;
    Json::Value conditions;
    for(Cnd cnd : Condition::GetAllCnds()) {
      Condition condition = Condition::GetCondition(cnd);
      conditions[cc]["name"] = condition.GetName();
      conditions[cc]["text"] = condition.GetText();
      if(condition.IsUnreleased()) {
	conditions[cc]["unreleased"] = true;
      }
      cc++;
    }
    std::string jsonStr;
    std::stringstream ss;
    ss << conditions;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }

}
