#include "pilot2.h"
#include "pagetoken2.h"
#include <libxwing2/converter.h>

using namespace libxwing2;

Pilot GetPilot(const PageToken& pt) {
  std::string s;
  if     ((s = pt.Get(PageToken::Pilot)) != "") { return Pilot::GetPilot(s); }
  else if((s = pt.Get(PageToken::XWS))   != "") { return Pilot::GetPilot(converter::xws::GetPilot(s)); }
  throw PilotNotFound("");
}

static void PrintCard(struct khtmlreq &hr, PageToken &pt, Pilot &pilot) {
  // div for card and card link
  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "cardcontainer", KATTR__MAX);
  // pilot card
  PrintPilotCard(hr, pt, pilot);
  // keywords
  if(pilot.GetKeywordList()) {
    khtml_elem(&hr, KELEM_BR);
    libxwing2::KeywordList kwl = *pilot.GetKeywordList();
    for(libxwing2::KWd kwd : kwl) {
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "keyword", KATTR__MAX);
      khtml_puts(&hr, libxwing2::Keyword::GetKeyword(kwd).GetName().c_str());
      khtml_closeelem(&hr, 1);
    }
  }
  // link to card image
  std::string imgUrl;
  try {
    imgUrl = converter::ffg::GetCard(pilot.GetPlt()).c_str();
  }
  catch(converter::ffg::PilotNotFound &pnf) { }
  if(imgUrl != "") {
    khtml_elem(&hr, KELEM_BR);
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "cardimage", KATTR__MAX);
    khtml_puts(&hr, "[");
    khtml_attr(&hr, KELEM_A, KATTR_HREF, converter::ffg::GetCard(pilot.GetPlt()).c_str(), KATTR__MAX);
    khtml_puts(&hr, "IMAGE");
    khtml_closeelem(&hr, 1);
    khtml_puts(&hr, "]");
    khtml_closeelem(&hr, 1);
  }
  // end of card/link div
  khtml_closeelem(&hr, 1);
}

void pilot2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  Pilot pilot = GetPilot(pt);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    css.Maneuvers();
    css.PilotInfo();
    css.PilotCard();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + pilot.GetName() + "]", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			    {LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).Get(), "pilots2"}
    }, pilot.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

  // pilot card
  PrintCard(hr, pt, pilot);

  // availablilty
  khtml_attr(&hr, KELEM_UL, KATTR__MAX);
  for(Rel rel : Release::GetByPilot(pilot.GetPlt())) {
    Release release = Release::GetRelease(rel);
    khtml_elem(&hr, KELEM_LI);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,release.GetSku()).Get().c_str(), KATTR__MAX);
    khtml_puts(&hr, release.GetName().c_str());
    khtml_closeelem(&hr, 2); // a li
  }
  khtml_closeelem(&hr, 1); // ul

  { // history
    khtml_elem(&hr, KELEM_TABLE);

    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, ""); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Date"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Hyp"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Cost"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Upgrades"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);

    const std::vector<PtL> ptls = PointList::GetPtLs();
    std::optional<bool> lastHyp;
    std::optional<Cost> lastCst;
    std::optional<UpgradeBar> lastUb;
    for(PtL ptl : ptls) {
      std::optional<bool> hyp = PointList::GetHyperspace(pilot.GetPlt(), ptl);
      std::optional<Cost> cst = PointList::GetCost(pilot.GetPlt(), ptl);
      std::optional<UpgradeBar> ub = PointList::GetUpgradeBar(pilot.GetPlt(), ptl);
      if((hyp && (!lastHyp || (*hyp != *lastHyp))) ||
	 (cst && (!lastCst || (*cst != *lastCst))) ||
	 (ub  && (!lastUb  || (*ub  != *lastUb)))) {
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, PointList::GetName(ptl).c_str()); khtml_closeelem(&hr, 1);
	char date[11];
	const PLDate d = PointList::GetEffectDate(ptl);
	snprintf(date, sizeof(date), "%04d/%02d/%02d", d.year, d.month, d.date);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, date); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, *hyp ? "Yes" : "No"); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, std::to_string(cst->GetCosts()[0]).c_str()); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD);
	PrintUpgradeBar(hr, *ub);
	khtml_closeelem(&hr, 1); // td
	khtml_closeelem(&hr, 1); // tr
      }
      lastHyp = hyp;
      lastCst = cst;
      lastUb  = ub;
    }
    khtml_closeelem(&hr, 1); // table
  }

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
