#pragma once
#include <optional>

// kcgi
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <kcgi.h>
#include <kcgihtml.h>

// other
#include <ctime>
#include <functional>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>



void resp_open(struct kreq *kr, enum khttp http, std::string mime="");
void resp_open(struct kreq *kr, enum khttp http, std::string mime, std::string filename);

std::string GetBuildDateTimeString();

void PrintBreadcrumbs(struct kreq *kr, struct khtmlreq &hr,
		      std::vector<std::pair<std::string,std::string>> trail, std::string current,
		      std::function<void()> extra = nullptr);

std::string StripLeadingSlash(std::string text);

void PrintHead(struct kreq *kr, khtmlreq &hr, std::string name, std::string css="");

void PrintError(khtmlreq &hr, std::string msg);

void DumpInfo(struct kreq *kr, khtmlreq &hr);
