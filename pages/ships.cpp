#include "ships.h"

using namespace libxwing;



void ships(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  // list of ships
  khtml_elem(&r, KELEM_TABLE);

  for(auto s : Ship::GetAllShips()) {
    khtml_elem(&r, KELEM_TR);

    SFa f = SFa::None;
    for(auto p : Pilot::GetAllPilots()) {
      if(p.GetShip().GetType() == s.GetType()) {
	f = f | p.GetSubFaction().GetType();
      }
    }
    { // faction
      khtml_elem(&r, KELEM_TD);
      if((f & SFa::RebelAlliance)  == SFa::RebelAlliance) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "factionRebel", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(Fac::Rebel).c_str());
	khtml_closeelem(&r, 1);
      }
      if((f & SFa::GalacticEmpire) == SFa::GalacticEmpire) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "factionImperial", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(Fac::Imperial).c_str());
	khtml_closeelem(&r, 1);
      }
      if((f & SFa::ScumAndVillany) == SFa::ScumAndVillany) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "factionScum", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(Fac::Scum).c_str());
	khtml_closeelem(&r, 1);
      }
      if((f & SFa::Resistance) == SFa::Resistance) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "factionResistance", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(SFa::Resistance).c_str());
	khtml_closeelem(&r, 1);
      }
      if((f & SFa::FirstOrder) == SFa::FirstOrder) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "factionFirstOrder", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(SFa::FirstOrder).c_str());
	khtml_closeelem(&r, 1);
      }
      khtml_closeelem(&r, 1);  
    }
    { // ship icon
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "shipGlyph", KATTR__MAX);
      khtml_puts(&r, ShipGlyph::GetGlyph(s.GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // ship name
      std::string sUrl= std::string(req->pname) + "/ship?ship=" + s.GetXws();
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "shipName", KATTR__MAX);
      khtml_attr(&r, KELEM_A, KATTR_HREF, sUrl.c_str(), KATTR__MAX);
      khtml_puts(&r, s.GetName().c_str());
      khtml_closeelem(&r, 2); // a, td
    }
    khtml_closeelem(&r, 1); // tr
  }

  khtml_closeelem(&r, 1); //table
  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
