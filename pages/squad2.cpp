#include "squad2.h"
#include "pagetoken2.h"
#include "../json/json.h"
#include "../b64/encode.h"
#include <sys/wait.h>

using namespace libxwing2;



//static std::string GetBase64(std::istream &input) {
//  base64::encoder enc;
//  std::ostringstream output;
//  enc.encode(input, output);
//  return output.str();
//}



Squad GetSquad(std::string input) {
  Squad s;      
  try {
    // is it a yasb url?
    std::string start = "https://raithos.github.io/?";
    if(input.substr(0, start.size()) == start) {
      s = converter::yasb::GetSquad(input);
    }
    // if not, assume it is xws
    else {
      std::stringbuf buf(input);
      std::istream istr(&buf);
      s = converter::xws::GetSquad(istr);
    }
  }
  catch(PilotNotFound pnf)     { throw std::runtime_error(pnf.what()); }
  catch(UpgradeNotFound unf)   { throw std::runtime_error(unf.what()); }
  catch(std::runtime_error &e) { throw std::runtime_error(e.what());   }
  catch(std::exception e)      { throw std::runtime_error(e.what());   }
  return s;
}


std::string GetListJuggler(Squad s) {
  std::stringstream ss;
  for(const Pilot &p : s.GetPilots()) {
    ss << (p.GetModCost() ? std::to_string(*p.GetModCost()) : "???") << " " << p.GetName() << " (" << p.GetShip().GetName() << ")";
    for(const Upgrade &u : p.GetAppliedUpgrades()) {
      ss << " + [" << u.GetUpgradeType().GetShortName()<< "] " << u.GetName();
    }
    ss << std::endl;
  }
  ss << "(" << (s.GetCost() ? std::to_string(*s.GetCost()) : "???") << ")" << std::endl;
  return ss.str();
}


std::string GenerateImage(const Squad &s) {
  // prepare the pipes ([read][write])
  int pc[2]; // parent-to-child
  int cp[2]; // child-to-parent
  int er[2]; // errors (child-to-parent)
  if(pipe(pc) < 0) { throw std::runtime_error("pipe(pc)"); }
  if(pipe(cp) < 0) { throw std::runtime_error("pipe(cp)"); }
  if(pipe(er) < 0) { throw std::runtime_error("pipe(er)"); }

  int pid = fork();

  // FAIL!
  if(pid < 0) {
    // close everything
    close(pc[0]); close(pc[1]);
    close(cp[0]); close(cp[1]);
    close(er[0]); close(er[1]);
    throw std::runtime_error("error forking");
  }

  // child
  else if(pid == 0) {
    dup2(pc[0], 0); // stdin  - read  parent-to-child
    dup2(cp[1], 1); // stdout - write child-to-parent
    dup2(er[1], 2); // stderr - write errors
    close(pc[1]); // NOT writing to   parent-to-child
    close(cp[0]); // NOT reading from child-to-parent
    close(er[0]); // NOT reading from errors

#if 1
    // loopback
    char ch;
    while(read(pc[0], &ch, 1) == 1) {
      write(cp[1], &ch, 1);
    }
    //std::string foo;
    //std::cin >> foo;
    //std::cout << foo;
#endif

    // *sigh* for now do this:
    //   'linux' - the environment on my web bost
    //   'else'  - my local openbsd/httpd machine
#ifdef __linux__
    execlp("/home/sirjorj/bin/xhud", "/home/sirjorj/bin/xhud", "img", "--", "--", nullptr);
#else
    //execlp("/bin/xhud", "/bin/xhud", "img", "--", "--", nullptr);
    // note: if openbsd is just not working, the libraries are probably out of date.
    // comment out the code below that writes the xws to the child and the lib errors will be shown
#endif

    // if we get here, exec failed

    //std::cerr << "poop";
    
    //fprintf(stderr, "farty");
    
    //perror("EXEC error: ");
    
    //close(pc[0]);
    //close(cp[1]);
    //close(er[1]);
    _exit(1);
  }

  // parent
  else {
    close(pc[0]); // NOT reading from parent-to-child
    close(cp[1]); // NOT writing to   child-to-parent
    close(er[1]); // NOT writing to   errors

    // send squad to child 
    std::string xws = converter::xws::GetSquad(s);
    for(char c : xws) {
      write(pc[1], &c, 1);
    }
    close(pc[1]);

    // get image from child
    std::stringbuf sb;
    char ch;
    while(read(cp[0], &ch, 1) == 1) {
      sb.sputc(ch);
    }
    close(cp[0]);

    // get any error messages
    std::string errMsg;
    {
      std::stringstream ss;
      while(read(er[0], &ch, 1) == 1) {
	ss << ch;
      }
      close(er[0]);
      errMsg = ss.str();
    }

    // done
    waitpid(pid, 0, 0);

    // handle error if one came back
    if(errMsg.size()) {
      throw std::runtime_error(("Image Generation Error: " + errMsg).c_str());
      //khtml_elem(&hr,KELEM_B); khtml_puts(&hr,"Image Generation Error: ");  khtml_closeelem(&hr,1); khtml_puts(&hr, errMsg.c_str()); khtml_elem(&hr, KELEM_BR);
    }
    //throw std::runtime_error("got back " + std::to_string(sb.str().size()) + " bytes - '" + sb.str() + "'");

    // handle the output
    std::istream input(&sb);
    std::string ret;

#if 1
    // debuging - figure out what happened!
    std::stringstream ss;

    while(true) {
      //ss << "?";
      char c=0;
      input.read(&c, 1);
      if(!input) {
	break;
      }
      //ss << std::hex << (int)c << " ";
      ss << c;
    }

    ret = ss.str();

#elif
    // draw the image
    ret = GetBase64(input);
    //throw std::runtime_error(("b64: " + b64).c_str());
#endif

    return ret;
  }
}

std::vector<std::string> GetPrintedSquad(Squad& squad, PtL ptl) {
  std::vector<std::string> ret;
  for(Pilot& pilot : squad.GetPilots()) {
    char pilotStr[64];
    std::optional<int16_t> pCost = pilot.GetModCost(ptl);
    if(!pCost) { return {}; }
    snprintf(pilotStr, sizeof(pilotStr), "[%d] %s (%d)\n", *pCost, pilot.GetName().c_str(), *pilot.GetNatCost(ptl));
    ret.push_back(pilotStr);
    for(Upgrade& upgrade : pilot.GetAppliedUpgrades()) {
      char upgradeStr[64];
      std::optional<int16_t> uCost = pilot.GetUpgCost(upgrade.GetUpg(), ptl);
      if(!uCost) { return {}; }
      snprintf(upgradeStr, sizeof(upgradeStr), "     %s (%d)\n", upgrade.GetName().c_str(), *uCost);
      ret.push_back(upgradeStr);
    }
  }
  return ret;
}

void squad2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format = pt.Get(PageToken::Format); // 'json' to return data as json, nothing to return data in a web page
  std::string squad  = pt.Get(PageToken::Squad); // xws or yasbURL - if nothing, print form and instructions
  // these are only checked if format is 'json' and value is ignored
  bool validate = pt.HasArg(PageToken::Validate); // return validation results
  bool costs    = pt.HasArg(PageToken::Cost); // print cost history of this list
  bool lj       = pt.HasArg(PageToken::LJ); // include list juggler style formatted squad
  bool yasb     = pt.HasArg(PageToken::YASB); // include yasb URL
  bool xws      = pt.HasArg(PageToken::XWS); // include xws
  bool image    = pt.HasArg(PageToken::Image); // include xhud image

  // if something was POSTed...
  if(squad != "") {

    // no format specified - generate the page
    if(format == "") {
      struct khtmlreq hr;

      // page
      resp_open(kr, KHTTP_200);
      khtml_open(&hr, kr, KHTML_PRETTY);
      khtml_elem(&hr, KELEM_DOCTYPE);
      khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

      { // head
	CSS css;
	PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
      }

      // body
      khtml_elem(&hr, KELEM_BODY);
      PrintBreadcrumbs(kr, hr, {
				{LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
	}, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

      // try to get the data
      bool gotSquad = true;
      Squad s;
      try {
	s = GetSquad(squad);
      }
      catch(std::runtime_error &e) {
	gotSquad = false;
	khtml_puts(&hr, e.what());
      }

      if(gotSquad) {

	// i might make these options again at some point, but until then, just set tham all to 1
	validate = costs = yasb = xws = lj = image = 1;

	if(validate) {
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Validate"); khtml_closeelem(&hr,2);
	  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TD);
	  std::vector<std::string> issues = s.Validate();
	  khtml_elem(&hr, KELEM_P);
	  khtml_elem(&hr,KELEM_B); khtml_puts(&hr,"Name: ");        khtml_closeelem(&hr,1); khtml_puts(&hr,s.GetName().c_str());        khtml_elem(&hr,KELEM_BR);
	  khtml_elem(&hr,KELEM_B); khtml_puts(&hr,"Description: "); khtml_closeelem(&hr,1); khtml_puts(&hr,s.GetDescription().c_str()); khtml_elem(&hr,KELEM_BR);
	  khtml_elem(&hr,KELEM_B); khtml_puts(&hr,"Validation: ");  khtml_closeelem(&hr,1);
	  if(issues.size() == 0) {
	    khtml_puts(&hr,"Ok");
	  } else {
	    khtml_puts(&hr,"Invalid!");
	    khtml_elem(&hr, KELEM_UL);
	    for(std::string s : issues) {
	      khtml_elem(&hr, KELEM_LI); khtml_puts(&hr, s.c_str()); khtml_closeelem(&hr,1);
	    }
	    khtml_closeelem(&hr,1); // UL
	  }
	  khtml_closeelem(&hr,1); // P
 	  khtml_closeelem(&hr,2); // TD TR
	  khtml_closeelem(&hr,2); // TABLE DIV
	}

	if(yasb) {
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "YASB"); khtml_closeelem(&hr,2);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TD);
	  std::string yasbStr = converter::yasb::GetSquad(s);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, yasbStr.c_str(), KATTR__MAX); khtml_puts(&hr, yasbStr.c_str()); khtml_closeelem(&hr,1);
	  khtml_closeelem(&hr,2); // TD TR
	  khtml_closeelem(&hr,2); // TABLE DIV
	}

	if(xws) {
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "XWS"); khtml_closeelem(&hr,2);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TD);
	  std::string xwsStr = converter::xws::GetSquad(s);
	  khtml_puts(&hr, xwsStr.c_str());
	  khtml_closeelem(&hr,2); // TD TR
	  khtml_closeelem(&hr,2); // TABLE DIV
	}

	if(lj) {
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "'List Juggler' Style Printing"); khtml_closeelem(&hr,2);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TD);
	  khtml_elem(&hr, KELEM_PRE);
	  std::string ljStr = GetListJuggler(s);
	  khtml_puts(&hr, ljStr.c_str());
	  khtml_closeelem(&hr,3); // DIV TD TR
	  khtml_closeelem(&hr,2); // TABLE DIV
	}

	if(costs) {
	  struct CostEntry {
	    PtL ptl;
	    int16_t cost;
	    std::vector<std::string> squad;
	  };

	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR);  khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX); khtml_puts(&hr, "Costs"); khtml_closeelem(&hr,2);

	  // generate squad lists
	  std::vector<CostEntry> squads;
	  {
	    std::vector<std::string> cur,last;
	    const std::vector<PtL> ptls = PointList::GetPtLs();
	    for(PtL ptl : ptls) {
	      cur = GetPrintedSquad(s, ptl);
	      if(cur != last) {
		squads.push_back({ptl, *s.GetCost(ptl), cur});
	      }
	      last = cur;
	    }
	  }
	  // print them newest to oldest
	  for(std::vector<CostEntry>::reverse_iterator it = squads.rbegin(); it!=squads.rend(); ++it) {
	    khtml_elem(&hr, KELEM_TR);
	    {
	      khtml_elem(&hr, KELEM_TD);
	      khtml_puts(&hr, PointList::GetName(it->ptl).c_str());
	      khtml_elem(&hr, KELEM_BR);
	      PLDate date = PointList::GetEffectDate(it->ptl);
	      char dateStr[11];
	      snprintf(dateStr, sizeof(dateStr), "%04d-%02d-%02d", date.year, date.month, date.date);
	      khtml_puts(&hr,dateStr);
	      khtml_closeelem(&hr, 1);
	    }
	    {
	      khtml_elem(&hr, KELEM_TD);
	      khtml_puts(&hr, std::to_string(it->cost).c_str());
	      khtml_closeelem(&hr, 1);
	    }
	    {
	      khtml_elem(&hr, KELEM_TD);
	      khtml_elem(&hr, KELEM_PRE);
	      for(const std::string& line : it->squad) {
		khtml_puts(&hr, line.c_str());
	      }
	      khtml_closeelem(&hr, 2);
	    }
	    khtml_closeelem(&hr, 1);
	  }
	  khtml_closeelem(&hr,2); // TABLE DIV
	}

	/*
	if(image) {
	  std::string src;
	  //try {
	    //src = "data:image/png;base64," + GenerateImage(s);
	    src = GenerateImage(s);
	    //}
	    //catch(std::runtime_error& e) {
	    //throw;
	    //}
	  //catch(...) {
	  //}
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Image"); khtml_closeelem(&hr,2);
	  khtml_elem(&hr, KELEM_TR);  khtml_elem(&hr, KELEM_TD);
	  //std::string b64 = GenerateImage(s);
	  //std::string src = "";//"data:image/png;base64," + b64;
	  khtml_attr(&hr, KELEM_IMG, KATTR_SRC, src.c_str(), KATTR__MAX);
	  khtml_closeelem(&hr,2); // TD TR
	  khtml_closeelem(&hr,2); // TABLE DIV
	}
	*/
      }

      khtml_closeelem(&hr, 2); // div body
      khtml_close(&hr);
    } // no format

    //
    // generate json reply
    //
    else if(format=="json") {
      Json::Value root;
      bool gotSquad = true;
      Squad s;
      try {
	s = GetSquad(squad);
      }
      catch(std::runtime_error &e) {
	gotSquad = false;
	root["ERROR"] = e.what();
      }

      // if we get here, we successfully loaded the squad object 's'!
      if(gotSquad) {
	if(image) {
	  std::string imageStr = GenerateImage(s);
	  root["image"] = imageStr;
	}

	if(lj) {
	  std::string ljStr = GetListJuggler(s);
	  root["lj"] = ljStr.c_str();
	}

	if(validate) {
	  int ic=0;
	  std::vector<std::string> issues = s.Validate();
	  int issuecount = 0;
	  for(std::string i : issues) {
	    issuecount++;
	    root["validate"]["issues"][ic++] = i;
	  }
	  root["validate"]["issuecount"] = issuecount;
	}

	if(xws) {
	  Json::Value jsonxws;
	  Json::Reader reader;
	  std::string genxws = converter::xws::GetSquad(s);
	  if(!reader.parse(genxws, jsonxws)) {
	    root["ERROR"]["message"] = "error parsing json";
	    root["ERROR"]["data"] = genxws;
	  }
	  root["xws"] = jsonxws;
	}

	if(yasb) {
	  root["yasb"] = converter::yasb::GetSquad(s);
	}

	if(root.size() == 0) {
	  root["help"]["example"] = "/squad2?format=json&validate=1&xws=1&yasb=1";
	  root["help"]["instructions"] = "Post your xws or yasbURL as key 'squad' and set any number of the following options as args in your query string (the value you set it to is irrelevent)";
	  root["help"]["options"]["image"]    = "Return xhud squad image (base64 encoded)";
	  root["help"]["options"]["lj"]       = "Return ListJuggler-styled text (you need to replace the '\n' with newlines)";
	  root["help"]["options"]["validate"] = "Return squad validation info";
	  root["help"]["options"]["xws"]      = "Return xws json";
	  root["help"]["options"]["yasb"]     = "Return yasb url";
	}
      }
      
      std::string jsonStr;
      std::stringstream ss;
      ss << root;
      jsonStr = ss.str();
      resp_open(kr, KHTTP_200, "application/json");
      khttp_puts(kr, jsonStr.c_str());
    }
  } // squad != ""

  // --- dont have xws data - ask for it
  else {
    struct khtmlreq hr;

    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    khtml_elem(&hr, KELEM_P); khtml_puts(&hr, "Paste squad (xws or yasb URL) here, and press Submit."); khtml_closeelem(&hr, 1);

    khtml_attr(&hr, KELEM_FORM, KATTR_ACTION, (std::string(kr->pname)+"/squad2").c_str(), KATTR_METHOD, "POST", KATTR__MAX);
    khtml_attr(&hr, KELEM_TEXTAREA, KATTR_NAME, "squad", KATTR_ROWS, "10", KATTR_COLS, "80", KATTR__MAX); khtml_closeelem(&hr, 1);             khtml_elem(&hr, KELEM_BR);
    //khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "checkbox", KATTR_NAME, "image",    KATTR_VALUE, "1", KATTR__MAX); khtml_puts(&hr, "Image");    khtml_elem(&hr, KELEM_BR);
    //khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "checkbox", KATTR_NAME, "lj",       KATTR_VALUE, "1", KATTR__MAX); khtml_puts(&hr, "LJ");       khtml_elem(&hr, KELEM_BR);
    //khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "checkbox", KATTR_NAME, "validate", KATTR_VALUE, "1", KATTR__MAX); khtml_puts(&hr, "Validate"); khtml_elem(&hr, KELEM_BR);
    //khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "checkbox", KATTR_NAME, "xws",      KATTR_VALUE, "1", KATTR__MAX); khtml_puts(&hr, "XWS");      khtml_elem(&hr, KELEM_BR);
    //khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "checkbox", KATTR_NAME, "yasb",     KATTR_VALUE, "1", KATTR__MAX); khtml_puts(&hr, "YASB");     khtml_elem(&hr, KELEM_BR);
    khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "Submit", KATTR__MAX);
    khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "hidden", KATTR_NAME, PageToken::PointList.c_str(), KATTR_VALUE, PointList::GetPtLStr(pt.GetPtL()).c_str(), KATTR__MAX);
    khtml_closeelem(&hr, 1); // form

    khtml_elem(&hr, KELEM_P);
    khtml_puts(&hr, "To use this from your web app:");
    khtml_elem(&hr, KELEM_BR);
    khtml_puts(&hr, "POST to ");
    khtml_elem(&hr, KELEM_U); khtml_puts(&hr, "https://xhud.sirjorj.com/xwing.cgi/squad2?format=json"); khtml_closeelem(&hr, 1);
    khtml_puts(&hr, " with your xws or yasb as a key called ");
    khtml_elem(&hr, KELEM_U); khtml_puts(&hr, "squad"); khtml_closeelem(&hr, 1);
    khtml_puts(&hr, ".");
    khtml_elem(&hr, KELEM_BR);
    khtml_puts(&hr, "What is returned depends on which attributes are set.");
    khtml_elem(&hr, KELEM_BR);
    khtml_elem(&hr, KELEM_BR);
    khtml_puts(&hr, "NOTE: 'image' is currently not working");
    khtml_elem(&hr, KELEM_BR);
    // arguments
    khtml_elem(&hr, KELEM_TABLE);
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "Attributes"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "xws"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "XWS text of the squad (NOTE: this is generated by libxwing and may not match the input xws - if you input xws without costs, this will have them)"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "yasb"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "YASB URL of squad (NOTE: this is generated by libxwing)"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "lj"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "Pretty-printed list in the style of List Juggler"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "validate"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "Validate the list and print any issues"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "image"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "A squad image (PNG) generated by xhud in Base64"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1); // TABLE
    // examples
    khtml_elem(&hr, KELEM_TABLE);
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "Examples"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Get Everything"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "https://xhud.sirjorj.com/xwing.cgi/squad2?format=json&xws=1&yasb=1&lj=1&validate=1&image=1"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TR);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Validate and LJ"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "https://xhud.sirjorj.com/xwing.cgi/squad2?format=json&lj=1&validate=1&image=1"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1); // TABLE
    
    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

}
