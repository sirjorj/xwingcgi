#include "inventory2.h"
#include "pagetoken2.h"
#include <libxwing2/xwi.h>
#include <sstream>

using namespace libxwing2;

static bool StartsWith(const std::string& str, const std::string& prefix) {
  return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
}

static std::string GetVal(std::string str) {
  return str.substr(str.find("_")+1);
}

static XWI GetXWI(const PageToken& pt) {
  XWI xwi;
  if(pt.HasArg(PageToken::XWI)) {
    std::istringstream ss(pt.Get(PageToken::XWI));
    return XWI(ss);
  }
  for(const std::pair<const std::string,std::string>& a : pt.GetArgs()) {
    // if we get xwi (json), build an object from it and return it
    // otherwise we read the header data and populate an object with it
    if(StartsWith(a.first, "expansion_")) {
      std::string expStr = GetVal(a.first);
      Release release = Release::GetRelease(expStr);
      int val = std::stoi(a.second);
      xwi.Set(release.GetRel(), val);
    }
    else if(StartsWith(a.first, "ship_")) {
      std::string expStr = GetVal(a.first);
      Ship ship = Ship::GetShip(converter::xws::GetShip(expStr));
      int val = std::stoi(a.second);
      xwi.Set(ship.GetShp(), val);
    }
    else if(StartsWith(a.first, "pilot_")) {
      std::string expStr = GetVal(a.first);
      Pilot pilot = Pilot::GetPilot(converter::xws::GetPilot(expStr));
      int val = std::stoi(a.second);
      xwi.Set(pilot.GetPlt(), val);
    }
    else if(StartsWith(a.first, "upgrade_")) {
      std::string expStr = GetVal(a.first);
      Upgrade upgrade = Upgrade::GetUpgrade(converter::xws::GetUpgrade(expStr));
      int val = std::stoi(a.second);
      xwi.Set(upgrade.GetUpg(), val);
    }
  }
  return xwi;
}

static std::map<Fac, std::map<Shp,std::vector<Pilot>>> GetPilotsByFacShpPlt() {
  std::map<Fac, std::map<Shp,std::vector<Pilot>>> data;
  for(Plt plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if(!pilot.IsUnreleased()) {
      data[pilot.GetFac()][pilot.GetShp()].push_back(pilot);
    }
  }
  return data;
}

static std::map<UpT, std::vector<Upgrade>> GetUpgradesByType() {
  std::map<UpT, std::vector<Upgrade>> ret;
  for(Upg upg : Upgrade::GetAllUpgs()) {
    Upgrade upgrade = Upgrade::GetUpgrade(upg);
    ret[upgrade.GetUpT()].push_back(upgrade);
  }
  return ret;
}

static TokenCollection GetTokens(const XWI &xwi) {
  TokenCollection ret;
  for(Rel rel : Release::GetAllRels()) {
    Release release = Release::GetRelease(rel);    
    int count = xwi.Get(rel);
    for(int i=0; i<count; i++) {
      ret += release.GetTokens();
    }
  }
  return ret;
}



void inventory2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string mode = pt.Get(PageToken::Mode);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    css.Inventory();
    css.ShipRow();
    css.Pilots();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {{kr->pname, StripLeadingSlash(kr->pname)}}, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

#if 0
  // DEBUG
  khtml_elem(&hr, KELEM_UL);
  for(int i=0; i<kr->fieldsz; i++) {
    khtml_elem(&hr, KELEM_LI);
    std::string x = std::string(kr->fields[i].key) + ": " + kr->fields[i].val;
    khtml_puts(&hr, x.c_str());
    khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 1);
  // /DEBUG
#endif

  //std::istringstream ss(xwiStr);
  try {
    XWI xwi = GetXWI(pt);

    if(mode == "") {
      // the big textbox for pasting the xwi json
      khtml_attr(&hr, KELEM_FORM, KATTR_ACTION, (std::string(kr->pname)+"/inventory2").c_str(), KATTR_METHOD, "POST", KATTR__MAX);
      khtml_attr(&hr, KELEM_TEXTAREA, KATTR_NAME, "xwi", KATTR_PLACEHOLDER, "Paste xwi here", KATTR_ROWS, "10", KATTR_COLS, "80", KATTR__MAX);
      khtml_puts(&hr, xwi.IsEmpty() ? "" : xwi.Serialize(true).c_str());
      khtml_closeelem(&hr, 1);
      khtml_elem(&hr, KELEM_BR);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "radio", KATTR_NAME, "mode", KATTR_VALUE, "View", KATTR_CHECKED, "Checked", KATTR__MAX); khtml_puts(&hr, "View");
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "radio", KATTR_NAME, "mode", KATTR_VALUE, "Edit", KATTR__MAX);                           khtml_puts(&hr, "Edit");
      khtml_elem(&hr, KELEM_BR);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "Submit", KATTR__MAX);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "hidden", KATTR_NAME, PageToken::PointList.c_str(), KATTR_VALUE, PointList::GetPtLStr(pt.GetPtL()).c_str(), KATTR__MAX);
      khtml_closeelem(&hr, 1); // form

      khtml_elem(&hr, KELEM_HR);

      khtml_elem(&hr, KELEM_PRE);
      khtml_puts(&hr, "xwi spec\n");
      khtml_puts(&hr, "--------\n");
      khtml_puts(&hr, "JSON Elements:\n");
      khtml_puts(&hr, "  \"expansions\" - contains an array of { \"sku\": \"_x_\", \"count\": _n_ }\n");
      khtml_puts(&hr, "      - _x_: expansion SKU (ex \"SWZ01\")\n");
      khtml_puts(&hr, "      - _n_: number of them in the collection (zero or greater)\n");
      khtml_puts(&hr, "  \"ships\" - contains an array of { \"ship\": \"_x_\", \"count\": _n_ }\n");
      khtml_puts(&hr, "      - _x_: xws name of a ship (ex \"btla4ywing\")\n");
      khtml_puts(&hr, "      - _n_: number of them in the collection (negative, zero, or positive)\n");
      khtml_puts(&hr, "  \"pilots\" - contains an array of { \"pilot\": \"_x_\", \"count\": _n_ }\n");
      khtml_puts(&hr, "      - _x_: xws name of a pilot (ex \"leiaorgana\")\n");
      khtml_puts(&hr, "      - _n_: number of them in the collection (negative, zero, or positive)\n");
      khtml_puts(&hr, "  \"upgrades\" - contains an array of { \"upgrade\": \"_x_\", \"count\": _n_ }\n");
      khtml_puts(&hr, "      - _x_: xws name of an upgrade (ex \"r2astromech\")\n");
      khtml_puts(&hr, "      - _n_: number of them in the collection (negative, zero, or positive)\n");
      khtml_puts(&hr, "\n");
      khtml_puts(&hr, "Notes:\n");
      khtml_puts(&hr, "  -Items with a count of zero may be excluded\n");
      khtml_puts(&hr, "    -They can also be left in if you want the make the json easier to edit manually\n");
      khtml_puts(&hr, "  -ships/pilots/upgrades count may be nagative to handle the case of someone getting an expansion but getting rid of part of it\n");
      khtml_puts(&hr, "  -per json rules, the order of the elements may vary\n");
      khtml_puts(&hr, "\n");
      khtml_puts(&hr, "Example:\n");

      khtml_puts(&hr, "{\n");
      khtml_puts(&hr, "  \"expansions\": [\n");
      khtml_puts(&hr, "    { \"sku\": \"SWZ01\", \"count\": 1 },\n");
      khtml_puts(&hr, "    { \"sku\": \"SWZ06\", \"count\": 1 }\n");
      khtml_puts(&hr, "  ],\n");
      khtml_puts(&hr, "  \"pilots\": [\n");
      khtml_puts(&hr, "    { \"pilot\": \"leiaorgana\", \"count\": 1 }\n");
      khtml_puts(&hr, "  ],\n");
      khtml_puts(&hr, "  \"ships\": [\n");
      khtml_puts(&hr, "    { \"ship\": \"btla4ywing\", \"count\": 1 },\n");
      khtml_puts(&hr, "    { \"ship\": \"modifiedyt1300lightfreighter\", \"count\": 1 },\n");
      khtml_puts(&hr, "    { \"ship\": \"t65xwing\", \"count\": 1 }\n");
      khtml_puts(&hr, "  ],\n");
      khtml_puts(&hr, "  \"upgrades\": [\n");
      khtml_puts(&hr, "    { \"upgrade\": \"r2astromech\", \"count\": -1 }\n");
      khtml_puts(&hr, "  ]\n");
      khtml_puts(&hr, "}\n");

      khtml_closeelem(&hr, 1);
    }

    if(mode == "Edit") {
      // the number controls for all the individual things
      khtml_attr(&hr, KELEM_P, KATTR__MAX);
      khtml_puts(&hr, "The 'Ships', 'Pilots', and 'Upgrades' categories are separate from expansions.  In other words, if you get an expansion, add it to the 'Expansions' category but don't add the individual components to the other categories.");
      khtml_closeelem(&hr, 1); // p
      khtml_attr(&hr, KELEM_P, KATTR__MAX);
      khtml_puts(&hr, "Items in the 'Ships', 'Pilots', and 'Upgrades' categories can be set to negative numbers.  This is for if you get an expansion but get rid of some of the components.");
      khtml_closeelem(&hr, 1); // p

      khtml_attr(&hr, KELEM_FORM, KATTR_ACTION, (std::string(kr->pname)+"/inventory2").c_str(), KATTR_METHOD, "POST", KATTR__MAX);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "Submit", KATTR__MAX);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "hidden", KATTR_NAME, PageToken::PointList.c_str(), KATTR_VALUE, PointList::GetPtLStr(pt.GetPtL()).c_str(), KATTR__MAX);
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);
      { // expansions
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR__MAX);
	khtml_puts(&hr, "Expansions");
	khtml_closeelem(&hr, 2); // th tr
	for(Rel rel : Release::GetAllRels()) {
	  Release release = Release::GetRelease(rel);
	  std::string label = release.GetSku() + " (" + release.GetName() + ")";
	  std::string name = "expansion_" + release.GetSku();
	  int value = xwi.Get(release.GetRel());
	  khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	  khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	  khtml_attr(&hr, KELEM_INPUT,
		     KATTR_TYPE, "number",
		     KATTR_NAME, name.c_str(),
		     KATTR_ID, name.c_str(),
		     KATTR_MIN, "0",
		     KATTR_MAX, "50",
		     KATTR_VALUE, std::to_string(value).c_str(),
		     KATTR__MAX);
	  khtml_attr(&hr, KELEM_LABEL, KATTR_FOR, name.c_str(), KATTR__MAX); khtml_puts(&hr, label.c_str()); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 2); // td tr
	}
	khtml_closeelem(&hr, 2); // table div
      }

      { // ships
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	khtml_puts(&hr, "Ships");
	khtml_closeelem(&hr, 2); // th tr
	for(Shp shp : Ship::GetAllShps()) {
	  Ship ship = Ship::GetShip(shp);
	  std::string label = ship.GetName();
	  std::string name = "ship_" + converter::xws::GetShip(ship.GetShp());
	  int value = xwi.Get(ship.GetShp());
	  khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipIcon", KATTR__MAX);
	  khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ship.GetName().c_str(), KATTR__MAX);
	  khtml_puts(&hr, ShipGlyph::GetGlyph(ship.GetShp()).c_str());
	  khtml_closeelem(&hr, 2); // span td
  	  khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	  khtml_attr(&hr, KELEM_INPUT,
		     KATTR_TYPE, "number",
		     KATTR_NAME, name.c_str(),
		     KATTR_ID, name.c_str(),
		     KATTR_MIN, "-50",
		     KATTR_MAX, "50",
		     KATTR_VALUE, std::to_string(value).c_str(),
		     KATTR__MAX);
	  khtml_attr(&hr, KELEM_LABEL, KATTR_FOR, name.c_str(), KATTR__MAX); khtml_puts(&hr, label.c_str()); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 2); // td tr
	}
	khtml_closeelem(&hr, 2); // table div
      }

      { // pilots
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	khtml_puts(&hr, "Pilots");
	khtml_closeelem(&hr, 2); // th tr
	std::map<Fac, std::map<Shp,std::vector<Pilot>>> data = GetPilotsByFacShpPlt();
	for(const std::pair<const Fac, std::map<Shp,std::vector<Pilot>>> &d1 : data) {
	  Faction faction = Faction::GetFaction(d1.first);
	  khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	  khtml_puts(&hr, faction.GetName().c_str());
	  khtml_closeelem(&hr, 2); // td tr
	  for(const std::pair<const Shp,std::vector<Pilot>> &d2 : d1.second) {
	    bool drawShip = true;
	    for(const Pilot& pilot : d2.second) {
	      khtml_elem(&hr, KELEM_TR);
	      if(drawShip) {
		khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(d2.second.size()).c_str(), KATTR__MAX);
		khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, Ship::GetShip(d2.first).GetName().c_str(), KATTR__MAX);
		khtml_puts(&hr, ShipGlyph::GetGlyph(d2.first).c_str());
		khtml_closeelem(&hr, 2); // span td
		drawShip=false;
	      }
	      std::string label = pilot.GetName();
	      std::string name = "pilot_" + converter::xws::GetPilot(pilot.GetPlt());
	      int value = xwi.Get(pilot.GetPlt());
	      khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	      khtml_attr(&hr, KELEM_INPUT,
			 KATTR_TYPE, "number",
			 KATTR_NAME, name.c_str(),
			 KATTR_MIN, "-100",
			 KATTR_MAX, "100",
			 KATTR_VALUE, std::to_string(value).c_str(),
			 KATTR__MAX);
	      khtml_attr(&hr, KELEM_LABEL, KATTR_FOR, name.c_str(), KATTR__MAX); khtml_puts(&hr, label.c_str()); khtml_closeelem(&hr, 1);
	      khtml_closeelem(&hr, 2); // td tr
	    }
	  }
	}
	khtml_closeelem(&hr, 2); // table div
      }

      { // upgrades
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR__MAX);
	khtml_puts(&hr, "Upgrades");
	khtml_closeelem(&hr, 2); // th tr
	std::map<UpT, std::vector<Upgrade>> data = GetUpgradesByType();
	for(const std::pair<const UpT, std::vector<Upgrade>> &p : data) {
	  khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	  khtml_puts(&hr, UpgradeType::GetUpgradeType(p.first).GetName().c_str());
	  khtml_closeelem(&hr, 2); // td tr
	  for(const Upgrade &upgrade : p.second) {
	    if(upgrade.IsUnreleased()) { continue; }
	    std::string label = upgrade.GetName();
	    std::vector<Fac> aFacs = upgrade.GetRestriction().GetAllowedFactions();
	    if(aFacs.size()) {
	      label += " (";
	      bool first = true;
	      for(Fac fac : aFacs) {
		Faction faction = Faction::GetFaction(fac);
		if(first) { first = false; }
		else      { label += ",";  }		  
		label += faction.Get3Letter();
	      }
	      label += ")";
	    }
	    std::string name = "upgrade_" + converter::xws::GetUpgrade(upgrade.GetUpg());
	    int value = xwi.Get(upgrade.GetUpg());
	    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	    khtml_attr(&hr, KELEM_INPUT,
		       KATTR_TYPE, "number",
		       KATTR_NAME, name.c_str(),
		       KATTR_MIN, "-100",
		       KATTR_MAX, "100",
		       KATTR_VALUE, std::to_string(value).c_str(),
		       KATTR__MAX);
	    khtml_attr(&hr, KELEM_LABEL, KATTR_FOR, name.c_str(), KATTR__MAX); khtml_puts(&hr, label.c_str()); khtml_closeelem(&hr, 1);
	    khtml_closeelem(&hr, 2); // td tr
	  }
	}
	khtml_closeelem(&hr, 2); // table div
      }
      khtml_closeelem(&hr, 1); // div (container)
    }

    else if(mode == "View") {
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

      // json
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
      khtml_puts(&hr, "xwi (full)");
      khtml_elem(&hr, KELEM_BR);
      khtml_attr(&hr, KELEM_TEXTAREA, KATTR_NAME, "xwi", KATTR_PLACEHOLDER, "Paste xwi here", KATTR_ROWS, "10", KATTR_COLS, "80", KATTR_READONLY, "", KATTR__MAX);
      khtml_puts(&hr, xwi.Serialize(false).c_str());
      khtml_closeelem(&hr, 2); // textarea div

      // json (shrink)
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
      khtml_puts(&hr, "xwi (minimal)");
      khtml_elem(&hr, KELEM_BR);
      khtml_attr(&hr, KELEM_TEXTAREA, KATTR_NAME, "xwi", KATTR_PLACEHOLDER, "Paste xwi here", KATTR_ROWS, "10", KATTR_COLS, "80", KATTR_READONLY, "", KATTR__MAX);
      khtml_puts(&hr, xwi.Serialize(true).c_str());
      khtml_closeelem(&hr, 2); // textarea div

      khtml_closeelem(&hr, 1); // div (container)

      khtml_elem(&hr, KELEM_HR);

      // component breakdown
      khtml_puts(&hr, "Component Breakdown");
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

      const XWI inv = xwi.GetResolved();
      
      { // ships
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	khtml_puts(&hr, "Ships");
	khtml_closeelem(&hr, 2); // th tr
	for(Shp shp : Ship::GetAllShps()) {
	  int c = inv.Get(shp);
	  if(c) {
	    Ship ship = Ship::GetShip(shp);
	    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(c).c_str()); khtml_closeelem(&hr, 1);
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipIcon", KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ship.GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, ShipGlyph::GetGlyph(ship.GetShp()).c_str());
	    khtml_closeelem(&hr, 2); // span td
	    khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, ship.GetName().c_str()); khtml_closeelem(&hr, 1);
	    khtml_closeelem(&hr, 1);
	  }
	}
	khtml_closeelem(&hr, 2); // table div(ships)
      }

      { // pilots
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	khtml_puts(&hr, "Pilots");
	khtml_closeelem(&hr, 2); // th tr
	static std::map<Fac, std::map<Shp,std::vector<Pilot>>> data = GetPilotsByFacShpPlt();
	for(const std::pair<const Fac, std::map<Shp,std::vector<Pilot>>> &d1 : data) {
	  bool printedFac = false;
	  for(const std::pair<const Shp,std::vector<Pilot>> &d2 : d1.second) {
	    bool printedShip = false;
	    int count=0;
	    {
	      for(const Pilot& pilot : d2.second) {
		if(inv.Get(pilot.GetPlt())) {
		  count++;
		}
	      }
	    }
     	    for(const Pilot& pilot : d2.second) {
	      if(pilot.IsUnreleased() || !inv.Get(pilot.GetPlt())) { continue; }
              if(!printedFac) {
		printedFac = true;
		Faction faction = Faction::GetFaction(d1.first);
		khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
		khtml_puts(&hr, faction.GetName().c_str());
		khtml_closeelem(&hr, 2); // td tr
	      }
	      khtml_elem(&hr, KELEM_TR);
	      if(!printedShip) {
		printedShip = true;
		khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(count).c_str(), KATTR__MAX);
		khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, Ship::GetShip(d2.first).GetName().c_str(), KATTR__MAX);
		khtml_puts(&hr, ShipGlyph::GetGlyph(d2.first).c_str());
		khtml_closeelem(&hr, 2); // span td
	      }
	      int value = inv.Get(pilot.GetPlt());
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(value).c_str()); khtml_closeelem(&hr, 1);
	      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, pilot.GetName().c_str()); khtml_closeelem(&hr, 1);
	      khtml_closeelem(&hr, 1); // tr
	    }
	  }
	}
	khtml_closeelem(&hr, 2); // table div(pilots)
      }

      { // upgrades
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	khtml_puts(&hr, "Upgrades");
	khtml_closeelem(&hr, 2); // th tr
	std::map<UpT, std::vector<Upgrade>> data = GetUpgradesByType();
	for(const std::pair<const UpT, std::vector<Upgrade>> &p : data) {
	  bool printedType = false;
	  for(const Upgrade &upgrade : p.second) {
	    if(upgrade.IsUnreleased() || !inv.Get(upgrade.GetUpg())) { continue; }
	    if(!printedType) {
	      printedType = true;
	      khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX);
	      khtml_puts(&hr, UpgradeType::GetUpgradeType(p.first).GetName().c_str());
	      khtml_closeelem(&hr, 2); // th tr
	    }
	    std::string label = upgrade.GetName();
	    std::vector<Fac> aFacs = upgrade.GetRestriction().GetAllowedFactions(); 
	    if(aFacs.size()) {
	      label += " (";
	      bool first = true;
	      for(Fac fac : aFacs) {
		Faction faction = Faction::GetFaction(fac);
		if(first) { first = false; }
		else      { label += ",";  }
		label += faction.Get3Letter();
	      }
	      label += ")";
	    }
	    int value = inv.Get(upgrade.GetUpg());
	    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(value).c_str()); khtml_closeelem(&hr, 1);
	    khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, upgrade.GetName().c_str()); khtml_closeelem(&hr, 1);
    	    khtml_closeelem(&hr, 1); // tr
	  }
	}
	khtml_closeelem(&hr, 2); // table div
      }

      { // tokens
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR__MAX); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "3", KATTR__MAX);
	khtml_puts(&hr, "Tokens");
	khtml_closeelem(&hr, 2); // th tr
	TokenCollection tc = GetTokens(xwi); // do not send the resolved version - we need the releases!
	std::map<Tok,std::shared_ptr<Tokens>> ts = tc.GetAllTokens();
	for(std::pair<Tok,std::shared_ptr<Tokens>> t : ts) {
	  Tok tok = t.first;
	  std::shared_ptr<Tokens> tokensSP = t.second;
	  Tokens tokens = *tokensSP.get();
	  Token token = Token::GetToken(tok);
	  int count = tokens.GetCount();

	  khtml_attr(&hr, KELEM_TR, KATTR__MAX);
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(count).c_str()); khtml_closeelem(&hr, 1);
	  khtml_attr(&hr, KELEM_TD, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, token.GetName().c_str()); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // tr

	  switch(tok) {
	  case Tok::ID:
	  case Tok::Lock:
	    {
	      IDTokens idts = tc.GetIDTokens(tok);
	      for(const std::pair<const int,int> &idt : idts.GetIDCounts()) {
		int id = idt.first;
		int count = idt.second;
		khtml_attr(&hr, KELEM_TR, KATTR__MAX);
		khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_closeelem(&hr, 1);
		khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(count).c_str()); khtml_closeelem(&hr, 1);
		khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, std::to_string(id).c_str()); khtml_closeelem(&hr, 1);
		khtml_closeelem(&hr, 1); // tr
	      }
	    }
	    break;

	    case Tok::SmallSingleTurret:
	    case Tok::SmallDoubleTurret:
	    case Tok::LargeSingleTurret:
	    case Tok::LargeDoubleTurret:
	    case Tok::HugeSingleTurret:
	    case Tok::HugeDoubleTurret:
	    {
	      TurretTokens tts = tc.GetTurretTokens(tok);
	      for(const std::pair<const std::optional<Fac>,int> &tt : tts.GetFacCounts()) {
		std::optional<Fac> fac= tt.first;
		int count = tt.second;
		khtml_attr(&hr, KELEM_TR, KATTR__MAX);
		khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_closeelem(&hr, 1);
		khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "invCount", KATTR__MAX); khtml_puts(&hr, std::to_string(count).c_str()); khtml_closeelem(&hr, 1);
		khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, fac ? Faction::GetFaction(*fac).GetShort().c_str() : ""); khtml_closeelem(&hr, 1);
		khtml_closeelem(&hr, 1); // tr
	      }
	    }
    	    break;

	  default:;
    	  }
	}
	khtml_closeelem(&hr, 2); // table div
      }
      khtml_closeelem(&hr, 1); // div (container)
    }
  }
  catch(std::runtime_error& err) {
    khtml_puts(&hr, err.what());
  }
  catch(...) {
    khtml_puts(&hr, "Unknown error");
  }
  khtml_closeelem(&hr, 1); // body

  khtml_close(&hr);
}
