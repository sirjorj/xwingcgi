#include "environment2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void environment2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  Environment environment = Environment::GetEnvironment(pt.Get(PageToken::Name));

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + environment.GetName() + "]", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			    {LinkBuilder(pt,"environments2").AddArg(PageToken::PointList).Get(), "environments2"}
    }, environment.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

  khtml_elem(&hr, KELEM_TABLE);

  { // name
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Name"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, environment.GetName().c_str()); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);
  }
  { // obstacles
    if(!environment.GetObstacles().empty()) {
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Obstacles"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, environment.GetObstacles().c_str()); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }    
  }
  { // devices
    if(!environment.GetDevices().empty()) {
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Devices"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, environment.GetDevices().c_str()); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }    
  }
  { // setup
    if(!environment.GetSetup().empty()) {
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Setup"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX);
      khtml_elem(&hr, KELEM_P); IconifyText(hr, environment.GetSetup()); khtml_closeelem(&hr, 1);
      if(environment.GetSetupPoints().size()) {
	khtml_elem(&hr, KELEM_UL);
	for(const std::string &p : environment.GetSetupPoints()) {
	  khtml_elem(&hr, KELEM_LI);
	  IconifyText(hr, p);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }
      khtml_closeelem(&hr, 1); // td
      khtml_closeelem(&hr, 1); // tr
    }    
  }
  { // special rule
    if(!environment.GetSpecialRule().empty()) {
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Special Rule"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX);
      khtml_elem(&hr, KELEM_P); IconifyText(hr, environment.GetSpecialRule()); khtml_closeelem(&hr, 1);
      if(environment.GetSpecialRulePoints().size()) {
	khtml_elem(&hr, KELEM_UL);
	for(const std::string &p : environment.GetSpecialRulePoints()) {
	  khtml_elem(&hr, KELEM_LI);
	  IconifyText(hr, p);
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1);
      }
      khtml_closeelem(&hr, 1); // td
      khtml_closeelem(&hr, 1); // tr
    }    
  }
  { // availability
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Availability"); khtml_closeelem(&hr, 1);
    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
    bool first=true;
    for(Rel rel : Release::GetAllRels()) {
      Release release = Release::GetRelease(rel);
      for(Env e : release.GetEnvironments()) {
	if(e == environment.GetEnv()) {
	  if(first) { first = false; }
	  else      { khtml_elem(&hr, KELEM_BR); }
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,release.GetSku()).Get().c_str(), KATTR__MAX);
	  khtml_puts(&hr, release.GetName().c_str());
	  khtml_closeelem(&hr, 1);
	  break;
	}
      }
    }
    khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 1); // table
  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
