#include "colors2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void colors2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);
  { // head
    CSS css;
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
  }
  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
    }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));
  // colors
  khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
  for(Clr clr : Color::GetAllClrs()) {
    Color color = Color::GetColor(clr);
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    // name
    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
    khtml_puts(&hr, color.GetName().c_str());
    khtml_closeelem(&hr, 1);
    // color
    khtml_attr(&hr, KELEM_TD, KATTR_STYLE, "font-family:monospace;", KATTR__MAX);
    khtml_puts(&hr, color.GetHex().c_str());
    khtml_closeelem(&hr, 1);
    // example
    std::string style = "background-color:#" + color.GetHex() + ";width:100px;";
    khtml_attr(&hr, KELEM_TD, KATTR_STYLE, style.c_str(), KATTR__MAX);
    khtml_closeelem(&hr, 1);
    // done
    khtml_closeelem(&hr, 1); // row
  }
  khtml_closeelem(&hr, 1); // table

  khtml_elem(&hr, KELEM_P);
  khtml_puts(&hr, "These come from a number of sources, including the official squadbuilder site, images from official site, and scans I made.  If you have more accurate colors or additional ones that I forgot, please let me know! :)");
  khtml_closeelem(&hr, 1);

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
