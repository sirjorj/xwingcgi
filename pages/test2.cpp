#include "test2.h"
#include <kcgixml.h>

using namespace libxwing2;



void test2(struct kreq *kr) {
  PageToken pt(kr);
  std::string subject="";
  for(int i=0; i<kr->fieldsz; i++) {
    if(strcmp(kr->fields[i].key, "subject")  == 0)  { subject = kr->fields[i].val; }
  }

  if(subject != subject) {
  }

  else if(subject=="comparepilotcards") {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);
    // head
    CSS css;
    css.Maneuvers();
    css.PilotInfo();
    css.PilotCard();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {{kr->pname, StripLeadingSlash(kr->pname)}}, kr->pagename);

    khtml_elem(&hr, KELEM_TABLE);
    for(Plt plt : Pilot::GetAllPlts()) {
      Pilot pilot = Pilot::GetPilot(plt);
      khtml_attr(&hr, KELEM_TR, KATTR_CLASS, pilot.IsUnreleased() ? "unr" : "", KATTR__MAX);

      khtml_elem(&hr, KELEM_TD);
      PrintPilotCard(hr, pt, pilot);
      khtml_closeelem(&hr, 1); // td (my card)

      khtml_elem(&hr, KELEM_TD);
      std::string imgUrl="";
      try {
	imgUrl = converter::ffg::GetCard(pilot.GetPlt()).c_str();
      }
      catch(converter::ffg::UpgradeNotFound &unf) {
      }
      if(imgUrl != "") {
	khtml_attr(&hr, KELEM_IMG, KATTR_SRC, imgUrl.c_str(), KATTR__MAX);
      }
      khtml_closeelem(&hr, 1); // td (image)

      khtml_closeelem(&hr, 1); // tr
    }
    khtml_closeelem(&hr, 1); // table

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else if(subject=="compareupgradecards") {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);
    // head
    CSS css;
    css.UpgradeInfo();
    css.UpgradeCard();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {{kr->pname, StripLeadingSlash(kr->pname)}}, kr->pagename);

    khtml_elem(&hr, KELEM_TABLE);
    for(Upg upg : Upgrade::GetAllUpgs()) {
      Upgrade upgrade = Upgrade::GetUpgrade(upg);
      khtml_attr(&hr, KELEM_TR, KATTR_CLASS, upgrade.IsUnreleased() ? "unr" : "", KATTR__MAX);

      khtml_elem(&hr, KELEM_TD);
      PrintUpgradeCard(hr, pt, upgrade);
      if(upgrade.IsDualSided()) {
	upgrade.Flip();
	PrintUpgradeCard(hr, pt, upgrade);
      }
      khtml_closeelem(&hr, 1); // td (my card)

      khtml_elem(&hr, KELEM_TD);
      std::string imgUrl="";
      try {
	imgUrl = converter::ffg::GetCard(upgrade.GetUpg(), 0).c_str();
      }
      catch(converter::ffg::UpgradeNotFound &unf) {
      }
      if(imgUrl != "") {
	khtml_attr(&hr, KELEM_IMG, KATTR_SRC, imgUrl.c_str(), KATTR__MAX);
      }
      if(upgrade.IsDualSided()) {
	imgUrl = "";
	try {
	  imgUrl = converter::ffg::GetCard(upgrade.GetUpg(), 1).c_str();
	}
	catch(converter::ffg::UpgradeNotFound &unf) {
	}
	if(imgUrl != "") {
	  khtml_attr(&hr, KELEM_IMG, KATTR_SRC, imgUrl.c_str(), KATTR__MAX);
	}
      }
      khtml_closeelem(&hr, 1); // td (image)

      khtml_closeelem(&hr, 1); // tr
    }
    khtml_closeelem(&hr, 1); // table

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);
    // head
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath);
    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {{kr->pname, StripLeadingSlash(kr->pname)}}, kr->pagename);

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }


  
}
