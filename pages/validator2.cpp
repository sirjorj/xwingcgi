#include "validator2.h"



void validator2(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  khtml_elem(&r, KELEM_P);
  khtml_puts(&r, "This has been replaced by ");
  khtml_attr(&r, KELEM_A, KATTR_HREF, "squad2", KATTR__MAX);
  khtml_puts(&r, "squad2");
  khtml_closeelem(&r, 2); // a p

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
