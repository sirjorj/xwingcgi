#include "dialcomp.h"
#include "pagetoken2.h"
#include "xwingcgi.h"
#include "xwingcgi2.h"



void dialcomp(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    libxwing2::CSS css;
    css.DialComp();
    PrintHead(kr, hr, std::string(kr->pname) + "/dialcomp", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
    }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

  std::map<libxwing2::Shp, libxwing::Shp> FEShips {
    { libxwing2::Shp::Aggressor,        libxwing::Shp::IG2000 },
    { libxwing2::Shp::AlphaClass,       libxwing::Shp::AlphaClassStarWing },
    { libxwing2::Shp::ARC170,           libxwing::Shp::ARC170 },
    { libxwing2::Shp::ASF01BWing,       libxwing::Shp::BWing },
    { libxwing2::Shp::AttackShuttle,    libxwing::Shp::AttackShuttle },
    { libxwing2::Shp::Auzituck,         libxwing::Shp::AuzituckGunship },
    { libxwing2::Shp::BTLA4YWing,       libxwing::Shp::YWing },
    { libxwing2::Shp::BTLS8KWing,       libxwing::Shp::KWing },
    { libxwing2::Shp::EWing,            libxwing::Shp::EWing },
    { libxwing2::Shp::FangFighter,      libxwing::Shp::ProtectorateStarfighter },
    { libxwing2::Shp::Firespray,        libxwing::Shp::Firespray31 },
    { libxwing2::Shp::G1A,              libxwing::Shp::G1A },
    { libxwing2::Shp::HWK290,           libxwing::Shp::HWK290 },
    { libxwing2::Shp::JM5K,             libxwing::Shp::JumpMaster5000 },
    { libxwing2::Shp::Kihraxz,          libxwing::Shp::Kihraxz },
    { libxwing2::Shp::Lambda,           libxwing::Shp::LambdaClassShuttle },
    { libxwing2::Shp::LancerClass,      libxwing::Shp::LancerClassPursuitCraft },
    { libxwing2::Shp::M12LKimogila,     libxwing::Shp::M12LKimogila },
    { libxwing2::Shp::M3A,              libxwing::Shp::M3AInterceptor },
    { libxwing2::Shp::MG100,            libxwing::Shp::BSF17Bomber },
    { libxwing2::Shp::ModYT1300,        libxwing::Shp::YT1300 },
    { libxwing2::Shp::Quadjumper,       libxwing::Shp::Quadjumper },
    { libxwing2::Shp::RZ1AWing,         libxwing::Shp::AWing },
    { libxwing2::Shp::Scurrg,           libxwing::Shp::ScurrgH6Bomber },
    { libxwing2::Shp::Sheathipede,      libxwing::Shp::SheathipedeClassShuttle },
    { libxwing2::Shp::StarViper,        libxwing::Shp::StarViper },
    { libxwing2::Shp::T65XWing,         libxwing::Shp::XWing },
    { libxwing2::Shp::T70XWing,         libxwing::Shp::T70XWing },
    { libxwing2::Shp::TIEAdvV1,         libxwing::Shp::TIEAdvancedPrototype },
    { libxwing2::Shp::TIEAdvX1,         libxwing::Shp::TIEAdvanced },
    { libxwing2::Shp::TIEagAggressor,   libxwing::Shp::TIEAggressor },
    { libxwing2::Shp::TIEcaPunisher,    libxwing::Shp::TIEPunisher },
    { libxwing2::Shp::TIEdDefender,     libxwing::Shp::TIEDefender },
    { libxwing2::Shp::TIEfoFighter,     libxwing::Shp::TIEfoFighter },
    { libxwing2::Shp::TIEinInterceptor, libxwing::Shp::TIEInterceptor },
    { libxwing2::Shp::TIElnFighter,     libxwing::Shp::TIEFighter },
    { libxwing2::Shp::TIEphPhantom,     libxwing::Shp::TIEPhantom },
    { libxwing2::Shp::TIEReaper,        libxwing::Shp::TIEReaper },
    { libxwing2::Shp::TIEsaBomber,      libxwing::Shp::TIEBomber },
    { libxwing2::Shp::TIEsfFighter,     libxwing::Shp::TIEsfFighter },
    { libxwing2::Shp::TIEskStriker,     libxwing::Shp::TIEStriker },
    { libxwing2::Shp::TIEvnSilencer,    libxwing::Shp::TIESilencer },
    { libxwing2::Shp::Upsilon,          libxwing::Shp::UpsilonClassShuttle },
    { libxwing2::Shp::UWing,            libxwing::Shp::UWing },
    { libxwing2::Shp::VCX100,           libxwing::Shp::VCX100 },
    { libxwing2::Shp::VT49,             libxwing::Shp::VT49Decimator },
    { libxwing2::Shp::YT2400,           libxwing::Shp::YT2400 },
    { libxwing2::Shp::YV666,            libxwing::Shp::YV666 },
    { libxwing2::Shp::Z95,              libxwing::Shp::Z95Headhunter },
  };

  khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "dialcomp", KATTR__MAX);
  khtml_elem(&hr, KELEM_TR);
  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "First Edition"); khtml_closeelem(&hr, 1);
  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Ship"); khtml_closeelem(&hr, 1);
  khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Second Edition"); khtml_closeelem(&hr, 1);
  khtml_closeelem(&hr, 1); // tr

  for(Shp shp : libxwing2::Ship::GetAllShps()) {
    Ship ship = Ship::GetShip(shp);

    khtml_elem(&hr, KELEM_TR);

    // First Edition
    if(FEShips.find(shp) == FEShips.end()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "na", KATTR__MAX);
      khtml_elem(&hr, KELEM_DIV);
      khtml_puts(&hr, "N/A");
      khtml_closeelem(&hr, 2); // div td
    } else {
      khtml_elem(&hr, KELEM_TD);
      libxwing::Ship feShip = libxwing::Ship::GetShip(FEShips.at(shp));
      PrintManeuvers(hr, feShip.GetManeuvers());
      khtml_closeelem(&hr, 1);
    }

    // Ship Icon
    std::string glyph = libxwing2::ShipGlyph::GetGlyph(shp);
    if(glyph == "") {
      khtml_attr(&hr, KELEM_TD, KATTR__MAX);
      khtml_puts(&hr, ship.GetShortName().c_str());
      khtml_closeelem(&hr, 1); // td
    }
    else {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "dcIcon", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ship.GetName().c_str(), KATTR__MAX);
      khtml_puts(&hr, glyph.c_str());
      khtml_closeelem(&hr, 2); // span td
    }

    // Second Edition
    khtml_elem(&hr, KELEM_TD);
    PrintManeuvers(hr, ship.GetManeuvers());
    khtml_closeelem(&hr, 1); // td

    khtml_closeelem(&hr, 1); // tr
  }

  khtml_closeelem(&hr, 1); // table

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
