#include "upgrade2.h"
#include "pagetoken2.h"

using namespace libxwing2;

Upgrade GetUpgrade(const PageToken& pt) {
  std::string s;
  if     ((s = pt.Get(PageToken::Upgrade)) != "") { return Upgrade::GetUpgrade(s); }
  else if((s = pt.Get(PageToken::XWS))     != "") { return Upgrade::GetUpgrade(converter::xws::GetUpgrade(s)); }
  throw UpgradeNotFound("");
}


static void PrintCard(struct khtmlreq &hr, PageToken &pt, Upgrade &upgrade, int side) {
  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "cardcontainer", KATTR__MAX);
  PrintUpgradeCard(hr, pt, upgrade);
  std::string imgUrl;
  try {
    imgUrl = converter::ffg::GetCard(upgrade.GetUpg(), side).c_str();
  }
  catch(converter::ffg::UpgradeNotFound &unf) { }
  if(imgUrl != "") {
    khtml_elem(&hr, KELEM_BR);
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "cardimage", KATTR__MAX);
    khtml_puts(&hr, "[");
    khtml_attr(&hr, KELEM_A, KATTR_HREF, imgUrl.c_str(), KATTR__MAX);
    khtml_puts(&hr, "IMAGE");
    khtml_closeelem(&hr, 1);
    khtml_puts(&hr, "]");
    khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 1);
}

void upgrade2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  Upgrade upgrade = GetUpgrade(pt);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    css.UpgradeInfo();
    css.UpgradeCard();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + upgrade.GetName() + "]", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			    {LinkBuilder(pt,"upgrades2").AddArg(PageToken::PointList).Get(), "upgrades2"}
    }, upgrade.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

  // upgrade card
  PrintCard(hr, pt, upgrade, 0);
  if(upgrade.IsDualSided()) {
    upgrade.Flip();
    PrintCard(hr, pt, upgrade, 1);
  }

  // effect
  if(upgrade.GetEffect()) {
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_effect", KATTR__MAX);
    IconifyText(hr, *upgrade.GetEffect());
    khtml_closeelem(&hr, 1);
  }

  // availability
  khtml_attr(&hr, KELEM_UL, KATTR__MAX);
  for(Rel rel : Release::GetByUpgrade(upgrade.GetUpg())) {
    Release release = Release::GetRelease(rel);
    khtml_elem(&hr, KELEM_LI);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,release.GetSku()).Get().c_str(), KATTR__MAX);
    khtml_puts(&hr, release.GetName().c_str());
    khtml_closeelem(&hr, 2); // a li
  }
  khtml_closeelem(&hr, 1); // ul

  { // history
    khtml_elem(&hr, KELEM_TABLE);

    khtml_attr(&hr, KELEM_TR, KATTR_CLASS, upgrade.IsUnreleased() ? "unr" : "", KATTR__MAX);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, ""); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Date"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Hyp"); khtml_closeelem(&hr, 1);
    khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Cost"); khtml_closeelem(&hr, 1);
    khtml_closeelem(&hr, 1);

    const std::vector<PtL> ptls = PointList::GetPtLs();
    std::optional<bool> lastHyp;
    std::optional<Cost> lastCst;
    for(PtL ptl : ptls) {
      std::optional<bool> hyp = PointList::GetHyperspace(upgrade.GetUpg(), ptl);
      std::optional<Cost> cst = PointList::GetCost(upgrade.GetUpg(), ptl);
      if((hyp && (!lastHyp || (*hyp != *lastHyp))) ||
	 (cst && (!lastCst || (*cst != *lastCst)))) {
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, PointList::GetName(ptl).c_str()); khtml_closeelem(&hr, 1);
	char date[11];
	const PLDate d = PointList::GetEffectDate(ptl);
	snprintf(date, sizeof(date), "%04d/%02d/%02d", d.year, d.month, d.date);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, date); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, *hyp ? "Yes" : "No"); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, cst->GetCostStr().c_str()); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1); // tr
      }
      lastHyp = hyp;
      lastCst = cst;
    }
    khtml_closeelem(&hr, 1); // table
  }

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
