#include "damagedeck2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void damagedeck2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string ddstr = pt.Get(PageToken::Deck);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  try {
    Deck dd = Deck::GetDeck(ddstr);
    { // head
      CSS css;
      css.DamageDeck();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + dd.GetName() + "]", css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			      {LinkBuilder(pt,"damagedecks2").AddArg(PageToken::PointList).Get(), "damagedecks2"}
      }, dd.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

    khtml_elem(&hr, KELEM_TABLE);

    for(Dmg dmg : DamageCard::GetAllDmgs(dd.GetDck())) {
      khtml_elem(&hr, KELEM_TR);
      DamageCard dc = DamageCard::GetDamageCard({dmg, dd.GetDck()});

      { // ids
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	bool first = true;
	std::string idStr;
	for(const uint8_t& id : dc.GetIds()) {
	  if(first) { first = false; }
	  else      { idStr += ","; }
	  idStr += std::to_string(id);
	}
	khtml_puts(&hr, idStr.c_str());
	khtml_closeelem(&hr, 1); // td
      }
      { // name
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
	khtml_puts(&hr, dc.GetName().c_str());
	khtml_closeelem(&hr, 1); // td
      }
      { // count
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	khtml_puts(&hr, (std::string("x")+std::to_string(dc.GetCount())).c_str());
	khtml_closeelem(&hr, 1); // td
      }
     
      { // trait
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
	khtml_puts(&hr, dc.GetTrait().GetName().c_str());
	khtml_closeelem(&hr, 1); // td
      }
      { // text
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	IconifyText(hr, dc.GetText());
	khtml_closeelem(&hr, 1); // td
      }
      // precision shot
      if(dc.GetPrS()) {
	PrecisionShot ps = *dc.GetPrecisionShot();
	{ // name
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
	  khtml_puts(&hr, ps.GetName().c_str());
	  khtml_closeelem(&hr, 1); // td
	}
	{ // arcs
	  khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	  for(Arc a : ps.GetArcs()) {
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "attackArc", KATTR_TITLE, FiringArc::GetFiringArc(a).GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, IconGlyph::GetGlyph(a).c_str());
	    khtml_closeelem(&hr, 1);
	  }
	  khtml_closeelem(&hr, 1); // td
	}
	{ // text
	  khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	  IconifyText(hr, ps.GetText());
	  khtml_closeelem(&hr, 1); // td
	}
      }
      
      khtml_closeelem(&hr, 1); // tr
    }
  }
  catch(DeckNotFound dnf) {
    khtml_puts(&hr, dnf.what());
  }

  khtml_closeelem(&hr, 1); // tab

  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
