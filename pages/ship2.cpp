#include "ship2.h"
#include "pagetoken2.h"
#include <algorithm>
#include <map>

using namespace libxwing2;

Ship GetShip(const PageToken& pt) {
  std::string s;
  if     ((s = pt.Get(PageToken::Ship)) != "") { return Ship::GetShip(s); }
  else if((s = pt.Get(PageToken::XWS))  != "") { return Ship::GetShip(converter::xws::GetShip(s)); }
  throw PilotNotFound("");
}

void ship2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  Ship ship = GetShip(pt);

  struct khtmlreq hr;

  // page
  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  { // head
    CSS css;
    css.ShipTitle();
    css.Maneuvers();
    css.PilotRow();
    PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + ship.GetName() + "]", css.GetCSS());
  }

  // body
  khtml_elem(&hr, KELEM_BODY);
  PrintBreadcrumbs(kr, hr, {
			    {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			    {LinkBuilder(pt,"ships2").AddArg(PageToken::PointList).Get(), "ships2"}
    }, ship.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

  { // header
    khtml_elem(&hr, KELEM_DIV);
    { // ship icon
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "shipHdrIcon", KATTR__MAX);
      khtml_puts(&hr, ShipGlyph::GetGlyph(ship.GetShp()).c_str());
      khtml_closeelem(&hr, 1);
    }
    { // ship name 
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "titleText", KATTR__MAX);
      khtml_puts(&hr, ship.GetName().c_str());
      khtml_closeelem(&hr, 1);
    }
    khtml_closeelem(&hr, 1);
  }
  {  // maneuvers
    PrintManeuvers(hr, ship.GetManeuvers());
    khtml_elem(&hr, KELEM_BR);
  }
  // get pilots & ship abilities
  std::vector<Pilot> pilots;
  std::map<std::string,std::string> shipAbilities;
  for(Plt plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if(pilot.GetShp() == ship.GetShp()) {
      pilots.push_back(pilot);
      auto sa = pilot.GetShipAbility();
      if(sa) {
	shipAbilities[sa->GetName()] = sa->GetText().GetMarkedText();
      }
    }
  }

  std::sort(pilots.begin(), pilots.end(), [] (Pilot a, Pilot b) {
      return (a.GetFac() < b.GetFac())
	|| ((a.GetFac() == b.GetFac()) && (a.GetNatInitiative() > b.GetNatInitiative()));
    });

  // print ship abilities
  if(shipAbilities.size()) {
    khtml_elem(&hr, KELEM_TABLE);
    for(const auto& a : shipAbilities) {
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TH);
      khtml_puts(&hr, a.first.c_str());
      khtml_closeelem(&hr, 1); // th
      khtml_elem(&hr, KELEM_TD);
      IconifyText(hr, a.second);
      khtml_closeelem(&hr, 2); // td tr
    }
    khtml_closeelem(&hr, 1); // table
  }
  // print pilot
  khtml_elem(&hr, KELEM_TABLE);
  for(auto p : pilots) {
    PrintPilotRow(kr, hr, pt, p);
  }

  khtml_closeelem(&hr, 1); // table

  khtml_closeelem(&hr, 1); // body                                                                                                                                                                                                 
  khtml_close(&hr);
}
