#include "ship.h"
#include <algorithm>

using namespace libxwing;



void ship(struct kreq *req) {
  struct khtmlreq  r;

  // load the ship
  std::string shipstr;
  for(int i=0; i<req->fieldsz; i++) {
    if(strcmp(req->fields[i].key, "ship") == 0) {
      shipstr = req->fields[i].val;
    }
  }
  Ship ship = Ship::GetShip(shipstr);

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath + " [" + ship.GetName() + "]");

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)},{std::string(req->pname)+"/ships", "ships"}}, ship.GetName());

  { // header
    khtml_elem(&r, KELEM_DIV);
    { // ship icon
      khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "HeaderIcon", KATTR__MAX);
      khtml_puts(&r, ShipGlyph::GetGlyph(ship.GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // ship name 
      khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "HeaderText", KATTR__MAX);
      khtml_puts(&r, ship.GetName().c_str());
      khtml_closeelem(&r, 1);
    }
    khtml_closeelem(&r, 1);
  }
  {  // maneuvers
    PrintManeuvers(r, ship.GetManeuvers());
    khtml_elem(&r, KELEM_BR);
  }
  // pilots
  std::vector<Pilot> pilots;
  for(Pilot p : Pilot::GetAllPilots()) {
    if(p.GetShip().GetType() == ship.GetType()) {
      pilots.push_back(p);
    }
  }
  std::sort(pilots.begin(), pilots.end(), [] (Pilot a, Pilot b) {
      return (a.GetFaction().GetType() < b.GetFaction().GetType())
	|| ((a.GetFaction().GetType() == b.GetFaction().GetType()) && (a.GetNatSkill() > b.GetNatSkill()));
    });

  khtml_elem(&r, KELEM_TABLE);

  for(auto p : pilots) {
    khtml_elem(&r, KELEM_TR);
    { // faction
      std::string css;
      switch(p.GetFaction().GetType()) {
      case Fac::Rebel:      css = "factionRebel";      break;
      case Fac::Imperial:   css = "factionImperial";   break;
      case Fac::Scum:       css = "factionScum";       break;
      default:              css = "";
      }
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
      khtml_puts(&r, IconGlyph::GetGlyph(p.GetFaction().GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { // skill
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "skill", KATTR__MAX);
      khtml_int(&r, p.GetNatSkill());
      khtml_closeelem(&r, 1);
    }
    { // name
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "pilotName", KATTR__MAX);
      std::string url = std::string(req->pname)+"/pilot?name=" + p.GetXws() + "&faction=" + p.GetFaction().GetXws() + "&ship=" + p.GetShip().GetXws();
      khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
      if(p.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str());
	khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, p.GetName().c_str());
      khtml_closeelem(&r, 2);
    }
    { // cost
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "cost", KATTR__MAX);
      khtml_int(&r, p.GetNatCost());
      khtml_closeelem(&r, 1);
    }
    { // arc
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "attackArc", KATTR__MAX);
      khtml_puts(&r, IconGlyph::GetGlyph(p.GetShip().GetPrimaryArc().GetType()).c_str());
      khtml_closeelem(&r, 1);
    }
    { //attack
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "attackStat", KATTR__MAX);
      khtml_int(&r, p.GetNatAttack());
      khtml_closeelem(&r, 1);
    }
    { // agility
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "agilityStat", KATTR__MAX);
      khtml_int(&r, p.GetNatAgility());
      khtml_closeelem(&r, 1);
    }
    { // hull
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "hullStat", KATTR__MAX);
      khtml_int(&r, p.GetNatHull());
      khtml_closeelem(&r, 1);
    }
    { // shield
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "shieldStat", KATTR__MAX);
      khtml_int(&r, p.GetNatShield());
      khtml_closeelem(&r, 1);
    }
    { // actions
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "actionW", KATTR__MAX);
      ForEachAction(p.GetNatActions(), [&r](Act a) {khtml_puts(&r, IconGlyph::GetGlyph(a).c_str());});
      khtml_closeelem(&r, 1);
    }
    { // upgrades
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "upgrades", KATTR__MAX);
      for(Upg u : p.GetNatPossibleUpgrades()) {
	if(u != Upg::Modification && u != Upg::Title) {
	  khtml_puts(&r, IconGlyph::GetGlyph(u).c_str());
	}
      };
      khtml_closeelem(&r, 1);
    }
    { // text
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, p.HasAbility() ? "pilotAbility" : "pilotFlavor", KATTR__MAX);
      if(p.IsUnreleased()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "unreleased", KATTR__MAX);
	khtml_puts(&r, "[UNRELEASED] ");
	khtml_closeelem(&r, 1);
      }
      IconifyText(r, p.GetText());
      khtml_closeelem(&r, 1);
    }

    khtml_closeelem(&r, 1); // tr
  }

  khtml_closeelem(&r, 1); // table

  khtml_closeelem(&r, 1); // body                                                                                                                                                                                                 
  khtml_close(&r);
}
