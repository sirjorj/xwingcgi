#include "search2.h"
#include "pagetoken2.h"

using namespace libxwing2;

static void PrintSearchTypeHeader(struct kreq *req, khtmlreq &r, std::string s) {
  khtml_attr(&r, KELEM_P, KATTR_CLASS, "typeHdr", KATTR__MAX);
  khtml_puts(&r, s.c_str());
  khtml_closeelem(&r, 1);
}

std::string FormatPlaceholder(std::string s) {
  if(s.size() == 0) { return ""; }
  std::string ret = s.substr(1, s.size()-2);
  return ToLower(ret);
}

void AddSymbol(struct khtmlreq &r, const IconGlyph& ig) {
  std::string name = ig.GetCardTextPlaceholder();
  std::string url = "search2?s=" + name;
  khtml_attr(&r, KELEM_A, KATTR_CLASS, "sym", KATTR_HREF, url.c_str(), KATTR__MAX);
  khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR_TITLE, name.c_str(), KATTR__MAX);
  khtml_puts(&r, ig.GetGlyph().c_str());
  khtml_closeelem(&r, 2);
}


void search2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string searchString = pt.Get(PageToken::Search);
  bool osd = pt.HasArg(PageToken::OSD);

  struct khtmlreq hr;

  // requesting the Open Search Description - https://www.chromium.org/tab-to-search
  if(osd) {
    std::string xml = R"(<?xml version="1.0"?>
  <OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
  <ShortName>Search xwing.cgi</ShortName>
  <Description>Search an unofficial X-wing Second Edition Database</Description>
  <Url type="text/html" method="get" template="https://xhud.sirjorj.com/xwing.cgi/search2?s={searchTerms}"/>
  <Image width="16" height="16">/xhud_favico.png</Image>
  <SearchForm>https://xhud.sirjorj.com/xwing.cgi/search2</SearchForm>
</OpenSearchDescription>)";
    resp_open(kr, KHTTP_200, "application/opensearch+xml");
    khttp_puts(kr, xml.c_str());
  }

  // if something was POSTed...
  else {
    CSS css;
    css.Search();
    css.PilotRow();
    css.UpgradeRow();

    if(searchString != "") {
      // page
      resp_open(kr, KHTTP_200);
      khtml_open(&hr, kr, KHTML_PRETTY);
      khtml_elem(&hr, KELEM_DOCTYPE);
      khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

      { // head
	PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
      }

      // body
      khtml_elem(&hr, KELEM_BODY);
      PrintBreadcrumbs(kr, hr, {
				{LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
	}, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

      khtml_puts(&hr, "Search: ");
      khtml_puts(&hr, "\"");
      khtml_puts(&hr, searchString.c_str());
      khtml_puts(&hr, "\"");

      { // *** ships ***
	std::vector<Shp> shps = Ship::Search(searchString);
	if(shps.size()) {
	  PrintSearchTypeHeader(kr, hr, "Ships");
	  khtml_elem(&hr, KELEM_TABLE);
	  for(Shp shp : shps) {
	    Ship ship = Ship::GetShip(shp);
	    PrintShipRow(kr, hr, pt, ship);
	  }
	  khtml_closeelem(&hr, 1); // table
	}
      }

      { // *** pilots ***
	std::vector<Plt> plts = Pilot::Search(searchString);
	if(plts.size()) {
	  PrintSearchTypeHeader(kr, hr, "Pilots");
	  khtml_elem(&hr, KELEM_TABLE);
	  for(Plt plt : plts) {
	    Pilot pilot = Pilot::GetPilot(plt);
	    PrintPilotRow(kr, hr, pt, pilot);
	  }
	}
	khtml_closeelem(&hr, 1); // table
      }

      {// *** upgrades ***
	std::vector<Upg> upgs = Upgrade::Search(searchString);
	if(upgs.size()) {
	  PrintSearchTypeHeader(kr, hr, "Upgrades");
	  khtml_elem(&hr, KELEM_TABLE);
	  for(Upg upg : upgs) {
	    Upgrade upgrade = Upgrade::GetUpgrade(upg);
	    PrintUpgradeRow(kr, hr, pt, upgrade);
	  }
	}
	khtml_closeelem(&hr, 1); // table
      }
      khtml_closeelem(&hr, 2); // div body
      khtml_close(&hr);
    } // no format

    else {
      // page
      resp_open(kr, KHTTP_200);
      khtml_open(&hr, kr, KHTML_PRETTY);
      khtml_elem(&hr, KELEM_DOCTYPE);
      khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

      { // head
	PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
      }

      // body
      khtml_elem(&hr, KELEM_BODY);
      PrintBreadcrumbs(kr, hr, {
				{LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
	}, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

      // form
      khtml_attr(&hr, KELEM_FORM, KATTR_ACTION, (std::string(kr->pname)+"/search2").c_str(), KATTR_METHOD, "POST", KATTR__MAX);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "text", KATTR_NAME, "s", KATTR_ROWS, "1", KATTR_COLS, "32", KATTR__MAX);
      khtml_elem(&hr, KELEM_BR);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "Submit", KATTR__MAX);
      khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "hidden", KATTR_NAME, PageToken::PointList.c_str(), KATTR_VALUE, PointList::GetPtLStr(pt.GetPtL()).c_str(), KATTR__MAX);
      khtml_closeelem(&hr, 1); // form

      // instructions
      khtml_elem(&hr, KELEM_P);
      khtml_puts(&hr, "This searches all ships/pilots/upgrades for the literal text passed in (no AND/OR rules).");
      khtml_closeelem(&hr, 1);
      khtml_elem(&hr, KELEM_P);
      khtml_puts(&hr, "Clicking on one of the following symbols will return all cards with tthat symbol in the text.");
      khtml_closeelem(&hr, 1);

      // symbols
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Search by Symbol (Text only)"); khtml_closeelem(&hr, 2);
      // hit/crit (focus and evade are actions)
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      for(const AtD& d : { AtD::Hit, AtD::Crit }) {
	const IconGlyph ig = IconGlyph::GetIconGlyph(d);
	AddSymbol(hr, ig);
      }
      khtml_closeelem(&hr, 2);
      // arcs
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      for(Arc arc : FiringArc::GetAllArcs()) {
	FiringArc firingArc = FiringArc::GetFiringArc(arc);
	if(firingArc.GetArc() == Arc::None) { continue; }
	const IconGlyph ig = IconGlyph::GetIconGlyph(firingArc.GetArc());
	AddSymbol(hr, ig);
      }
      khtml_closeelem(&hr, 2);
      // actions
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      for(Act act : Action::GetAllActs()) {
	const IconGlyph ig = IconGlyph::GetIconGlyph(act);
	AddSymbol(hr, ig);
      }
      khtml_closeelem(&hr, 2);
      // upgrade types
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      for(UpT upt : UpgradeType::GetAllUpTs()) {
	const IconGlyph ig = IconGlyph::GetIconGlyph(upt);
	AddSymbol(hr, ig);
      }
      khtml_closeelem(&hr, 2);
      khtml_closeelem(&hr, 1); // symbol table

      // browser integration
      khtml_elem(&hr, KELEM_P);
      khtml_puts(&hr, "Your web browser should detect this and allow you to add it as a search engine - if not you can add it manually like this:");
      khtml_closeelem(&hr, 1);

      // web browser instructions
      khtml_elem(&hr, KELEM_TABLE);
      { // header
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "Web Browser Settings"); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      {
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Search Engine"); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "xwing.cgi"); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      {
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "Keyword"); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "xw (or whatever you want)"); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      {
	khtml_elem(&hr, KELEM_TR);
	khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "URL"); khtml_closeelem(&hr, 1);
	khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "https://xhud.sirjorj.com/xwing.cgi/search2?s=%s"); khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 1);
      }
      khtml_closeelem(&hr, 1); // table

      khtml_closeelem(&hr, 1); // body
      khtml_close(&hr);
    }
  }
}
