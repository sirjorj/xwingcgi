#include "pilot.h"

using namespace libxwing;



void pilot(struct kreq *req) {
  struct khtmlreq  r;

  std::string pltstr, facstr, shpstr, format="";
  for(int i=0; i<req->fieldsz; i++) {
    if     (strcmp(req->fields[i].key, "name")   == 0)  { pltstr = req->fields[i].val; }
    else if(strcmp(req->fields[i].key, "faction") == 0) { facstr = req->fields[i].val; }
    else if(strcmp(req->fields[i].key, "ship")    == 0) { shpstr = req->fields[i].val; }
    else if(strcmp(req->fields[i].key, "format")  == 0) { format = req->fields[i].val; }
  }
  Pilot pilot = Pilot::GetPilot(pltstr, facstr, shpstr);

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath + " [" + pilot.GetName() + "]");

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)},{std::string(req->pname)+"/pilots", "pilots"}}, pilot.GetName());

  if((format == "") || (format == "table")) {
    khtml_elem(&r, KELEM_TABLE);
    {// pilot name
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Name"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.GetName().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // pilot xws
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Name (xws)"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.GetXws().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // short name
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Name (short)"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.GetShortName().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // unique
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Is Unique"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.IsUnique() ? "Yes" : "No"); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // faction
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Faction"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.GetFaction().GetName().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // ship
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "SubFaction"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.GetSubFaction().GetName().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // ship
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Ship"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
      std::string url = std::string(req->pname)+"/ship?ship=" + pilot.GetShip().GetXws();
      khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
      khtml_puts(&r, pilot.GetShip().GetName().c_str());
      khtml_closeelem(&r, 3);
    }
    { // cost
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Cost"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatCost()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // skill
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Skill"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatSkill()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // attack
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Attack"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatAttack()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // agility
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Agility"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatAgility()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // hull
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Hull"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatHull()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // shield
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Shield"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatShield()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // ability
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Has Ability"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, pilot.HasAbility() ? "Yes" : "No"); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // text
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Text"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); IconifyText(r, pilot.GetText().c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1);
    }
    { // actions
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Actions"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "actionW", KATTR__MAX);
      std::string actStr;
      int first = true;
      ForEachAction(pilot.GetNatActions(), [&actStr, &first](Act a) {
	  if(first) { first = false; }
	  else      { actStr += " "; }
	  actStr += IconGlyph::GetGlyph(a);
	});
      khtml_puts(&r, actStr.c_str());
      khtml_closeelem(&r, 2);
    }
    { // upgrades
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Upgrades"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "upgrades", KATTR__MAX);
      std::string upgStr;
      for(Upg u : pilot.GetNatPossibleUpgrades()) {
	upgStr += IconGlyph::GetGlyph(u);
      }
      khtml_puts(&r, upgStr.c_str());
      khtml_closeelem(&r, 2);
    }
    { // availability
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&r, "Availability"); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX);
      int first=true;
      for(Release rel : Release::GetPilot(pilot.GetXws(), pilot.GetFaction().GetType(), pilot.GetShip().GetType())) {
	if(first) { first = false; }
	else      { khtml_elem(&r, KELEM_BR); }
	std::string url = std::string(req->pname)+"/release?sku=" + rel.GetSku();
	khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	khtml_puts(&r, rel.GetName().c_str());
	khtml_closeelem(&r, 1);
      }
    }

    khtml_closeelem(&r, 2);
    khtml_closeelem(&r, 1); // table

    // card view link
    khtml_elem(&r, KELEM_P);
    khtml_puts(&r, "[");
    std::string pUrl = std::string(req->pname)+"/pilot?name=" + pilot.GetXws() + "&faction=" + pilot.GetFaction().GetXws() + "&ship=" + pilot.GetShip().GetXws() + "&format=card";
    khtml_attr(&r, KELEM_A, KATTR_HREF, pUrl.c_str(), KATTR__MAX);
    khtml_puts(&r, "View as card");
    khtml_closeelem(&r, 1); // a
    khtml_puts(&r, "]");
    khtml_closeelem(&r, 1); // p
  }

  else if(format == "card") {
    int sides = pilot.IsDualSided() ? 2 : 1;
    for(int i=0; i<sides; i++) {
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcPilot", KATTR__MAX);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcManeuvers", KATTR__MAX); PrintManeuvers(r, pilot.GetShip().GetManeuvers()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcSkill", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatSkill()).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcName", KATTR__MAX);
      if(pilot.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, pilot.GetName().c_str());
      khtml_closeelem(&r, 1);

      Fac f = pilot.GetFaction().GetType();
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, (f==Fac::Rebel) ? "pcFactReb" : (f==Fac::Imperial) ? "pcFactImp" : "pcFactScu", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(pilot.GetSubFaction().GetType()).c_str()); khtml_closeelem(&r, 1);

      std::string url = std::string(req->pname)+"/ship?ship=" + pilot.GetShip().GetXws();
      khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcShip", KATTR__MAX); khtml_puts(&r, pilot.GetShip().GetName().c_str());
      khtml_closeelem(&r, 2);

      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcAtckIcon", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(pilot.GetShip().GetPrimaryArc().GetType()).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcAgilIcon", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::agility).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcHullIcon", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::hull).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcShldIcon", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::shield).c_str()); khtml_closeelem(&r, 1);

      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcAtck", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatAttack()).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcAgil", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatAgility()).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcHull", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatHull()).c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcShld", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatShield()).c_str()); khtml_closeelem(&r, 1);

      if(pilot.HasAbility()) {
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcAbility", KATTR__MAX); IconifyText(r, pilot.GetText()); khtml_closeelem(&r, 1);
      } else {
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcGenText", KATTR__MAX); khtml_puts(&r, pilot.GetText().c_str()); khtml_closeelem(&r, 1);
      }

      std::string actStr;
      bool space = false;
      ForEachAction(pilot.GetNatActions(), [&actStr, &space](Act a){
	  if(space) {
	    actStr += " ";
	  } else {
	    space=true;
	  }
	  actStr += IconGlyph::GetGlyph(a);
	});
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcActions", KATTR__MAX); khtml_puts(&r, actStr.c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "pcShipIcon", KATTR__MAX); khtml_puts(&r, ShipGlyph::GetGlyph(pilot.GetShip().GetType()).c_str()); khtml_closeelem(&r, 1);
      std::string upgStr;
      for(Upg u : pilot.GetNatPossibleUpgrades()) {
	if(u != Upg::Modification && u != Upg::Title) {
	  upgStr += IconGlyph::GetGlyph(u);
	  upgStr += " ";
	}
      };
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "p_div_Upgrades", KATTR__MAX); khtml_puts(&r, upgStr.c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "p_div_Cost", KATTR__MAX); khtml_puts(&r, std::to_string(pilot.GetNatCost()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1); // div
      pilot.Flip();
    }
  }

  else {
    khtml_puts(&r, "Unknown format");
  }

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
