#include "pilots2.h"
#include "pagetoken2.h"
#include "../json/json.h"

using namespace libxwing2;

std::vector<QBCard> GetCards(QB _qb) {
  std::vector<QBCard> ret;
  for(QBC qbc : QuickBuild::GetAllQBCs()) {
    QBCard qbCard = QuickBuild::GetQBCard(qbc);
    for(const QB &qb : qbCard.qbs) {
      if(qb == _qb) {
    	ret.push_back(qbCard);
      }
    }
  }
  return ret;
}


void quickbuilds2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format=pt.Get(PageToken::Format);

  if(format == "") {
    struct khtmlreq hr;

    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      css.QuickBuilds();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

    // list of quickbuilds
    for(Fac fac : Faction::GetAllFacs()) {
      Faction faction = Faction::GetFaction(fac);
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
      { // faction
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, ("icon_" + faction.Get3Letter()).c_str(), KATTR_COLSPAN, "5", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "headerSize", KATTR_TITLE, faction.GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(faction.GetFac()).c_str());
	khtml_closeelem(&hr, 3); // span td tr
      }

      // could probably optimize this the same way pilots2 is done...

      for(Shp shp : Ship::GetAllShps()) {
	Ship ship = Ship::GetShip(shp);
	std::vector<QuickBuild> quickBuilds;
	for(Plt plt : Pilot::GetAllPlts()) {
	  Pilot pilot = Pilot::GetPilot(plt);
	  if(pilot.GetFac() == fac && pilot.GetShp() == shp) {
	    try {
	      for(QB qb : QuickBuild::GetQBs(plt)) {
		QuickBuild quickBuild = QuickBuild::GetQuickBuild(qb);
		quickBuilds.push_back(quickBuild);
	      }
	    }
	    catch(QuickBuildNotFound &qbnf) {
	    }
	  }
	}

	if(quickBuilds.size()) {
	  bool drawShip=true;
	  int rows=0;
	  for(const QuickBuild& q : quickBuilds) { rows += q.GetQBPlts().size(); }
	  for(const QuickBuild& q : quickBuilds) {
	    khtml_elem(&hr, KELEM_TR);

	    if(drawShip) { // ship icon
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(rows).c_str(), KATTR__MAX);
	      khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ship.GetName().c_str(), KATTR__MAX);
	      khtml_puts(&hr, ShipGlyph::GetGlyph(shp).c_str());
	      khtml_closeelem(&hr, 2); // span td
	      drawShip=false;
	    }

	    { // threat
	      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(q.GetQBPlts().size()).c_str(), KATTR__MAX);
	      khtml_puts(&hr, std::to_string(q.GetThreat()).c_str());
	      khtml_closeelem(&hr, 1);
	    }

	    bool notFirstShip = false;
	    for(const QBPlt &qbp : q.GetQBPlts()) {
	      if(notFirstShip) {
		khtml_elem(&hr, KELEM_TR);
	      }
	      else {
		notFirstShip = true;
	      }
	      { // pilot
		khtml_elem(&hr, KELEM_TD);
		std::string xp;
		Pilot p = Pilot::GetPilot(qbp.plt);
		try{
		  xp = converter::xws::GetPilot(qbp.plt);
		}
		catch(PilotNotFound &pnf) {
		  khtml_puts(&hr, std::string("CANNOT FIND PILOT" + std::to_string((int)p.GetPlt())).c_str());
		  continue;
		}
		catch(std::runtime_error &re) {
		  khtml_puts(&hr, (std::string("error: ") + re.what()).c_str());
		}
		catch(...) {
		  khtml_puts(&hr, "WTF?");
		  continue;
		}
		PrintPilotName(kr, hr, pt, p);
		khtml_closeelem(&hr, 1); // td
	      }
	      { // upgrade
		khtml_elem(&hr, KELEM_TD);
		bool isFirst = true;
		for(Upg up : qbp.upgs) {
		  Upgrade u = Upgrade::GetUpgrade(up);
		  if(isFirst) { isFirst=false;}
		  else        { khtml_puts(&hr, " . "); }
		  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "upgrade2").AddArg(PageToken::PointList).AddArg(PageToken::Upgrade,u.GetUpgStr()).Get().c_str(), KATTR__MAX);
		  if(u.GetLimited()) {
		    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR__MAX);
		    for(int i=0; i<u.GetLimited(); i++) {
		      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::unique).c_str());
		    }
		    khtml_closeelem(&hr, 1);
		  }
		  khtml_puts(&hr, u.GetName().c_str());
		  khtml_closeelem(&hr, 1); // a
		}
		khtml_closeelem(&hr, 1); // td
	      }
	      { // source
		khtml_elem(&hr, KELEM_TD);
		std::vector<QBCard> cards = GetCards(q.GetQB());
		bool first = true;
		for(const QBCard &qbCard : cards) {
		  if(first) { first = false; }
		  else      { khtml_puts(&hr, " "); }
		  khtml_puts(&hr, "[");
		  switch(qbCard.qbct) {
		  case QBCT::PDF:
		    khtml_puts(&hr, "PDF");
		    break;
		  case QBCT::Exp:
		    {
		      try{
			Release release = Release::GetRelease(qbCard.qbctData);
			khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,qbCard.qbctData).Get().c_str(), KATTR__MAX);
			khtml_puts(&hr, qbCard.qbctData.c_str());
			khtml_closeelem(&hr, 1); // a
		      }
		      catch(ReleaseNotFound &rnf) {
			khtml_puts(&hr, rnf.what());
		      }
		    }
		    break;
		  case QBCT::Promo:
		    khtml_puts(&hr, qbCard.qbctData.c_str());
		    break;
		  }
		  khtml_puts(&hr, "]");
		}
		khtml_closeelem(&hr, 1); // td
	      }
	      khtml_closeelem(&hr, 1); // tr
	    } // for qbplt
	  } // for(QuickBuild)
	} // if there are quickbuilds
      } // for(Ships)
      khtml_closeelem(&hr, 2); // table div(floater)
    } // for(faction)
    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR__MAX); khtml_puts(&hr, "Formats"); khtml_closeelem(&hr, 2);
      { // json
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"quickbuilds2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "json"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      khtml_closeelem(&hr, 1); // table
    }





    khtml_closeelem(&hr, 1); // div(container) body
    khtml_close(&hr);
  }

  else if(format == "json") {
    int qc=0;
    Json::Value jsQB;

    for(QB qb : QuickBuild::GetAllQBs()) {
      QuickBuild quickBuild = QuickBuild::GetQuickBuild(qb);
      jsQB[qc]["faction"] = quickBuild.GetQBPilots()[0].pilot.GetFaction().GetName();
      jsQB[qc]["name"] = quickBuild.GetName();
      jsQB[qc]["threat"] = quickBuild.GetThreat();
      int pc=0;
      for(const QBPilot &qbPilot : quickBuild.GetQBPilots()) {
	jsQB[qc]["pilots"][pc]["name"] = qbPilot.pilot.GetName();
	jsQB[qc]["pilots"][pc]["name_xws"] = converter::xws::GetPilot(qbPilot.pilot.GetPlt());
	jsQB[qc]["pilots"][pc]["ship"] = qbPilot.pilot.GetShip().GetName();
	int uc=0;
	for(const Upgrade &upgrade : qbPilot.upgrades) {
	  jsQB[qc]["pilots"][pc]["upgrades"][uc]["type"] = upgrade.GetUpgradeType().GetName();
	  jsQB[qc]["pilots"][pc]["upgrades"][uc]["name"] = upgrade.GetName();
	  jsQB[qc]["pilots"][pc]["upgrades"][uc]["name_xws"] = converter::xws::GetUpgrade(upgrade.GetUpg());
	  uc++;
	}
	pc++;
      }

      int sc=0;
      std::vector<QBCard> cards = GetCards(quickBuild.GetQB());
      for(const QBCard &qbCard : cards) {
	std::string source;
	std::string data = qbCard.qbctData;
	switch(qbCard.qbct) {
	case QBCT::PDF: source = "PDF"; break;
	case QBCT::Exp: source = "Expansion"; break;
	case QBCT::Promo: source = "Promo"; break;
	}
	jsQB[qc]["source"][sc]["type"] = source;
	jsQB[qc]["source"][sc]["data"] = data;
	sc++;
      }
      






      qc++;
    }

    std::string jsonStr;
    std::stringstream ss;
    ss << jsQB;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }
}
