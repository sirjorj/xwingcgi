#include "pagetoken2.h"
#include <sstream>

using namespace libxwing2;

const std::string PageToken::Cost = "costs";
const std::string PageToken::Deck = "dd";
const std::string PageToken::Format = "format";
const std::string PageToken::Image = "image";
const std::string PageToken::LJ = "lj";
const std::string PageToken::Mode = "mode";
const std::string PageToken::Name = "name";
const std::string PageToken::OSD = "osd";
const std::string PageToken::Pilot = "pilot";
const std::string PageToken::PointList = "ptl";
const std::string PageToken::Restriction = "restriction";
const std::string PageToken::Search = "s";
const std::string PageToken::Ship = "ship";
const std::string PageToken::SKU = "sku";
const std::string PageToken::Squad = "squad";
const std::string PageToken::Upgrade = "upgrade";
const std::string PageToken::Validate = "validate";
const std::string PageToken::XWI = "xwi";
const std::string PageToken::XWS = "xws";
const std::string PageToken::YASB = "yasb";

PageToken::PageToken(struct kreq *kr)
  : scheme(kschemes[kr->scheme]), host(kr->host), pname(kr->pname), path(kr->fullpath) {
  for(int i=0; i<kr->fieldsz; i++) {
    this->args[kr->fields[i].key] = kr->fields[i].val;
  }
}

std::string PageToken::GetScheme() const { return this->scheme; }
std::string PageToken::GetHost()   const { return this->host; }
std::string PageToken::GetPName()  const { return this->pname; }
std::string PageToken::GetPath()   const { return this->path; }
const std::map<std::string,std::string>& PageToken::GetArgs() const {
  return this->args;
}

bool PageToken::HasArg(std::string k) const {
  return this->args.find(k) != this->args.end();
}

std::string PageToken::Get(std::string k) const {
  return (this->args.find(k) != this->args.end()) ? this->args.at(k) : "";
}

PtL PageToken::GetPtL() const {
  std::string ptl = this->Get(PageToken::PointList);
  if(ptl == "") {
    return PtL::Current;    
  } else {
    return PointList::GetPtL(ptl);
  }
}





LinkBuilder::LinkBuilder(const PageToken &pt, std::string p)
  : pt(pt), path(p) {
}

LinkBuilder& LinkBuilder::AddArg(std::string k) {
  if(k == PageToken::PointList) {
    this->args[k] = PointList::GetPtLStr(pt.GetPtL());
  } else {
    this->args[k] = pt.Get(k);
  }
  return *this;
}

LinkBuilder& LinkBuilder::AddArg(std::string k, std::string v) {
  this->args[k] = v;
  return *this;
}

LinkBuilder& LinkBuilder::AllArgs() {
  this->args = pt.GetArgs();
  return *this;
}

static void Append(std::stringstream &ss, const std::string& add) {
  std::string str = ss.str();
  bool a = str[str.size()-1] == '/';
  bool b = add[0] == '/';
  if     (a && b)   { ss << add.substr(1); }
  else if(!a && !b) { ss << "/" << add; }
  else              { ss << add; }
}

std::string LinkBuilder::Get() {
  std::stringstream ss;
  ss << this->pt.GetScheme() << "://";
  ss << this->pt.GetHost();
  Append(ss, this->pt.GetPName());
  Append(ss, this->path);
  ss << "?";
  bool first = true;
  for(const std::pair<const std::string, std::string> &a : this->args) {
    if((a.first == PageToken::PointList) && (PointList::GetPtL(a.second) == PtL::Current)) { continue; }
    if(first) { first = false; }
    else      { ss << "&"; }
    if(a.first == PageToken::PointList) {
      ss << a.first << "=" << a.second;
    } else {
      ss << a.first << "=" << a.second;
    }
  }
  return ss.str();
}
