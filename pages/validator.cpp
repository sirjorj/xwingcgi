#include "validator.h"
#include "../b64/encode.h"
#include <sys/wait.h>

using namespace libxwing;



static std::string GetBase64(std::istream &input) {
  base64::encoder enc;
  std::ostringstream output;
  enc.encode(input, output);
  return output.str();
}



void validator(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  std::string xws;
  for(int i=0; i<req->fieldsz; i++) {
    if(strcmp(req->fields[i].key, "xws") == 0) {
      xws = req->fields[i].val;
      break;
    }
  }

  if(xws != "") {
    std::stringbuf buf(xws);
    std::istream istr(&buf);
    try {
      Squad squad = Squad(istr);

      // info
      std::vector<std::string> issues = squad.Verify();
      khtml_elem(&r, KELEM_P);
      khtml_elem(&r,KELEM_B); khtml_puts(&r,"Name: ");        khtml_closeelem(&r,1); khtml_puts(&r,squad.GetName().c_str());        khtml_elem(&r,KELEM_BR);
      khtml_elem(&r,KELEM_B); khtml_puts(&r,"Description: "); khtml_closeelem(&r,1); khtml_puts(&r,squad.GetDescription().c_str()); khtml_elem(&r,KELEM_BR);
      khtml_elem(&r,KELEM_B); khtml_puts(&r,"Validation: ");  khtml_closeelem(&r,1);
      if(issues.size() == 0) {
	khtml_puts(&r,"Ok");
      } else {
	khtml_puts(&r,"Invalid!");
	khtml_elem(&r, KELEM_UL);
	for(std::string s : issues) {
	  khtml_elem(&r, KELEM_LI); khtml_puts(&r, s.c_str()); khtml_closeelem(&r,1);
	}
	khtml_closeelem(&r,1); // UL
      }
      khtml_closeelem(&r,1); // P
    }
    catch(PilotNotFound pnf) {
      khtml_puts(&r, pnf.what());
      return;
    }
    catch(UpgradeNotFound unf) {
      khtml_puts(&r, unf.what());
      return;
    }
    catch(SquadNotFound snf) {
      khtml_puts(&r, snf.what());
      return;
    }
    catch(std::exception e) {
      khtml_puts(&r, "Invalid squad!");
      khtml_elem(&r, KELEM_BR); 
      khtml_puts(&r, e.what());
      return;
    }

    // generate the image...
    try {
      int pc[2];
      int cp[2];
      int er[2];
      int pid;

      if(pipe(pc) < 0) { throw std::runtime_error("pipe(pc)"); }
      if(pipe(cp) < 0) { throw std::runtime_error("pipe(cp)"); }
      if(pipe(er) < 0) { throw std::runtime_error("pipe(er)"); }

      pid = fork();
      switch(pid) {
      case -1: // FAIL!
	throw std::exception();

      case 0: // child
	{
	  dup2(pc[0], 0);
	  dup2(cp[1], 1);
	  dup2(er[1], 2);
	  close(pc[1]);
	  close(cp[0]);
	  close(er[0]);

	  // *sigh* for now do this:
	  //   'linux' - the environment on my web bost
	  //   'else'  - my local openbsd/httpd machine
#ifdef __linux__
	  execlp("/home/sirjorj/bin/xhud1e", "/home/sirjorj/bin/xhud1e", "img", "--", "--", nullptr);
#else
	  execlp("/bin/xhud1e", "/bin/xhud1e", "img", "--", "--", nullptr);
	  // note: if openbsd is just not working, the libraries are probably out of date.
	  // comment out the code below that writes the xws to the child and the lib errors will be shown
#endif

	  // if we get here, exec failed
	  exit(1);
	}
	break;

      default: // parent
	{
	  close(pc[0]);
	  close(cp[1]);
	  close(er[1]);
	  // send squad to child
	  for(char c : xws) {
	    write(pc[1], &c, 1);
	  }
	  close(pc[1]);
	  // get image from child
	  std::stringbuf sb;
	  char ch;
	  while(read(cp[0], &ch, 1) == 1) {
	    sb.sputc(ch);
	  }
	  close(cp[1]);
	  // get any error messages
	  std::string errMsg;
	  while(read(er[0], &ch, 1) == 1) {
	    errMsg += ch;
	  }
	  close(er[1]);
	  // done
	  waitpid(pid, 0, 0);

	  if(errMsg.size()) {
	    khtml_elem(&r,KELEM_B); khtml_puts(&r,"Image Generation Error: ");  khtml_closeelem(&r,1); khtml_puts(&r, errMsg.c_str()); khtml_elem(&r, KELEM_BR);
	  }
	  
	  // draw the image
	  std::istream input(&sb);
	  std::string b64 = GetBase64(input);
	  std::string src = "data:image/png;base64," + b64;
	  khtml_attr(&r, KELEM_IMG, KATTR_SRC, src.c_str(), KATTR__MAX);
	}
	break;
      }
    }
    catch(std::exception e) {
      khtml_puts(&r, "Error generating image!");
      khtml_elem(&r, KELEM_BR); 
      khtml_puts(&r, e.what());
    }
  } else {
    khtml_elem(&r, KELEM_P); khtml_puts(&r, "Paste xws here and press Submit to validate a squad and see an example xhud overlay image."); khtml_closeelem(&r, 1);
    khtml_attr(&r, KELEM_FORM, KATTR_ACTION, (std::string(req->pname)+"/validator").c_str(), KATTR_METHOD, "POST", KATTR__MAX);
    khtml_attr(&r, KELEM_TEXTAREA, KATTR_NAME, "xws", KATTR_ROWS, "20", KATTR_COLS, "80", KATTR__MAX);  khtml_closeelem(&r, 1);
    khtml_elem(&r, KELEM_BR);
    khtml_attr(&r, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "Submit", KATTR__MAX);
    khtml_closeelem(&r, 1); // form
  }

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
