#include "damagedecks.h"

using namespace libxwing;



void damagedecks(struct kreq *req) {
  struct khtmlreq  r;

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath);

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}}, req->pagename);

  khtml_elem(&r, KELEM_TABLE);
  for(Deck d : Deck::GetAllDecks()) {
    khtml_elem(&r, KELEM_TR);
    { // Name
      bool isHugeShip = (d.GetXws() != "core" && d.GetXws() != "core2");
      std::string css;
      if(isHugeShip) { css = "text"; }
      else           { css = "bold"; }
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
      std::string url = std::string(req->pname)+"/damagedeck?dd=" + d.GetXws();
      khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
      khtml_puts(&r, d.GetName().c_str());
      khtml_closeelem(&r, 2); // a, td
    }
    khtml_closeelem(&r, 1); // tr
  }
  khtml_closeelem(&r, 1); // tab
  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
