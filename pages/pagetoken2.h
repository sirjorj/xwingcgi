#pragma once
#include <libxwing2/pointlist.h>

#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <kcgi.h>

#include <map>
#include <string>

using namespace libxwing2;



class PageToken {
 public:
  // keys
  const static std::string Cost;
  const static std::string Deck;
  const static std::string Format;
  const static std::string Image;
  const static std::string LJ;
  const static std::string Mode;
  const static std::string Name;
  const static std::string OSD;
  const static std::string Pilot;
  const static std::string PointList;
  const static std::string Restriction;
  const static std::string Search;
  const static std::string Ship;
  const static std::string SKU;
  const static std::string Squad;
  const static std::string Upgrade;
  const static std::string Validate;
  const static std::string XWI;
  const static std::string XWS;
  const static std::string YASB;

  // constructor
  PageToken(struct kreq *kr);

  // generic accessors
  std::string GetScheme() const;
  std::string GetHost()   const;
  std::string GetPName()  const;
  std::string GetPath()   const;
  const std::map<std::string,std::string>& GetArgs() const;

  bool HasArg(std::string k)     const;
  std::string Get(std::string k) const;

  
  
  // specialized accessors
  PtL GetPtL() const;

private:
  const std::string scheme;
  const std::string host;
  const std::string pname;
  const std::string path;
  std::map<std::string,std::string> args;
};



class LinkBuilder {
public:
  LinkBuilder(const PageToken &pt, std::string p);
  
  LinkBuilder& AddArg(std::string k);
  LinkBuilder& AddArg(std::string k, std::string v);
  LinkBuilder& AllArgs();

  std::string Get();

private:
  const PageToken &pt;
  std::string path;
  std::map<std::string,std::string> args;
};
