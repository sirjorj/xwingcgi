#include "ships2.h"
#include "pagetoken2.h"
#include <libxwing2/converter.h>
#include "../json/json.h"

using namespace libxwing2;

void ReturnError(struct kreq *kr, std::string error, std::string what) {
  struct khtmlreq  r;
  resp_open(kr, KHTTP_500);
  khtml_open(&r, kr, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khttp_puts(kr, error.c_str());
  khttp_puts(kr, " - ");
  khttp_puts(kr, what.c_str());
  khtml_close(&r);
}

void ships2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format = pt.Get(PageToken::Format);

  if(format == "" || format == "table") {  
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      css.ShipRow();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    // list of ships
    khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);

    for(Shp shp : Ship::GetAllShps()) {
      Ship ship = Ship::GetShip(shp);
      khtml_elem(&hr, KELEM_TR);
      PrintShipRow(kr, hr, pt, ship);
      khtml_closeelem(&hr, 1); // tr
    }

    khtml_closeelem(&hr, 1); //table

    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      khtml_puts(&hr, "[");
      khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "ships2").AddArg(PageToken::Format, "json").Get().c_str(), KATTR__MAX);
      khtml_puts(&hr, "json");
      khtml_closeelem(&hr, 1); // a
      khtml_puts(&hr, "]");
      khtml_closeelem(&hr, 1); // rd
      khtml_closeelem(&hr, 2); // tr, table
    }

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }
  else if(format == "json") {
    int pc=0;
    Json::Value ships;
    try {
      for(Shp shp : Ship::GetAllShps()) {
	Ship ship = Ship::GetShip(shp);
	// faction
	std::vector<Fac> facs;
	for(Plt plt : Pilot::GetAllPlts()) {
	  Pilot pilot = Pilot::GetPilot(plt);
	  if(pilot.GetShp() == shp) {
	    if(std::find(facs.begin(), facs.end(), pilot.GetFac()) == facs.end()) {
	      facs.push_back(pilot.GetFac());
	    }
	  }
	}

	int fc=0;
	// maybe sort facs here?
	for(Fac fac : facs) {
	  ships[pc]["factions"][fc++] = Faction::GetFaction(fac).GetName();
	}
	/*
	if((f & Fac::Rebel)      == Fac::Rebel)      { ships[pc]["factions"][fc++] = Faction::GetFaction(Fac::Rebel).GetName(); }
	if((f & Fac::Imperial)   == Fac::Imperial)   { ships[pc]["factions"][fc++] = Faction::GetFaction(Fac::Imperial).GetName(); }
	if((f & Fac::Scum)       == Fac::Scum)       { ships[pc]["factions"][fc++] = Faction::GetFaction(Fac::Scum).GetName(); }
	if((f & Fac::Resistance) == Fac::Resistance) { ships[pc]["factions"][fc++] = Faction::GetFaction(Fac::Resistance).GetName(); }
	if((f & Fac::FirstOrder) == Fac::FirstOrder) { ships[pc]["factions"][fc++] = Faction::GetFaction(Fac::FirstOrder).GetName(); }
	*/
	// name
	ships[pc]["name"] = ship.GetName();
	// glyph
	ShipGlyph sg = ShipGlyph::GetShipGlyph(ship.GetShp());
	ships[pc]["glyph"]["scss"] = sg.GetScssName();
	ships[pc]["glyph"]["char"] = sg.GetGlyph();
	// dial id
	ships[pc]["dialid"] = ship.GetDialId();
	// maneuvers
	std::string  vas;
	bool first = true;
	vas += ship.GetShortName();
	vas += ".";
	int mc=0;

	for(const Maneuver& m : ship.GetManeuvers()) {
	  // add to the array of maneuvers
	  ships[pc]["maneuvers"]["list"][mc]["speed"] = m.speed;
	  ships[pc]["maneuvers"]["list"][mc]["bearing"] = Bearing::GetBearing(m.bearing).GetShortName();
	  ships[pc]["maneuvers"]["list"][mc]["difficulty"] = Difficulty::GetDifficulty(m.difficulty).GetName();

	  // add to the vassal string
	  if(first) { first = false; }
	  else      { vas += ","; }

	  vas += std::to_string(m.speed);

	  switch(m.bearing) {
	  case Brn::LTurn:       vas += "T"; break;
	  case Brn::LBank:       vas += "B"; break;
	  case Brn::Straight:    vas += "F"; break;
	  case Brn::RBank:       vas += "N"; break;
	  case Brn::RTurn:       vas += "Y"; break;
	  case Brn::KTurn:       vas += "K"; break;
	  case Brn::Stationary:  vas += "O"; break;
	  case Brn::LSloop:      vas += "L"; break;
	  case Brn::RSloop:      vas += "P"; break;
	  case Brn::LTroll:      vas += "E"; break;
	  case Brn::RTroll:      vas += "R"; break;
	  case Brn::RevLBank:    vas += "A"; break;
	  case Brn::RevStraight: vas += "S"; break;
	  case Brn::RevRBank:    vas += "D"; break;
	  }

	  switch(m.difficulty) {
	  case Dif::Red:   vas += "R"; break;
	  case Dif::White: vas += "W"; break;
	  case Dif::Blue:  vas += "B"; break;
	  default: break;
	  }
	  mc++;
	}
	ships[pc]["maneuvers"]["xwvassal"] = vas;
	pc++;
      }
    }
    catch(libxwing2::converter::xws::ShipNotFound &snf) { ReturnError(kr, "xws::ShipNotFound", snf.what()); return; }
    catch(libxwing2::ShipNotFound &snf) { ReturnError(kr, "ShipNotFound", snf.what()); return; }
    catch(libxwing2::ShipGlyphNotFound &snf) { ReturnError(kr, "ShipGlyphNotFound", snf.what()); return; }
    catch(std::exception &ex) { ReturnError(kr, "exception", ex.what()); return ; }
    // add to these...

    std::string jsonStr;
    std::stringstream ss;
    ss << ships;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }
}
