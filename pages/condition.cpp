#include "condition.h"

using namespace libxwing;



void condition(struct kreq *req) {
  struct khtmlreq  r;

  std::string namestr;
  for(int i=0; i<req->fieldsz; i++) {
    if     (strcmp(req->fields[i].key, "name")    == 0) { namestr    = req->fields[i].val; }
  }
  Condition condition = Condition::GetCondition(namestr);

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath + "/" + condition.GetName());

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)}, {std::string(req->pname)+"/conditions", "conditions"}}, condition.GetName());

  khtml_elem(&r, KELEM_TABLE);

  { // name
    khtml_elem(&r, KELEM_TR);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Name"); khtml_closeelem(&r, 1);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, condition.GetName().c_str()); khtml_closeelem(&r, 1);
    khtml_closeelem(&r, 1);
  }
  { // text
    khtml_elem(&r, KELEM_TR);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Text"); khtml_closeelem(&r, 1);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); IconifyText(r, condition.GetText()); khtml_closeelem(&r, 1);
    khtml_closeelem(&r, 1);
  }
  { // availability
    khtml_elem(&r, KELEM_TR);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Availability"); khtml_closeelem(&r, 1);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
    bool first=true;
    for(Release rel : Release::GetAllReleases()) {
      for(Cnd c : rel.GetConditions()) {
	if(c == condition.GetType()) {
	  if(first) { first = false; }
	  else      { khtml_elem(&r, KELEM_BR); }
	  std::string url = std::string(req->pname)+"/release?sku=" + rel.GetSku();
	  khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	  khtml_puts(&r, rel.GetName().c_str());
	  khtml_closeelem(&r, 1);
	  break;
	}
      }
    }
    khtml_closeelem(&r, 1);
  }
  khtml_closeelem(&r, 3); // tr, table, body
  khtml_close(&r);
}
