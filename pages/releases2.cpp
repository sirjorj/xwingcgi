#include "releases2.h"
#include "pagetoken2.h"
#include "../json/json.h"

using namespace libxwing2;

std::string GetDate(uint16_t y, uint8_t m, uint8_t d) {
  char b[11];
  snprintf(b, sizeof(b), "%04d-%02d-%02d", y, m, d);
  return b;
}

std::string GetWaveString(RelGroup r) {
  switch(r) {
  case RelGroup::FeWave14:    return "Wave XIV (1st Ed)";
  case RelGroup::CoreSet:     return "Core Set";
  case RelGroup::Accessories: return "Accessories";
  case RelGroup::CardPack:    return "Card Pack";
  case RelGroup::Huge:        return "Huge Ships";
  case RelGroup::Wave1:       return "Wave I";
  case RelGroup::Wave2:       return "Wave II";
  case RelGroup::Wave3:       return "Wave III";
  case RelGroup::Wave4:       return "Wave IV";
  case RelGroup::Wave5:       return "Wave V";
  case RelGroup::Wave6:       return "Wave VI";
  case RelGroup::Wave7:       return "Wave VII";
  case RelGroup::Wave8:       return "Wave VIII";
  case RelGroup::Wave9:       return "Wave IX";
  default:                    return "";
  }
}

void releases2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format=pt.Get(PageToken::Format);

  if(format == "") {
    struct khtmlreq hr;

    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      css.Releases();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    // print list of releases
    khtml_elem(&hr, KELEM_TABLE);
    for(Rel rel : Release::GetAllRels()) {
      Release release = Release::GetRelease(rel);
      std::string css = (release.IsUnreleased()) ? "unr" : "";
      khtml_elem(&hr, KELEM_TR);
      { // sku 
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX);
	khtml_puts(&hr, release.GetSku().c_str());
	khtml_closeelem(&hr, 1);
      }
      { // wave
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	khtml_puts(&hr, GetWaveString(release.GetGroup()).c_str());
	khtml_closeelem(&hr, 1);
      }
      { // expansion name
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, css.c_str(), KATTR__MAX);
	PrintReleaseName(kr, hr, pt, release);
	khtml_closeelem(&hr, 1);
      }
      khtml_closeelem(&hr, 1); // tr
    }
    khtml_closeelem(&hr, 1); // table

    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR);
      khtml_elem(&hr, KELEM_TD);
      khtml_puts(&hr, "[");
      khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"releases2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").Get().c_str(), KATTR__MAX);
      khtml_puts(&hr, "json");
      khtml_closeelem(&hr, 1); // a
      khtml_puts(&hr, "]");
      khtml_closeelem(&hr, 1); // rd

      khtml_closeelem(&hr, 2); // tr, table
    }

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else if(format == "json") {
    int rc=0;
    Json::Value jReleases;
    for(Rel rel : Release::GetAllRels()) {
      Release release = Release::GetRelease(rel);
      jReleases[rc]["sku"] = release.GetSku();
      jReleases[rc]["isbm"] = release.GetIsbn();
      jReleases[rc]["name"] = release.GetName();
      jReleases[rc]["dates"]["announce"] = GetDate(release.GetAnnounceDate().year, release.GetAnnounceDate().month, release.GetAnnounceDate().date);
      if(release.GetReleaseDate().year) {
	jReleases[rc]["dates"]["release"] = GetDate(release.GetReleaseDate().year, release.GetReleaseDate().month, release.GetReleaseDate().date);
      }
      if(release.GetAnnouncementArticle().url != "") {
	jReleases[rc]["articles"]["announcement"] = release.GetAnnouncementArticle().url;
      }
      {
	int pc=0;
	for(const Article& pa : release.GetPreviewArticles()) { jReleases[rc]["articles"]["preview"][pc++] = pa.url; }
      }
      if(release.GetReleaseArticle().url != "") {
	jReleases[rc]["articles"]["release"] = release.GetReleaseArticle().url;
      }
      {
	int sc=0;
	for(auto s : release.GetShips()) {
	  jReleases[rc]["ships"][sc++] = Ship::GetShip(s.ship).GetName();
	}
      }
      {
	int pc=0;
	for(Plt p : release.GetPilots()) {
	  jReleases[rc]["pilots"][pc++] = Pilot::GetPilot(p).GetName();
	}
      }
      {
	int uc=0;
	for(Upg u : release.GetUpgrades()) {
	  jReleases[rc]["upgrades"][uc++] = Upgrade::GetUpgrade(u).GetName();
	}
      }
      if(release.IsUnreleased()) {
	jReleases[rc]["unreleased"] = true;
      }
      { // tokens
	int tCount=0;
	std::map<Tok,std::shared_ptr<Tokens>> tc = release.GetTokens().GetAllTokens();
	for(std::pair<Tok,std::shared_ptr<Tokens>> t : tc) {
	  Tok tok = t.first;
	  Token token = Token::GetToken(tok);
	  std::shared_ptr<Tokens> tokensSP = t.second;
	  Tokens tokens = *tokensSP.get();
	  if(tok == Tok::Dial) {
	    int dc=0;
	    Dials dials = *dynamic_cast<Dials*>(tokensSP.get());
	    for(const std::pair<Shp,Fac>& d : dials.GetDials()) {
	      jReleases[rc]["tokens"][tCount]["dial"][dc]["ship"] = Ship::GetShip(d.first).GetName();
	      jReleases[rc]["tokens"][tCount]["dial"][dc]["faction"] = Faction::GetFaction(d.second).GetName();
	      dc++;
	    }
	  }
	  else if(tok == Tok::DialId) {
	    int dc=0;
	    DialIds dialids = *dynamic_cast<DialIds*>(tokensSP.get());
	    for(const std::pair<const Shp,int>& d : dialids.GetDialIds()) {
	      jReleases[rc]["tokens"][tCount]["dialid"][dc]["ship"] = Ship::GetShip(d.first).GetName();
	      jReleases[rc]["tokens"][tCount]["dialid"][dc]["count"] = d.second;
	      dc++;
	    }
	  }
	  else if(tok == Tok::Ship) {
	    int sc=0;
	    ShTokens st = *dynamic_cast<ShTokens*>(tokensSP.get());
	    std::vector<std::pair<Plt,Plt>> pilots = st.GetPilots();
	    for(std::pair<Plt,Plt> pp : pilots) {
	      jReleases[rc]["tokens"][tCount]["ship"][sc][0] = Pilot::GetPilot(pp.first).GetName();
	      jReleases[rc]["tokens"][tCount]["ship"][sc][1] = Pilot::GetPilot(pp.second).GetName();
	      sc++;
	    }
	  }
	  else if((tok==Tok::ID) || (tok==Tok::Lock)) {
	    int lc=0;
	    IDTokens idt = *dynamic_cast<IDTokens*>(tokensSP.get());
	    std::map<int,int> idCounts = idt.GetIDCounts();
	    for(std::pair<int,int> idCount : idCounts) {
	      jReleases[rc]["tokens"][tCount][token.GetName()][std::to_string(idCount.first)] = idCount.second;
	      lc++;
	    }
	  }
	  else if((tok==Tok::SmallSingleTurret) || (tok==Tok::SmallDoubleTurret) ||
		  (tok==Tok::LargeSingleTurret) || (tok==Tok::LargeDoubleTurret) ||
		  (tok==Tok::HugeSingleTurret)  || (tok==Tok::HugeDoubleTurret)) {
	    TurretTokens tt = *dynamic_cast<TurretTokens*>(tokensSP.get());
	    std::map<std::optional<Fac>,int> turretCounts = tt.GetFacCounts();
	    for(std::pair<std::optional<Fac>,int> trtCount : turretCounts) {
	      jReleases[rc]["tokens"][tCount][token.GetName()][trtCount.first ? Faction::GetFaction(*trtCount.first).GetShort() : "None"] = trtCount.second;
	    }
	  }
	  else {
	    jReleases[rc]["tokens"][tCount][token.GetName()] = tokens.GetCount();
	  }
	  tCount++;
	}
      }
      rc++;
    }

    std::string jsonStr;
    std::stringstream ss;
    ss << jReleases;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }
}
