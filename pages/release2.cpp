#include "release2.h"
#include "pagetoken2.h"

using namespace libxwing2;

void release2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string skustr =pt.Get(PageToken::SKU);

  struct khtmlreq hr;

  resp_open(kr, KHTTP_200);
  khtml_open(&hr, kr, KHTML_PRETTY);
  khtml_elem(&hr, KELEM_DOCTYPE);
  khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  try {
    Release release = Release::GetRelease(skustr);

    { // head
      CSS css;
      css.ReleaseInfo();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath + " [" + release.GetName() + "]", css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)},
			      {LinkBuilder(pt,"releases2").AddArg(PageToken::PointList).Get(), "releases2"}
      }, release.GetName(), GetPointsListForBreadcrumbs(hr, ptl, pt));

    { // header
      khtml_elem(&hr, KELEM_DIV);
      { // name
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "titleText", KATTR__MAX);
	khtml_puts(&hr, release.GetName().c_str());
	khtml_closeelem(&hr, 1);
      }
       khtml_closeelem(&hr, 1);
    }
    // info
    khtml_elem(&hr, KELEM_TABLE);
    { // sku
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "SKU"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, release.GetSku().c_str()); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }
    { // isbn
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "ISBN"); khtml_closeelem(&hr, 1);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, release.GetIsbn().c_str()); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }
    { // announced
      char date[11];
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Announced"); khtml_closeelem(&hr, 1);
      snprintf(date, sizeof(date), "%04d.%02d.%02d", release.GetAnnounceDate().year, release.GetAnnounceDate().month, release.GetAnnounceDate().date);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, date); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }
    { // released
      char date[11];
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR__MAX); khtml_puts(&hr, "Released"); khtml_closeelem(&hr, 1);
      snprintf(date, sizeof(date), "%04d.%02d.%02d", release.GetReleaseDate().year, release.GetReleaseDate().month, release.GetReleaseDate().date);
      khtml_attr(&hr, KELEM_TD, KATTR__MAX); khtml_puts(&hr, date); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1);
    }
    { // articles
      std::vector<std::pair<std::string, std::string>> urls;
      if(release.GetAnnouncementArticle().url != "")        { urls.push_back({"Announcement", release.GetAnnouncementArticle().url}); }
      for(const Article& pa : release.GetPreviewArticles()) { urls.push_back({"Preview", pa.url}); }
      if(release.GetReleaseArticle().url != "")             { urls.push_back({"Release", release.GetReleaseArticle().url}); }
      bool printedLabel = false;
      for(auto url : urls) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(urls.size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Articles");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	khtml_attr(&hr, KELEM_A, KATTR_HREF, url.second.c_str(), KATTR__MAX);
	khtml_puts(&hr, url.first.c_str());
	khtml_closeelem(&hr, 3);
      }
    }
    { // ships
      bool printedLabel = false;
      for(auto sh : release.GetShips()) {
	Ship ship = Ship::GetShip(sh.ship);
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetShips().size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr,release.GetShips().size() > 1 ? "Ships" : "Ship");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"ship2").AddArg(PageToken::PointList).AddArg(PageToken::Ship,ship.GetShpStr()).Get().c_str(), KATTR__MAX);
	khtml_puts(&hr, ship.GetName().c_str());
	khtml_closeelem(&hr, 3);
      }
    }
    { // pilots
      bool printedLabel = false;
      for(Plt rp : release.GetPilots()) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetPilots().size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Pilots");
	  khtml_closeelem(&hr, 1); // td (label)
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	try {
	  const Pilot pilot = Pilot::GetPilot(rp);
	  PrintPilotName(kr, hr, pt, pilot);
	  khtml_closeelem(&hr, 1);

	}
	catch(PilotNotFound pnf) {
	  PrintError(hr, pnf.what());
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1); // tr
      }
    }
    { // upgrades
      bool printedLabel = false;
      for(Upg u : release.GetUpgrades()) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetUpgrades().size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Upgrades");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	try {
	  const Upgrade upgrade = Upgrade::GetUpgrade(u);
	  PrintUpgradeName(kr, hr, pt, upgrade);
	  khtml_closeelem(&hr, 1);
	}
	catch (UpgradeNotFound unf) {
	  PrintError(hr, unf.what());
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 1); // tr
      }
    }
    { // conditions
      bool printedLabel = false;
      for(Cnd c : release.GetConditions()) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(release.GetConditions().size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, release.GetConditions().size() > 1 ? "Conditions" : "Condition");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	try {
	  const Condition condition = Condition::GetCondition(c);
	  PrintConditionName(kr, hr, pt, condition);
	  khtml_closeelem(&hr, 1);
	}
	catch(ConditionNotFound cnf) {
	  PrintError(hr, cnf.what());
	  khtml_closeelem(&hr, 1);
	}
      }
    }
    { // dials
      std::map<std::pair<Shp,Fac>,int> data;
      Dials dials = release.GetTokens().GetDials();
      for(const std::pair<Shp,Fac>& d : dials.GetDials()) {
	data[d]++;
      }
      bool printedLabel = false;
      for(const std::pair<const std::pair<Shp,Fac>,int>& d : data) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(data.size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Dials");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	std::ostringstream oss;
	oss << std::to_string(d.second) << "x " << Ship::GetShip(d.first.first).GetName() << " (" << Faction::GetFaction(d.first.second).GetShort() << ")";
	khtml_puts(&hr, oss.str().c_str());
	khtml_closeelem(&hr, 2); // td tr
      }
    }
    { // dialids
      bool printedLabel = false;
      DialIds dialids = release.GetTokens().GetDialIds();
      for(const std::pair<const Shp,int>& did : dialids.GetDialIds()) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(dialids.GetDialIds().size()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Dial IDs");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	std::string dialidStr = std::to_string(did.second) + "x " + Ship::GetShip(did.first).GetName();// + " / " + did.second;
	khtml_puts(&hr, dialidStr.c_str());
	khtml_closeelem(&hr, 2); // td tr
      }
    }
    { // tokens
      bool printedLabel = false;
      std::map<Tok,std::shared_ptr<Tokens>> tc = release.GetTokens().GetAllTokens();
      for(std::pair<Tok,std::shared_ptr<Tokens>> t : tc) {
	Tok tok = t.first;
	if(tok == Tok::Dial)   { continue; }
	if(tok == Tok::DialId) { continue; }
	if(tok == Tok::Ship)   { continue; }
	std::shared_ptr<Tokens> tokensSP = t.second;
	Tokens tokens = *tokensSP.get();
	int count = tokens.GetCount();
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  int ignored = 0;
	  ignored += (tc.find(Tok::Dial)   != tc.end()) ? 1 : 0;
	  ignored += (tc.find(Tok::DialId) != tc.end()) ? 1 : 0;
	  ignored += (tc.find(Tok::Ship)   != tc.end()) ? 1 : 0;
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(tc.size()-ignored).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Tokens");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	std::string tokStr = "x" + std::to_string(count) + " " + Token::GetToken(tok).GetName();
	if(false) { }
	else if((tok==Tok::ID) || (tok==Tok::Lock)) {
	  tokStr += " (";
	  bool f = true;
	  IDTokens idt = *dynamic_cast<IDTokens*>(tokensSP.get());
	  std::map<int,int> idCounts = idt.GetIDCounts();
	  for(std::pair<int,int> idCount : idCounts) {
	    if(!f) { tokStr += ", "; } else { f = false; }
	    tokStr += std::to_string(idCount.first);
	  }
	  tokStr += ")";
	}
	else if((tok==Tok::SmallSingleTurret) || (tok==Tok::SmallDoubleTurret) ||
		(tok==Tok::LargeSingleTurret) || (tok==Tok::LargeDoubleTurret) ||
		(tok==Tok::HugeSingleTurret)  || (tok==Tok::HugeDoubleTurret)) {
	  tokStr += " (";
	  bool f = true;
	  TurretTokens tt = *dynamic_cast<TurretTokens*>(tokensSP.get());
	  std::map<std::optional<Fac>,int> turretCounts = tt.GetFacCounts();
	  for(std::pair<std::optional<Fac>,int> trtCount : turretCounts) {
	    if(!f) { tokStr += ", "; } else { f = false; }
	    tokStr += trtCount.first ? Faction::GetFaction(*trtCount.first).GetShort() : "";
	  }
	  tokStr += ")";
	}
	khtml_puts(&hr, tokStr.c_str());
      }
    }
    { // ship tokens
      bool printedLabel = false;
      ShTokens shipTokens = release.GetTokens().GetShipTokens();
      for(const std::pair<Plt,Plt>& st : shipTokens.GetPilots()) {
	khtml_elem(&hr, KELEM_TR);
	if(!printedLabel) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "key", KATTR_ROWSPAN, std::to_string(shipTokens.GetCount()).c_str(), KATTR__MAX);
	  khtml_puts(&hr, "Ship Tokens");
	  khtml_closeelem(&hr, 1);
	  printedLabel = true;
	}
	khtml_attr(&hr, KELEM_TD, KATTR__MAX);
	std::string tokStr = Pilot::GetPilot(st.first).GetName() + " / " + Pilot::GetPilot(st.second).GetName();
	khtml_puts(&hr, tokStr.c_str());
	khtml_closeelem(&hr, 2); // td tr
      }
    }
  }
  catch(ReleaseNotFound rnf) { khtml_puts(&hr, rnf.what()); }
  catch(ShipNotFound    snf) { khtml_puts(&hr, snf.what()); }
  catch(PilotNotFound   pnf) { khtml_puts(&hr, pnf.what()); }
  catch(UpgradeNotFound unf) { khtml_puts(&hr, unf.what()); }
  catch(TokenNotFound   tnf) { khtml_puts(&hr, tnf.what()); }
  catch(converter::xws::ShipNotFound    snf) { khtml_puts(&hr, snf.what()); }
  catch(converter::xws::PilotNotFound   pnf) { khtml_puts(&hr, pnf.what()); }
  catch(converter::xws::UpgradeNotFound unf) { khtml_puts(&hr, unf.what()); }

  khtml_closeelem(&hr, 1); // table
  khtml_closeelem(&hr, 1); // body
  khtml_close(&hr);
}
