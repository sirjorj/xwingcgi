#include "upgrade.h"

using namespace libxwing;



void upgrade(struct kreq *req) {
  struct khtmlreq  r;

  std::string typestr, namestr;
  for(int i=0; i<req->fieldsz; i++) {
    if     (strcmp(req->fields[i].key, "type") == 0) { typestr = req->fields[i].val; }
    else if(strcmp(req->fields[i].key, "name") == 0) { namestr = req->fields[i].val; }
  }
  Upgrade upgrade = Upgrade::GetUpgrade(typestr, namestr);

  // page
  resp_open(req, KHTTP_200);
  khtml_open(&r, req, KHTML_PRETTY);
  khtml_elem(&r, KELEM_DOCTYPE);
  khtml_attr(&r, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

  // head
  PrintHead(req, r, std::string(req->pname) + req->fullpath + " [" + upgrade.GetName() + "]");

  // body
  khtml_elem(&r, KELEM_BODY);
  PrintBreadcrumbs(req, r, {{req->pname, StripLeadingSlash(req->pname)},{std::string(req->pname)+"/upgrades", "upgrades"}}, upgrade.GetName());

  std::string format;
  for(int i=0; i<req->fieldsz; i++) {
    if(strcmp(req->fields[i].key, "format") == 0) {
      format = req->fields[i].val;
      break;
    }
  }

  if((format == "") || (format == "table")) {
    int sides = upgrade.IsDualSided() ? 2 : 1;
    for(int i=0; i<sides; i++) {
      if(i) { khtml_elem(&r, KELEM_BR); }

      khtml_elem(&r, KELEM_TABLE);
      { // name
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Name"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.GetName().c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // xws
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Name (xws)"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.GetXws().c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // short name
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Name (short)"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.GetShortName().c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // upgrade type
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Type"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.GetType().GetName().c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // unique
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Is Unique"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.IsUnique() ? "Yes" : "No"); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // limited
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Is Limited"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, upgrade.IsLimited() ? "Yes" : "No"); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // cost
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Cost"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, std::to_string(upgrade.GetCost()).c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // attack & range
	if(upgrade.GetAttackStats()) {
	  khtml_elem(&r, KELEM_TR);
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Attack"); khtml_closeelem(&r, 1);
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX); khtml_puts(&r, std::to_string(upgrade.GetAttackStats()->attack).c_str()); khtml_closeelem(&r, 1);
	  khtml_closeelem(&r, 1);
	  
	  khtml_elem(&r, KELEM_TR);
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",   KATTR__MAX); khtml_puts(&r, "Range"); khtml_closeelem(&r, 1);
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value", KATTR__MAX);
	  if(upgrade.GetAttackStats()->minRange == upgrade.GetAttackStats()->maxRange) {
	    khtml_puts(&r, std::to_string(upgrade.GetAttackStats()->minRange).c_str());
	  } else {
	    khtml_puts(&r, (std::to_string(upgrade.GetAttackStats()->minRange)+ "-" + std::to_string(upgrade.GetAttackStats()->maxRange)).c_str());
	  }
	  khtml_closeelem(&r, 1);
	  khtml_closeelem(&r, 1);
	}
      }
      { // restriction
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",    KATTR__MAX); khtml_puts(&r, "Restriction"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); khtml_puts(&r, upgrade.GetRestrictionText().c_str()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // text
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",    KATTR__MAX); khtml_puts(&r, "Text"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX); IconifyText(r, upgrade.GetText()); khtml_closeelem(&r, 1);
	khtml_closeelem(&r, 1);
      }
      { // availability
	khtml_elem(&r, KELEM_TR);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "key",    KATTR__MAX); khtml_puts(&r, "Availability"); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "value",  KATTR__MAX);
	int first=true;
	for(Release rel : Release::GetUpgrade(upgrade.GetType().GetType(), upgrade.GetXws())) {
	  if(first) { first = false; }
	  else      { khtml_elem(&r, KELEM_BR); }
	  std::string url = std::string(req->pname)+"/release?sku=" + rel.GetSku();
	  khtml_attr(&r, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
	  khtml_puts(&r, rel.GetName().c_str());
	  khtml_closeelem(&r, 1);
	}
      }
      khtml_closeelem(&r, 2);
      khtml_closeelem(&r, 1); // table
      upgrade.Flip();
    }
      
    // card view link
    khtml_elem(&r, KELEM_P);
    khtml_puts(&r, "[");
    std::string uUrl = std::string(req->pname)+"/upgrade?type=" + upgrade.GetType().GetXws() + "&name=" + upgrade.GetXws() + "&format=card";
    khtml_attr(&r, KELEM_A, KATTR_HREF, uUrl.c_str(), KATTR__MAX);
    khtml_puts(&r, "View as card");
    khtml_closeelem(&r, 1); // a
    khtml_puts(&r, "]");
    khtml_closeelem(&r, 1); // p
  }

  else if(format == "card") {
    int sides = upgrade.IsDualSided() ? 2 : 1;
    for(int i=0; i<sides; i++) {
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucUpgrade", KATTR__MAX);
      bool hasAttack = upgrade.GetAttackStats() != std::nullopt;
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, hasAttack ? "ucNameA" : "ucName", KATTR__MAX);
      if(upgrade.IsUnique()) {
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX); khtml_puts(&r, IconGlyph::GetGlyph(Ico::unique).c_str()); khtml_closeelem(&r, 1);
      }
      khtml_puts(&r, upgrade.GetName().c_str());
      khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, hasAttack ? "ucRestrictionsA" : "ucRestrictions", KATTR__MAX); khtml_puts(&r, upgrade.GetRestrictionText().c_str()); khtml_closeelem(&r, 1);
      if(hasAttack) {
	std::string power = std::to_string(upgrade.GetAttackStats()->attack);
	std::string range = (upgrade.GetAttackStats()->minRange == upgrade.GetAttackStats()->maxRange)
	  ? std::to_string(upgrade.GetAttackStats()->minRange)
	  : std::to_string(upgrade.GetAttackStats()->minRange) + "-" + std::to_string(upgrade.GetAttackStats()->maxRange);
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucAttackPower", KATTR__MAX);  khtml_puts(&r, power.c_str()); khtml_closeelem(&r, 1);
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucAttackRange", KATTR__MAX);  khtml_puts(&r, range.c_str()); khtml_closeelem(&r, 1);
      }
      if(upgrade.GetEnergyLimit()) {
	std::string limit = std::to_string(*upgrade.GetEnergyLimit());
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucEnergyLimit", KATTR__MAX);  khtml_puts(&r, limit.c_str()); khtml_closeelem(&r, 1);
      }
      if(upgrade.GetEnergyModifier()) {
	std::string modifier = ((*upgrade.GetEnergyModifier() >= 0) ? "+" : "") + std::to_string(*upgrade.GetEnergyModifier());
	khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucEnergyModifier", KATTR__MAX);  khtml_puts(&r, modifier.c_str()); khtml_closeelem(&r, 1);
      }

      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucText", KATTR__MAX);     IconifyText(r, upgrade.GetText()); khtml_closeelem(&r, 1);
      std::string slots;
      for(Upg u : upgrade.GetReqSlots()) {
	slots += IconGlyph::GetGlyph(u);
      }
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucTypeIcon", KATTR__MAX); khtml_puts(&r, slots.c_str()); khtml_closeelem(&r, 1);
      khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "ucCost", KATTR__MAX);     khtml_puts(&r, std::to_string(upgrade.GetCost()).c_str()); khtml_closeelem(&r, 1);
      khtml_closeelem(&r, 1); // div
      upgrade.Flip();
    }
  }

  else {
    khtml_puts(&r, "Unknown format");
  }

  khtml_closeelem(&r, 1); // body
  khtml_close(&r);
}
