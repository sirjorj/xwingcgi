#include "xwingcgi.h"

using namespace libxwing;

// publics

void PrintManeuvers(struct khtmlreq &r, Maneuvers maneuvers) {
  Maneuver m;
  int8_t min = 10, max = -10;
  for(auto a : maneuvers) { if(a.speed > max) {max = a.speed;} if(a.speed < min) {min=a.speed;} }
  khtml_attr(&r, KELEM_DIV, KATTR_CLASS, "div_Maneuvers", KATTR__MAX);
  khtml_attr(&r, KELEM_TABLE, KATTR_CLASS, "tab_Maneuvers", KATTR__MAX);
  // forward/stationary
  for(int s=max; s>=min; s--) {
    khtml_elem(&r, KELEM_TR);
    khtml_attr(&r, KELEM_TD, KATTR_CLASS, "td_ManeuverSpeed", KATTR__MAX);
    khtml_int(&r, s);
    khtml_closeelem(&r, 1);
    // regular
    for(Brn b : {Brn::LTurn, Brn::LBank, (s==0) ? Brn::Stationary : Brn::Straight, Brn::RBank, Brn::RTurn}) {
      if(FindManeuver(maneuvers, s, b, m)) {
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, (m.difficulty==Difficulty::Red) ? "td_ManeuverRed" : (m.difficulty==Difficulty::White) ? "td_ManeuverWhite" : "td_ManeuverGreen", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(m.bearing).c_str());
	khtml_closeelem(&r, 1);
      } else {
      	khtml_attr(&r, KELEM_TD, KATTR_CLASS, "td_ManeuverWhite", KATTR__MAX);
	khtml_closeelem(&r, 1);
      }
    }
    // special
    for(Brn b : {Brn::KTurn, Brn::LSloop, Brn::RSloop, Brn::LTroll, Brn::RTroll}) {
      if(FindManeuver(maneuvers, s, b, m)) {
	khtml_attr(&r, KELEM_TD, KATTR_CLASS, (m.difficulty==Difficulty::Red) ? "td_ManeuverRed" : (m.difficulty==Difficulty::White) ? "td_ManeuverWhite" : "td_ManeuverGreen", KATTR__MAX);
	khtml_puts(&r, IconGlyph::GetGlyph(m.bearing).c_str());
	khtml_closeelem(&r, 1);
      }
    }
    khtml_closeelem(&r, 1); // tr
  }
  // reverse
  for(int s=min; s<=max; s++) {
    bool doIt = false;
    for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
      if(FindManeuver(maneuvers, s, b, m)) {
        doIt = true;
        break;
      }
    }
    if(doIt) {
      khtml_elem(&r, KELEM_TR);
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "td_ManeuverSpeed", KATTR__MAX);
      khtml_int(&r, s*-1);
      khtml_closeelem(&r, 1);
      // pad the LTurn spot
      khtml_attr(&r, KELEM_TD, KATTR_CLASS, "td_ManeuverWhite", KATTR__MAX); khtml_closeelem(&r, 1);
      for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
	if(FindManeuver(maneuvers, s, b, m)) {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, (m.difficulty==Difficulty::Red) ? "td_ManeuverRed" : (m.difficulty==Difficulty::White) ? "td_ManeuverWhite" : "td_ManeuverGreen", KATTR__MAX);
	  khtml_puts(&r, IconGlyph::GetGlyph(m.bearing).c_str());
	  khtml_closeelem(&r, 1); // td
	} else {
	  khtml_attr(&r, KELEM_TD, KATTR_CLASS, "", KATTR__MAX); khtml_closeelem(&r, 1);
	}
      }
      khtml_closeelem(&r, 1); // tr
    }
  }
  khtml_closeelem(&r, 1); // table
  khtml_closeelem(&r, 1); // div
}



void IconifyText(struct khtmlreq &r, std::string s) {
  std::string buffer;
  std::string holder;
  bool isInHolder = false;
  for(char c : s) {
    switch(c) {
    case '{':
      isInHolder = true;
      holder += c;
      break;

    case '}': {
      holder += c;
      std::string glyph = IconGlyph::GetGlyph(holder);
      if(glyph != "") {
	khtml_puts(&r, buffer.c_str());
	khtml_attr(&r, KELEM_SPAN, KATTR_CLASS, "iconGlyph", KATTR__MAX);
	khtml_puts(&r, glyph.c_str());
	khtml_closeelem(&r, 1);
	buffer = "";
	holder = "";
      }
      else {
	buffer += holder;
	holder = "";
      }
      isInHolder = false;
      break;
    }

    default:
      if(isInHolder) {
	holder += c;
      } else {
	buffer += c;
      }
    }
  }
  if(buffer.size()) {
    khtml_puts(&r, buffer.c_str());
  }
}
