#include "pilots2.h"
#include "pagetoken2.h"
#include "../json/json.h"
#include <algorithm>
#include <iostream>
#include <iomanip>

using namespace libxwing2;

static std::map<Fac, std::map<Shp,std::vector<Pilot>>> GetPilotsByFacShpPlt(const std::string& restriction) {
  std::map<Fac, std::map<Shp,std::vector<Pilot>>> data;
  for(Plt plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if(restriction == "hyperspace") {
      if((pilot.GetHyperspace() == std::nullopt) || (*pilot.GetHyperspace() == false)) {
	continue;
      }
    }
    data[pilot.GetFac()][pilot.GetShp()].push_back(pilot);
  }
  return data;
}

static std::vector<Pilot> GetPilots(const std::string& restriction) {
  std::vector<Pilot> data;
  for(Plt plt : Pilot::GetAllPlts()) {
    Pilot pilot = Pilot::GetPilot(plt);
    if(restriction == "hyperspace") {
      if((pilot.GetHyperspace() == std::nullopt) || (*pilot.GetHyperspace() == false)) {
	continue;
      }
    }
    data.push_back(pilot);
  }
  return data;
}

static std::string CSV(std::string txt) {
  std::stringstream ss;
  bool enclose = false;
  for(const char c : txt) {
    if(c == ',') {
      enclose = true;
    }
    if(c == '"') {
      enclose = true;
      ss << '"' << '"';
    } else {
      ss << c;
    }
  }
  return (enclose ? "\"" : "") + ss.str() + (enclose ? "\"" : "");
}

static std::string GetKeywords(std::optional<KeywordList> keywordlist) {
  std::stringstream ss;
  if(keywordlist) {
    KeywordList kwl = *keywordlist;
    bool first = true;
    for(KWd kwd : kwl) {
      if(first) { first = false; }
      else      { ss << ",";     }
      ss << Keyword::GetKeyword(kwd).GetName();
    }
  }
  return ss.str();
}

static std::string GetAttack(const PriAttacks& att) {
  std::stringstream ss;
  for(const PriAttack &pa : att) {
    ss << "[" << FiringArc::GetFiringArc(pa.arc).GetName() << " " << std::to_string(pa.value) << "]";
  }
  return ss.str();
}

static std::string GetChargeable( const Chargeable ch) {
  std::stringstream ss;
  if(ch.GetCapacity()) {
    ss << std::to_string(ch.GetCapacity());
    for(int r=0; r<ch.GetRecurring(); r++) {
      ss << "^";
    }
  }
  return ss.str();
}

static std::string GetActions(const ActionBar& ab) {
  std::stringstream ss;
  bool first = true;
  for(auto a : ab) {
    if(first) { first = false; }
    else      { ss << " "; }
    bool next = false;
    ss << "[";
    for(SAct sa : a) {
      if(next) {
	ss << ">";
      }
      std::string dif;
      switch(sa.difficulty) {
      case Dif::White:  dif = "(W)"; break;
      case Dif::Red:    dif = "(R)"; break;
      case Dif::Purple: dif = "(P)"; break;
      default:          dif = "";  break;
      }
      ss << dif << Action::GetAction(sa.action).GetName();
      next = true;
    }
    ss << "]";
  }
  return ss.str();
}

static std::string GetUpgrades(const std::optional<UpgradeBar>& ub) {
  std::stringstream ss;
  if(ub) {
    bool first = true;
    for(const UpgradeSlot& s : *ub) {
      if(first) { first = false; }
      else      { ss << " "; }
      ss << "(" << UpgradeType::GetUpgradeType(s.GetTypes()[0]).GetName() << ")";
    }
  }
  return ss.str();
}

static std::string GetCost(const std::optional<uint16_t>& cost) {
  std::stringstream ss;
  if(cost) {
    ss << std::to_string(*cost);
  }
  return ss.str();
}

static std::string GetHyperspace(const std::optional<bool>& hyp) {
  std::stringstream ss;
  if(hyp) {
    ss << (*hyp ? "Yes" : "No");
  }
  return ss.str();
}



void pilots2(struct kreq *kr) {
  PageToken pt(kr);
  libxwing2::PtL ptl = pt.GetPtL();
  std::string format=pt.Get(PageToken::Format);
  std::string restriction=pt.Get(PageToken::Restriction);

  if(format == "") {
    struct khtmlreq hr;
    // page
    resp_open(kr, KHTTP_200);
    khtml_open(&hr, kr, KHTML_PRETTY);
    khtml_elem(&hr, KELEM_DOCTYPE);
    khtml_attr(&hr, KELEM_HTML, KATTR_LANG, "en", KATTR__MAX);

    { // head
      CSS css;
      css.Pilots();
      PrintHead(kr, hr, std::string(kr->pname) + kr->fullpath, css.GetCSS());
    }

    // body
    khtml_elem(&hr, KELEM_BODY);
    PrintBreadcrumbs(kr, hr, {
			      {LinkBuilder(pt,"").AddArg(PageToken::PointList).Get(), StripLeadingSlash(kr->pname)}
      }, kr->pagename, GetPointsListForBreadcrumbs(hr, ptl, pt));

    // gather data
    std::map<Fac, std::map<Shp,std::vector<Pilot>>> data = GetPilotsByFacShpPlt(restriction);

    // make a little icon index of faction icon
    khtml_attr(&hr, KELEM_DIV, KATTR_ID, "index", KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "indexTbl", KATTR__MAX);
    khtml_elem(&hr, KELEM_TR);
    for(Fac fac : Faction::GetAllFacs()) {
      Faction faction = Faction::GetFaction(fac);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, ("indexBar icon_" + faction.Get3Letter()).c_str(), KATTR__MAX);
      khtml_attr(&hr, KELEM_A, KATTR_HREF, ("#"+faction.Get3Letter()).c_str(), KATTR_CLASS, "invLink", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "indexSpan", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(faction.GetFac()).c_str());
      khtml_closeelem(&hr, 3); // span a td
    }
    khtml_closeelem(&hr, 3); // tr table div
    khtml_elem(&hr, KELEM_BR);

    // start the flow container
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "container", KATTR__MAX);

    // list of pilots (by faction)
    for(const std::pair<const Fac, std::map<Shp,std::vector<Pilot>>> &d1 : data) {
      Fac fac = d1.first;
      Faction faction = Faction::GetFaction(fac);
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "floater", KATTR__MAX);
      khtml_attr(&hr, KELEM_A, KATTR_ID, faction.Get3Letter().c_str(), KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR__MAX);
      { // faction
	khtml_elem(&hr, KELEM_TR);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, ("icon_" + faction.Get3Letter()).c_str(), KATTR_COLSPAN, "2", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "headerSize", KATTR_TITLE, faction.GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(faction.GetFac()).c_str());
	khtml_closeelem(&hr, 3); // span td tr
      }

      for(Shp shp : Ship::GetAllShps()) {
	std::vector<Pilot> pilots = data[fac][shp];
	bool drawShip=true;
	for(const Pilot& p : pilots) {
	  khtml_elem(&hr, KELEM_TR);
	  if(drawShip) {
	    Ship ship = Ship::GetShip(shp);
	    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipHeader", KATTR_ROWSPAN, std::to_string(pilots.size()).c_str(), KATTR__MAX);
	    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, ship.GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, ShipGlyph::GetGlyph(shp).c_str());
	    khtml_closeelem(&hr, 2); // span td
	    drawShip=false;
	  }
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, p.IsUnreleased() ? "unr" : "", KATTR__MAX);
	  PrintPilotName(kr, hr, pt, p);
	  khtml_closeelem(&hr, 2); // td tr
	}
      }
      khtml_closeelem(&hr, 3); // table a div
    }
    khtml_closeelem(&hr, 1); // div (container)
    
    { // alt formats
      khtml_elem(&hr, KELEM_BR);
      khtml_elem(&hr, KELEM_TABLE);
      khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "Formats"); khtml_closeelem(&hr, 2);
      { // web
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "web"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // text
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "text"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"text").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"text").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // csv
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "csv"); khtml_closeelem(&hr, 1);
	}
	{
	  
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"csv").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"csv").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      { // json
	khtml_elem(&hr, KELEM_TR);
	{
	  khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, "json"); khtml_closeelem(&hr, 1);
	}
	{
	  khtml_elem(&hr, KELEM_TD);
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "everything"); khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, " / ");
	  khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt,"pilots2").AddArg(PageToken::PointList).AddArg(PageToken::Format,"json").AddArg(PageToken::Restriction,"hyperspace").Get().c_str(), KATTR__MAX); khtml_puts(&hr, "hyperspace"); khtml_closeelem(&hr, 1);
	  khtml_closeelem(&hr, 1); // td
	}
      }
      khtml_closeelem(&hr, 1); // table
    }

    khtml_closeelem(&hr, 1); // body
    khtml_close(&hr);
  }

  else if(format == "json") {
    int pc=0;
    Json::Value jPilots;
    std::vector<Pilot> pilots = GetPilots(restriction);

    for(const Pilot& pilot : pilots) {
      jPilots[pc]["name"] = pilot.GetName();
      {
	std::string xws;
	try {
	  xws = converter::xws::GetPilot(pilot.GetPlt());
	}
	catch(converter::xws::PilotNotFound(& pnf)) {
	  xws = "???";
	}
	jPilots[pc]["xws"] = xws;
      }
      {
	int ffg;
	try {
	  ffg = converter::ffg::GetPilot(pilot.GetPlt());
	}
	catch(converter::ffg::PilotNotFound(& pnf)) {
	  ffg = -1;
	}
	jPilots[pc]["ffg"] = (ffg == -1) ? "???" : std::to_string(ffg);
      }

      {
	std::string cardart;
	try {
	  cardart = converter::ffg::GetArt(pilot.GetPlt());
	}
	catch(converter::ffg::PilotNotFound(& pnf)) {
	  cardart = "???";
	}
	jPilots[pc]["cardart"] = cardart;
      }
      {
	std::string cardimg;
	try {
	  cardimg = converter::ffg::GetCard(pilot.GetPlt());
	}
	catch(converter::ffg::PilotNotFound(& pnf)) {
	  cardimg = "???";
	}
	jPilots[pc]["cardimg"] = cardimg;
      }
      if(pilot.GetSubtitle() != "") { jPilots[pc]["subtitle"] = pilot.GetSubtitle(); }
      jPilots[pc]["limited"] = pilot.GetLimited();
      jPilots[pc]["faction"] = pilot.GetFaction().GetName();
      jPilots[pc]["ship"] = pilot.GetShip().GetName();
      jPilots[pc]["basesize"] = pilot.GetShip().GetBaseSize().GetName();
      jPilots[pc]["cost"] = pilot.GetNatCost() ? std::to_string(*pilot.GetNatCost()) : "???";
      jPilots[pc]["hyperspace"] = (pilot.GetHyperspace() && *(pilot.GetHyperspace())) ? "Yes" : "No";
      jPilots[pc]["initiative"] = pilot.GetNatInitiative();
      if(pilot.GetKeywordList()) {
	std::ostringstream oss;
	bool first = true;
	KeywordList kwl = *pilot.GetKeywordList();
	for(const KWd& kwd : kwl) {
	  if(first) { first = false; }
	  else      { oss << ",";    }
	  oss << Keyword::GetKeyword(kwd).GetName();
	}
	if(oss.str().size()) {
	  jPilots[pc]["keywords"] = oss.str();
	}
      }
      {
	int ac=0;
	for(PriAttack pi : pilot.GetNatAttacks()) {
	  jPilots[pc]["stats"][ac]["type"] = "attack";
	  jPilots[pc]["stats"][ac]["arc"] = FiringArc::GetFiringArc(pi.arc).GetName();
	  jPilots[pc]["stats"][ac]["value"] = pi.value;
	  ac++;
	}
	jPilots[pc]["stats"][ac]["type"] = "agility";
	jPilots[pc]["stats"][ac]["value"] = pilot.GetNatAgility();
	ac++;
	jPilots[pc]["stats"][ac]["type"] = "hull";
	jPilots[pc]["stats"][ac]["value"] = pilot.GetNatHull();
	ac++;
	if(pilot.GetNatShield().GetCapacity() > 0) {
	  jPilots[pc]["stats"][ac]["type"] = "shields";
	  jPilots[pc]["stats"][ac]["value"] = pilot.GetNatShield().GetCapacity();
	  ac++;
	}
	if(pilot.GetNatCharge().GetCapacity() > 0) {
	  jPilots[pc]["stats"][ac]["charge"]["capacity"] = pilot.GetNatCharge().GetCapacity();
	  jPilots[pc]["stats"][ac]["charge"]["rechargeable"] = pilot.GetNatCharge().GetRecurring();
	  ac++;
	}
	if(pilot.GetNatForce().GetCapacity() > 0) {
	  jPilots[pc]["stats"][ac]["force"]["capacity"] = pilot.GetNatForce().GetCapacity();
	  jPilots[pc]["stats"][ac]["force"]["rechargeable"] = pilot.GetNatForce().GetRecurring();
	  ac++;
	}
      }
      if(pilot.GetShipAbility()) { jPilots[pc]["shipAbility"] = pilot.GetShipAbility()->GetName() + ": " + pilot.GetShipAbility()->GetText().GetCleanText(); }
      jPilots[pc][pilot.HasAbility() ? "ability" : "text"] = pilot.GetText().GetCleanText();
      {
	int ac=0;
	for(std::list<SAct> sa : pilot.GetNatActions()) {
	  jPilots[pc]["actions"][ac]["difficulty"] = Difficulty::GetDifficulty(sa.front().difficulty).GetName();
	  jPilots[pc]["actions"][ac]["type"] = Action::GetAction(sa.front().action).GetName();
	  if(sa.size() == 2) {
	    jPilots[pc]["actions"][ac]["linked"]["difficulty"] = Difficulty::GetDifficulty(sa.back().difficulty).GetName();
	    jPilots[pc]["actions"][ac]["linked"]["type"] = Action::GetAction(sa.back().action).GetName();
	  }
	  ac++;
	}
      }
      {
	int uc=0;
	std::optional<UpgradeBar> ub = pilot.GetNatUpgradeBar();
	if(ub) {
	  for(const UpgradeSlot& s : *ub) {
	    jPilots[pc]["upgrades"][uc] = UpgradeType::GetUpgradeType(s.GetTypes()[0]).GetName();
	    uc++;
	  }
	}
      }
      {
	int ac = 0;
	for(Rel rel : Release::GetByPilot(pilot.GetPlt())) {
	  Release release = Release::GetRelease(rel);
	  jPilots[pc]["availability"][ac]["name"] = release.GetName();
	  jPilots[pc]["availability"][ac]["sku"] = release.GetSku();
	  ac++;
	}
      }
      if(pilot.IsUnreleased()) {
	jPilots[pc]["unreleased"] = true;
      }
      pc++;
    }

    std::string jsonStr;
    std::stringstream ss;
    ss << jPilots;
    jsonStr = ss.str();
    resp_open(kr, KHTTP_200, "application/json");
    khttp_puts(kr, jsonStr.c_str());
  }

  else if(format == "text") {
    struct ds {
      std::string fac;
      std::string shp;
      std::string plt;
    };
    std::vector<ds> data;
    {
      // sort data
      std::map<Fac, std::map<Shp,std::vector<Pilot>>> tmp = GetPilotsByFacShpPlt(restriction);
      for(const std::pair<const Fac, std::map<Shp,std::vector<Pilot>>> &d1 : tmp) {
	for(const std::pair<const Shp,std::vector<Pilot>> &d2 : d1.second) {
	  for(const Pilot &pilot : d2.second) {
	    data.push_back({pilot.GetFaction().Get3Letter(), pilot.GetShip().GetShortName(), pilot.GetName()});
	  }
	}
      }
    }
    int facLen=0;
    int shpLen=0;
    int pltLen=0;
    for(const ds &d : data) {
      facLen = std::max(facLen, (int)d.fac.size());
      shpLen = std::max(shpLen, (int)d.shp.size());
      pltLen = std::max(pltLen, (int)d.plt.size());
    }
    std::stringstream ss;
    for(const ds &d : data) {
      ss << std::setw(facLen) << std::left << d.fac;
      ss << "  ";
      ss << std::setw(shpLen) << std::left << d.shp;
      ss << "  ";
      ss << std::setw(pltLen) << std::left << d.plt;
      ss << std::endl;
    }
    resp_open(kr, KHTTP_200, "text/plain");
    khttp_puts(kr, ss.str().c_str());
  }

  else if(format == "csv") {
    std::stringstream ss;
    ss << "Faction," << "Ship," << "Limit," << "Cost," << "Hyperspace," << "Init," << "Name," << "Subtitle,"
       << "Keywords," << "Attack," << "Agility," << "Hull," << "Shield," << "Charge," << "Force," << "Energy,"
       << "Actions," << "Upgrades," << "Ability," << "Text"
       << std::endl;

    for(Plt plt : Pilot::GetAllPlts()) {
      Pilot pilot = Pilot::GetPilot(plt);
      if(restriction == "hyperspace") {
	if((pilot.GetHyperspace() == std::nullopt) || (*pilot.GetHyperspace() == false)) {
	  continue;
	}
      }
      ss << CSV(pilot.GetFaction().GetName());
      ss << ",";
      ss << CSV(pilot.GetShip().GetName());
      ss << ",";
      ss << CSV(pilot.GetLimited() ? std::to_string(pilot.GetLimited()) : "");
      ss << ",";
      ss << CSV(GetCost(pilot.GetNatCost()));
      ss << ",";
      ss << CSV(GetHyperspace(pilot.GetHyperspace()));
      ss << ",";
      ss << CSV(std::to_string(pilot.GetNatInitiative()));
      ss << ",";
      ss << CSV(pilot.GetName());
      ss << ",";
      ss << CSV(pilot.GetSubtitle());
      ss << ",";
      ss << CSV(GetKeywords(pilot.GetKeywordList()));
      ss << ",";
      ss << CSV(GetAttack(pilot.GetNatAttacks()));
      ss << ",";
      ss << CSV(std::to_string(pilot.GetNatAgility()));
      ss << ",";
      ss << CSV(std::to_string(pilot.GetNatHull()));
      ss << ",";
      ss << CSV(GetChargeable(pilot.GetNatShield()));
      ss << ",";
      ss << CSV(GetChargeable(pilot.GetNatCharge()));
      ss << ",";
      ss << CSV(GetChargeable(pilot.GetNatForce()));
      ss << ",";
      ss << CSV(GetChargeable(pilot.GetNatEnergy()));
      ss << ",";
      ss << CSV(GetActions(pilot.GetNatActions()));
      ss << ",";
      ss << CSV(GetUpgrades(pilot.GetNatUpgradeBar()));
      ss << ",";
      ss << CSV(pilot.HasAbility() ? "Ability" : "Flavor");
      ss << ",";
      ss << CSV(pilot.GetText().GetCleanText());
      ss << std::endl;
    }
    resp_open(kr, KHTTP_200, "text/csv", "pilots2.csv");
    khttp_puts(kr, ss.str().c_str());
  }
}
