#include "global.h"



// private helpers

// https://stackoverflow.com/a/1765217
tm TimeWhenCompiled() {
  std::string datestr = __DATE__;
  std::string timestr = __TIME__;

  std::istringstream iss_date( datestr );
  std::string str_month;
  int day;
  int year;
  iss_date >> str_month >> day >> year;

  int month;
  if     ( str_month == "Jan" ) month = 1;
  else if( str_month == "Feb" ) month = 2;
  else if( str_month == "Mar" ) month = 3;
  else if( str_month == "Apr" ) month = 4;
  else if( str_month == "May" ) month = 5;
  else if( str_month == "Jun" ) month = 6;
  else if( str_month == "Jul" ) month = 7;
  else if( str_month == "Aug" ) month = 8;
  else if( str_month == "Sep" ) month = 9;
  else if( str_month == "Oct" ) month = 10;
  else if( str_month == "Nov" ) month = 11;
  else if( str_month == "Dec" ) month = 12;
  else exit(-1);

  for( std::string::size_type pos = timestr.find( ':' ); pos != std::string::npos; pos = timestr.find( ':', pos ) )
    timestr[ pos ] = ' ';
  std::istringstream iss_time( timestr );
  int hour, min, sec;
  iss_time >> hour >> min >> sec;

  tm t = {0};
  t.tm_mon = month-1;
  t.tm_mday = day;
  t.tm_year = year - 1900;
  t.tm_hour = hour - 1;
  t.tm_min = min;
  t.tm_sec = sec;
  return t;
}



// publics

void resp_open(struct kreq *kr, enum khttp http, std::string mime) {
  khttp_head(kr, kresps[KRESP_STATUS], "%s", khttps[http]);
  khttp_head(kr, kresps[KRESP_CONTENT_TYPE], "%s", mime=="" ? kmimetypes[kr->mime] : mime.c_str());
  khttp_body(kr);
}

void resp_open(struct kreq *kr, enum khttp http, std::string mime, std::string filename) {
  khttp_head(kr, kresps[KRESP_STATUS], "%s", khttps[http]);
  khttp_head(kr, kresps[KRESP_CONTENT_TYPE], "%s", mime=="" ? kmimetypes[kr->mime] : mime.c_str());
  khttp_head(kr, kresps[KRESP_CONTENT_DISPOSITION], "attachment; filename=\"%s\"", filename.c_str());
  khttp_body(kr);
}



std::string GetBuildDateTimeString() {
  tm build = TimeWhenCompiled();
  char buildStr[20];
  snprintf(buildStr, sizeof(buildStr),
	   "%04d.%02d.%02d %02d:%02d:%02d",
	   build.tm_year+1900,
	   build.tm_mon+1,
	   build.tm_mday,
	   build.tm_hour+1,
	   build.tm_min,
	   build.tm_sec);
  return buildStr;
}



void PrintBreadcrumbs(struct kreq* kr, struct khtmlreq &hr,
		      std::vector<std::pair<std::string,std::string>> trail, std::string current,
		      std::function<void()> extra) {
  // https://stackoverflow.com/questions/2603700/how-to-align-3-divs-left-center-right-inside-another-div
  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "topBar", KATTR__MAX);
  { // breadcrumbs
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "breadcrumbs", KATTR__MAX);
    std::string hostname = std::string(kschemes[kr->scheme]) + "://" + kr->host;
    khtml_attr(&hr, KELEM_A, KATTR_HREF, hostname.c_str(), KATTR__MAX); khtml_puts(&hr, kr->host); khtml_closeelem(&hr, 1);
    for(std::pair<std::string,std::string> t : trail) {
      khtml_puts(&hr, "/");
      khtml_attr(&hr, KELEM_A, KATTR_HREF, t.first.c_str(), KATTR__MAX); khtml_puts(&hr, t.second.c_str()); khtml_closeelem(&hr, 1);
    }
    khtml_puts(&hr, "/");
    khtml_puts(&hr, current.c_str());
    khtml_closeelem(&hr, 1); // div (breadcrumbs)
  }
  { // extra stuff (like points version)
    if(extra) {
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "curptl", KATTR__MAX); extra(); khtml_closeelem(&hr, 1);
    } else {
      const char* nbsp = "&nbsp";
      khttp_write(kr, nbsp, 5);
    }
  }
  { // last build time
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "lastbuildbc", KATTR__MAX); khtml_puts(&hr, (std::string("built: ") + GetBuildDateTimeString()).c_str()); khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 1); // div (topbar)
  khtml_elem(&hr, KELEM_HR);
}



std::string StripLeadingSlash(std::string text) {
  std::string ret = text;
  if(ret[0] == '/') {
    ret.erase(0, 1);
  }
  return ret;
}



void PrintHead(struct kreq* kr, khtmlreq &hr, std::string name, std::string css) {
  const std::string description = "Unofficial data hub for X-wing (First and Second Editions) by FFG";
  khtml_elem(&hr, KELEM_HEAD);
  // search engine stuff
  std::string osd = std::string(kr->pname) + "/search2?osd";
  khtml_attr(&hr, KELEM_LINK, KATTR_TYPE, "application/opensearchdescription+xml", KATTR_REL, "search", KATTR_HREF, osd.c_str(), KATTR_TITLE, "Search xwing.cgi", KATTR__MAX);
  // meta stuff so posting a link in slack/discord shows the little description box thing
  khtml_attr(&hr, KELEM_META, KATTR_CHARSET, "utf-8", KATTR__MAX);
  khtml_attr(&hr, KELEM_META, KATTR_NAME, "viewport", KATTR_CONTENT, "width=device-width, initial-scale=1.0", KATTR__MAX);
  khtml_attr(&hr, KELEM_META, KATTR_NAME, "description", KATTR_CONTENT, description.c_str(), KATTR__MAX);
  khtml_attr(&hr, KELEM_META, KATTR_NAME, "og:title", KATTR_CONTENT, "xwing.cgi", KATTR__MAX);
  khtml_attr(&hr, KELEM_META, KATTR_NAME, "og:description", KATTR_CONTENT, description.c_str(), KATTR__MAX);

  khtml_elem(&hr, KELEM_TITLE); khtml_puts(&hr, name.c_str()); khtml_closeelem(&hr, 1);
  if(css == "") {
    khtml_attr(&hr, KELEM_LINK, KATTR_REL, "stylesheet", KATTR_TYPE, "text/css", KATTR_HREF, "/xwingcgi.css", KATTR__MAX);
  }
  khtml_elem(&hr, KELEM_STYLE);
  khtml_puts(&hr, css.c_str());
  khtml_closeelem(&hr, 1);

  khtml_attr(&hr, KELEM_LINK, KATTR_REL, "icon", KATTR_TYPE, "image/png", KATTR_HREF, "/xhud_favico.png", KATTR__MAX);
  khtml_closeelem(&hr, 1); // head
}



void PrintError(khtmlreq &hr, std::string msg) {
  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "sp_Error", KATTR__MAX);
  khtml_puts(&hr, msg.c_str());
  khtml_closeelem(&hr, 1);
}



void DumpInfo(struct kreq *kr, khtmlreq &hr) {
  khtml_attr(&hr, KELEM_TABLE, KATTR_BORDER, "1",  KATTR__MAX);
  khtml_elem(&hr, KELEM_TR); khtml_attr(&hr, KELEM_TH, KATTR_COLSPAN, "2", KATTR__MAX); khtml_puts(&hr, "DumpInfo()"); khtml_closeelem(&hr, 2);

  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "fullpath"); khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->fullpath); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "host");     khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->host);     khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "pagename"); khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->pagename); khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "path");     khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->path);     khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "pname");    khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->pname);    khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "remote");   khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->remote);   khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "suffix");   khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kr->suffix);   khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "scheme");   khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, kschemes[kr->scheme]);   khtml_closeelem(&hr, 2);
  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "fieldsz");  khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD); khtml_puts(&hr, std::to_string(kr->fieldsz).c_str());   khtml_closeelem(&hr, 2);

  khtml_elem(&hr, KELEM_TR); khtml_elem(&hr, KELEM_TH); khtml_puts(&hr, "fields");   khtml_closeelem(&hr, 1); khtml_elem(&hr, KELEM_TD);
  //khtml_puts(&hr, kr->suffix);
  for(int i=0; i<kr->fieldsz; i++) {
    if(i != 0) { khtml_elem(&hr, KELEM_BR); }
    std::string s = std::string(kr->fields[i].key) + " : " + kr->fields[i].val;
    khtml_puts(&hr, s.c_str());
  }
  khtml_closeelem(&hr, 2);
  
  khtml_closeelem(&hr, 1); // table
}



/*
struct	kreq {
	struct khead		 *reqmap[KREQU__MAX];
	struct khead		 *reqs;
	size_t		 	  reqsz;
	enum kmethod		  method;
	enum kauth		  auth;
	struct khttpauth	  rawauth;
	struct kpair		 *cookies;
	size_t			  cookiesz;
	struct kpair		**cookiemap;
	struct kpair		**cookienmap;
	struct kpair		 *fields;
	struct kpair		**fieldmap;
	struct kpair		**fieldnmap;
	size_t			  fieldsz;
	size_t			  mime;
	size_t			  page;
	enum kscheme		  scheme;
	char			 *path;
	char			 *suffix;
	char			 *fullpath;
	char			 *pagename;
	char			 *remote;
	char			 *host;
	uint16_t		  port;
	struct kdata		 *kdata;
	const struct kvalid	 *keys;
	size_t			  keysz;
	char			 *pname;
	void			 *arg; 
};
*/
