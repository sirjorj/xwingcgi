#include "xwingcgi2.h"

namespace libxwing2 {

// publics

static std::string GetMnvDifStr(Dif d) {
  std::string dif;
  if     (d == Dif::Blue)   { return "mnvDif_B"; }
  else if(d == Dif::White)  { return "mnvDif_W"; }
  else if(d == Dif::Red)    { return "mnvDif_R"; }
  else if(d == Dif::Purple) { return "mnvDif_P"; }
  else                      { return "";         }
}

std::function<void()> GetPointsListForBreadcrumbs(khtmlreq &hr, PtL ptl, const PageToken& pt) {
  return [&hr,ptl,&pt] {
	   khtml_attr(&hr, KELEM_FORM, KATTR_ACTION, LinkBuilder(pt, pt.GetPath()).AllArgs().Get().c_str(), KATTR_METHOD, "get", KATTR__MAX);
	   khtml_attr(&hr, KELEM_LABEL, KATTR_FOR, "points", KATTR__MAX); khtml_puts(&hr, "Points:"); khtml_closeelem(&hr, 1);
	   khtml_attr(&hr, KELEM_SELECT, KATTR_NAME, PageToken::PointList.c_str(), KATTR_ID, "points", KATTR__MAX);
	   for(PtL p : PointList::GetPtLs()) {
	     if(p == ptl) {
	       khtml_attr(&hr, KELEM_OPTION, KATTR_VALUE, PointList::GetPtLStr(p).c_str(), KATTR_SELECTED, "selected", KATTR__MAX);
	     } else {
	       khtml_attr(&hr, KELEM_OPTION, KATTR_VALUE, PointList::GetPtLStr(p).c_str(), KATTR__MAX);
	     }
	     khtml_puts(&hr, PointList::GetName(p).c_str());
	     khtml_closeelem(&hr, 1); // option
	   }
	   khtml_closeelem(&hr, 1); // select

	   for(const std::pair<const std::string,std::string>& a : pt.GetArgs()) {
	     if(a.first != PageToken::PointList) {
	       khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "hidden", KATTR_NAME, a.first.c_str(), KATTR_VALUE, a.second.c_str(), KATTR__MAX);
	     }
	   }
	   khtml_attr(&hr, KELEM_INPUT, KATTR_TYPE, "submit", KATTR_VALUE, "set", KATTR__MAX);
	   khtml_closeelem(&hr, 1); // form
	 };
}

void PrintManeuvers(struct khtmlreq &hr, Maneuvers maneuvers) {
  Maneuver m;
  int8_t min = 10, max = -10;
  for(auto a : maneuvers) { if(a.speed > max) {max = a.speed;} if(a.speed < min) {min=a.speed;} }
  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "mnvDiv", KATTR__MAX);
  khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "mnvTab", KATTR__MAX);
  // forward/stationary
  for(int s=max; s>=min; s--) {
    khtml_elem(&hr, KELEM_TR);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "mnvSpd", KATTR__MAX);
    khtml_int(&hr, s);
    khtml_closeelem(&hr, 1);
    // regular
    for(Brn b : {Brn::LTurn, Brn::LBank, (s==0) ? Brn::Stationary : Brn::Straight, Brn::RBank, Brn::RTurn}) {
      if(FindManeuver(maneuvers, s, b, m)) {
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(m.difficulty).c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(m.bearing).c_str());
	khtml_closeelem(&hr, 1);
      } else {
      	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(Dif::White).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1);
      }
    }
    // special
    for(Brn b : {Brn::KTurn, Brn::LSloop, Brn::RSloop, Brn::LTroll, Brn::RTroll}) {
      if(FindManeuver(maneuvers, s, b, m)) {
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(m.difficulty).c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(m.bearing).c_str());
	khtml_closeelem(&hr, 1);
      }
    }
    khtml_closeelem(&hr, 1); // tr
  }
  // reverse
  for(int s=min; s<=max; s++) {
    bool doIt = false;
    for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
      if(FindManeuver(maneuvers, s, b, m)) {
        doIt = true;
        break;
      }
    }
    if(doIt) {
      khtml_elem(&hr, KELEM_TR);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "mnvSpd", KATTR__MAX);
      khtml_int(&hr, s*-1);
      khtml_closeelem(&hr, 1);
      // pad the LTurn spot
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(Dif::White).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1);
      for(Brn b : {Brn::RevLBank, Brn::RevStraight, Brn::RevRBank}) {
	if(FindManeuver(maneuvers, s, b, m)) {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(m.difficulty).c_str(), KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(m.bearing).c_str());
	  khtml_closeelem(&hr, 1); // td
	} else {
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(m.difficulty).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1);
	}
      }
      // pad the RTurn spot
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, GetMnvDifStr(Dif::White).c_str(), KATTR__MAX); khtml_closeelem(&hr, 1);
      khtml_closeelem(&hr, 1); // tr
    }
  }
  khtml_closeelem(&hr, 1); // table
  khtml_closeelem(&hr, 1); // div
}



static void PrintName(khtmlreq &hr, std::string name, std::string url, uint8_t limited) {
  khtml_attr(&hr, KELEM_A, KATTR_HREF, url.c_str(), KATTR__MAX);
  if(limited) {
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR__MAX);
    for(int i=0; i<limited; i++) {
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::unique).c_str());
    }
    khtml_closeelem(&hr, 1); // span (limited glyph)
  }
  khtml_puts(&hr, name.c_str());
  khtml_closeelem(&hr, 1); // a
}

void PrintShipName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Ship &s) {
  PrintName(hr, s.GetName(), LinkBuilder(pt,"ship").AddArg(PageToken::PointList).AddArg(PageToken::Ship,s.GetShpStr()).Get(), 0);
}

void PrintPilotName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Pilot &p) {
  PrintName(hr, p.GetName(), LinkBuilder(pt, "pilot2").AddArg(PageToken::PointList).AddArg(PageToken::Pilot,p.GetPltStr()).Get(), p.GetLimited());
}

void PrintUpgradeName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Upgrade &u) {
  PrintName(hr, u.GetName(), LinkBuilder(pt, "upgrade2").AddArg(PageToken::PointList).AddArg(PageToken::Upgrade,u.GetUpgStr()).Get(), u.GetLimited());
}

void PrintUpgradeTitle(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Upgrade &u) {
  PrintName(hr, u.GetTitle(), LinkBuilder(pt, "upgrade2").AddArg(PageToken::PointList).AddArg(PageToken::Upgrade,u.GetUpgStr()).Get(), u.GetLimited());
}

void PrintConditionName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Condition &c) {
  PrintName(hr, c.GetName(), LinkBuilder(pt, "condition2").AddArg(PageToken::PointList).AddArg(PageToken::Name,c.GetCndStr()).Get(), c.GetLimited());
}

void PrintEnvironmentName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Environment &e) {
  PrintName(hr, e.GetName(), LinkBuilder(pt, "environment2").AddArg(PageToken::PointList).AddArg(PageToken::Name,e.GetEnvStr()).Get(), 0);
}

void PrintReleaseName(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Release &rel) {
  PrintName(hr, rel.GetName(), LinkBuilder(pt, "release2").AddArg(PageToken::PointList).AddArg(PageToken::SKU,rel.GetSku()).Get(), 0);
}



void PrintActionBar(khtmlreq &hr, const ActionBar& ab) {
  for(auto a : ab) {
    bool next = false;
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "actionGroup", KATTR__MAX);
    for(SAct sa : a) {
      if(next) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "linked", KATTR_TITLE, "Linked to", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::linked).c_str());
	khtml_closeelem(&hr, 1);
      }
      std::string dif;
      switch(sa.difficulty) {
      case Dif::White:  dif = "actionW"; break;
      case Dif::Red:    dif = "actionR"; break;
      case Dif::Purple: dif = "actionP"; break;
      default:          dif = "";        break;
      }
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, dif.c_str(), KATTR_TITLE, Action::GetAction(sa.action).GetName().c_str(), KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Action::GetAction(sa.action).GetAct()).c_str());
      khtml_closeelem(&hr, 1);
      next = true;
    }
    khtml_closeelem(&hr, 1); // span (actionGroup)
  }
}

void PrintUpgradeBar(khtmlreq &hr, const UpgradeBar& ub) {
  for(const UpgradeSlot& s : ub) {
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgrade", KATTR_TITLE, UpgradeType::GetUpgradeType(s.GetTypes()[0]).GetName().c_str(), KATTR__MAX);
    khtml_puts(&hr, IconGlyph::GetGlyph(s.GetTypes()[0]).c_str());
    khtml_closeelem(&hr, 1); // span
  }
}



void PrintShipRow(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Ship& s) {
  khtml_elem(&hr, KELEM_TR);
  { // factions
    std::vector<Fac> facs;
    for(Plt plt : Pilot::GetAllPlts()) {
      Pilot pilot = Pilot::GetPilot(plt);
      if(pilot.GetShp() == s.GetShp()) {
	if(std::find(facs.begin(), facs.end(), pilot.GetFac()) == facs.end()) {
	  facs.push_back(pilot.GetFac());
	}
      }
    }
    FactionsTD(hr, facs);
  }
  { // ship icon
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipIcon", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, s.GetName().c_str(), KATTR__MAX);
    khtml_puts(&hr, ShipGlyph::GetGlyph(s.GetShp()).c_str());
    khtml_closeelem(&hr, 2);
  }
  { // dial id
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "dialId", KATTR__MAX);
    khtml_puts(&hr, s.GetDialId().c_str());
    khtml_closeelem(&hr, 1); // td
  }
  { // ship name
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipName", KATTR__MAX);
    khtml_attr(&hr, KELEM_A, KATTR_HREF, LinkBuilder(pt, "ship2").AddArg(PageToken::PointList).AddArg(PageToken::Ship,s.GetShpStr()).Get().c_str(), KATTR__MAX);
    khtml_puts(&hr, s.GetName().c_str());
    khtml_closeelem(&hr, 1); // a
    khtml_closeelem(&hr, 1); // td
  }
  khtml_closeelem(&hr, 1); // tr
}

void PrintPilotRow(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Pilot& p) {
  PtL ptl = pt.GetPtL();
  khtml_attr(&hr, KELEM_TR, KATTR_CLASS, p.IsUnreleased() ? "unr" : "", KATTR__MAX);
  { // ship icon
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "shipIcon", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, p.GetShip().GetName().c_str(), KATTR__MAX);
    khtml_puts(&hr, ShipGlyph::GetGlyph(p.GetShip().GetShp()).c_str());
    khtml_closeelem(&hr, 2);
  }
  { // faction
    FactionsTD(hr, {p.GetFac()});
  }
  { // initiative
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "init", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "initiative", KATTR__MAX);
    khtml_int(&hr, p.GetNatInitiative());
    khtml_closeelem(&hr, 2); // span, td
  }
  { // name
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pilotName", KATTR__MAX);
    PrintPilotName(kr, hr, pt, p);
    khtml_closeelem(&hr, 1); // td
  }
  { // cost
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "cost", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Cost", KATTR__MAX);
    khtml_puts(&hr, p.GetNatCost(ptl) ? std::to_string(*p.GetNatCost(ptl)).c_str() : "???");
    khtml_closeelem(&hr, 2); // span, td
  }
  { // stats
    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "statTable", KATTR__MAX);
    khtml_attr(&hr, KELEM_TR, KATTR_CLASS, p.IsUnreleased() ? "unr" : "", KATTR__MAX);
    // attack
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
    bool br = false;
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "attackStat", KATTR__MAX);
    for(PriAttack a : p.GetNatAttacks()) {
      if(br) { khtml_elem(&hr, KELEM_BR); }
      // arc icon
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "attackArc", KATTR_TITLE, FiringArc::GetFiringArc(a.arc).GetName().c_str(), KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(a.arc).c_str());
      khtml_closeelem(&hr, 1);
      // attack value
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "attackVal", KATTR_TITLE, "Attack", KATTR__MAX);
      khtml_puts(&hr, std::string(std::to_string(a.value)).c_str());
      khtml_closeelem(&hr, 1);
      br = true;
    }
    khtml_closeelem(&hr, 2); // span, td
    // agility
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "agilityStat", KATTR_TITLE, "Agility", KATTR__MAX);
    khtml_int(&hr, p.GetNatAgility());
    khtml_closeelem(&hr, 2); // span, td
    // hull
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "hullStat", KATTR_TITLE, "Hull", KATTR__MAX);
    khtml_int(&hr, p.GetNatHull());
    khtml_closeelem(&hr, 2); // span, td
    // shield
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "shieldStat", KATTR_TITLE, "Shield", KATTR__MAX);
    khtml_int(&hr, p.GetNatShield().GetCapacity());
    khtml_closeelem(&hr, 1); // span
    if(p.GetNatShield().GetRecurring() == 1) {
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "shieldIcon", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
      khtml_closeelem(&hr, 1); // span
    }
    else if(p.GetNatShield().GetRecurring() == 2) {
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "shieldIcon", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::doublerecurring).c_str());
      khtml_closeelem(&hr, 1); // span
    }
    khtml_closeelem(&hr, 1); // td
    // force
    if(p.GetNatForce().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "forceStat", KATTR_TITLE, "Force", KATTR__MAX);
      khtml_puts(&hr, std::to_string(p.GetNatForce().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(p.GetNatForce().GetRecurring()) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "forceIcon", KATTR_TITLE, "Recurring", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 1); // td
    }
    // charge
    if(p.GetNatCharge().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "chargeStat", KATTR_TITLE, "Charge", KATTR__MAX);
      khtml_puts(&hr, std::to_string(p.GetNatCharge().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(p.GetNatCharge().GetRecurring()) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "chargeIcon", KATTR_TITLE, "Recurring", KATTR__MAX);
	Ico gly;
	switch(p.GetNatCharge().GetRecurring()) {
	case  1: gly = Ico::recurring; break;
	case  2: gly = Ico::doublerecurring; break;
	case  3: gly = Ico::triplerecurring; break;
	case -1: gly = Ico::negativerecurring; break;
	default: gly = Ico::recurring; break;
	}
	khtml_puts(&hr, IconGlyph::GetGlyph(gly).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 1); // td
    }
    // energy
    if(p.GetNatEnergy().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "statTd", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "energyStat", KATTR_TITLE, "Energy", KATTR__MAX);
      khtml_int(&hr, p.GetNatEnergy().GetCapacity());
      khtml_closeelem(&hr, 1); // span
      if(p.GetNatEnergy().GetRecurring() == 1) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "energyIcon", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      else if(p.GetNatEnergy().GetRecurring() == 2) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "energyIcon", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::doublerecurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 1); // td
    }
    khtml_closeelem(&hr, 2); // tr, table
    khtml_closeelem(&hr, 1); // td
  }
  { // actions
    khtml_attr(&hr, KELEM_TD, KATTR__MAX);
    PrintActionBar(hr, p.GetNatActions());
    khtml_closeelem(&hr, 1);
  }
  { //upgrades
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "upgradeBar", KATTR__MAX);
    std::optional<UpgradeBar> ub = p.GetNatUpgradeBar(ptl);
    if(ub) {
      PrintUpgradeBar(hr, *ub);
    }
    khtml_closeelem(&hr, 1); // td
  }
  { // ship ability
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "textAb", KATTR__MAX);
    auto sa = p.GetShipAbility();
    if(sa) {
      khtml_puts(&hr, sa->GetName().c_str());
    }
    khtml_closeelem(&hr, 1);
  }
  { // text
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, p.HasAbility() ? "textAb" : "textFl", KATTR__MAX);
    IconifyText(hr, p.GetText().GetCleanText());
    khtml_closeelem(&hr, 1); // td
  }
  khtml_closeelem(&hr, 1); // tr
}

void PrintUpgradeRow(struct kreq *kr, khtmlreq &hr, const PageToken& pt, const Upgrade& u) {
  Upgrade up = Upgrade::GetUpgrade(u.GetUpg());
  int sides = up.IsDualSided() ? 2 : 1;

  khtml_elem(&hr, KELEM_TR);
  { // upgrade slots
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "upgradeBar", KATTR_ROWSPAN, std::to_string(sides).c_str(), KATTR__MAX);
    for(UpT t : up.GetSlots()) {
      UpgradeType ut = UpgradeType::GetUpgradeType(t);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgrade", KATTR_TITLE, ut.GetName().c_str(), KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(t).c_str());
      khtml_closeelem(&hr, 1); // span
    }
    khtml_closeelem(&hr, 1); // td
  }
  { // faction
    if(up.GetRestriction().GetAllowedFactions().size()) {
      FactionsTD(hr, up.GetRestriction().GetAllowedFactions(), sides);
    } else {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "tdFaction", KATTR_ROWSPAN, std::to_string(sides).c_str(), KATTR__MAX);
      khtml_closeelem(&hr, 1);
    }
  }
  { // cost
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "center",  KATTR_ROWSPAN, std::to_string(sides).c_str(), KATTR__MAX);
    std::optional<Cost> uc = up.GetCost();
    if(!uc) {
      khtml_puts(&hr, "???");
    }
    else {
      switch(uc->GetCsT()) {
	// none
        case CsT::Const: {
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgCost", KATTR_TITLE, "Cost", KATTR__MAX);
	  khtml_int(&hr, uc->GetCosts()[0]);
	  khtml_closeelem(&hr, 1); // span
	  break;
	}
	// base size
        case CsT::Base: {
	  std::vector<std::string> bs = { "S:", "M:", "L:", "H:" };
	  int i=0;
	  for(uint16_t c : uc->GetCosts()) {
	    khtml_puts(&hr, bs[i].c_str());
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgCost", KATTR_TITLE, "Cost", KATTR__MAX);
	    khtml_int(&hr, c);
	    khtml_closeelem(&hr, 1); // span
	    khtml_puts(&hr, " ");
	    i++;
	  }
	  break;
	}
	// agi
        case CsT::Agi: {
	  std::vector<std::string> agi = { "0", "1", "2", "3" };
	  int i=0;
	  for(uint16_t c : uc->GetCosts()) {
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "agiStat", KATTR_TITLE, "Agility", KATTR__MAX);
	    khtml_puts(&hr, agi[i].c_str());
	    khtml_closeelem(&hr, 1); // span
	    khtml_puts(&hr, ":");
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgCost", KATTR_TITLE, "Cost", KATTR__MAX);
	    khtml_int(&hr, c);
	    khtml_closeelem(&hr, 1); // span
	    khtml_puts(&hr, " ");
	    i++;
	  }
	  break;
	}
	// init
        case CsT::Init: {
	  std::vector<std::string> init = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
	  int i=0;
	  for(uint16_t c : uc->GetCosts()) {
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "initStat", KATTR_TITLE, "Initiative", KATTR__MAX);
	    khtml_puts(&hr, init[i].c_str());
	    khtml_closeelem(&hr, 1); // span
	    khtml_puts(&hr, ":");
  	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "upgCost", KATTR_TITLE, "Cost", KATTR__MAX);
	    khtml_int(&hr, c);
	    khtml_closeelem(&hr, 1); // span
	    khtml_puts(&hr, " ");
	    i++;
	  }
	  break;
	}
      }
    }
    khtml_closeelem(&hr, 1); // td
  }
  // per-side
  for(int s=0; s < sides; s++) {
    // back sides need a new row
    if(s != 0) {
      khtml_elem(&hr, KELEM_TR);
    }

    { // title
      khtml_elem(&hr, KELEM_TD);
      PrintUpgradeTitle(kr, hr, pt, up);
      khtml_closeelem(&hr, 1);
    }
    { // text
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, up.HasAbility() ? "textAb" : "textFl", KATTR__MAX);
      IconifyText(hr, up.GetText().GetCleanText());
      khtml_closeelem(&hr, 1); // td
    }
    if(s != 0) {
      khtml_closeelem(&hr, 1); // tr
    }
    up.Flip();
  } // end per-side

  khtml_closeelem(&hr, 1); // tr
}



void FactionsTD(khtmlreq &hr, std::vector<Fac> facs, int sides) {
  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "tdFaction", KATTR_ROWSPAN, std::to_string(sides).c_str(), KATTR__MAX);
  for(Fac fac : facs) {
    Faction faction = Faction::GetFaction(fac);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, ("icon_"+faction.Get3Letter()).c_str(), KATTR_TITLE, Faction::GetFaction(fac).GetName().c_str(), KATTR__MAX);
    khtml_puts(&hr, IconGlyph::GetGlyph(fac).c_str());
    khtml_closeelem(&hr, 1);
  }
  khtml_closeelem(&hr, 1);  
}

// i hate this routine so much...
enum class MarkupType {
  None = 0,
  Icon = 1,
  Mkup = 2,
};
void IconifyText(struct khtmlreq &hr, std::string s) {
  std::string normalText;
  std::string markupText;
  //bool isInMarkup = false;
  MarkupType markupType = MarkupType::None;
  size_t markupStart=0;
  for(int i=0; i<s.size(); i++) {
    char c = s[i];
    switch(c) {
    case '{':
      if(markupType == MarkupType::None) {
	//khtml_puts(&hr, (" {" + std::to_string(i) + "{ ").c_str());
	markupType = MarkupType::Icon;
	markupText += c;
      }
      break;

    case '}': {
      if(markupType == MarkupType::Icon) {
	//khtml_puts(&hr, (" }" + std::to_string(i) + "} ").c_str());
	markupText += c;
	std::string glyph = IconGlyph::GetGlyph(markupText);
	if(glyph != "" && glyph != markupText) {
	  khtml_puts(&hr, normalText.c_str());
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR__MAX);
	  khtml_puts(&hr, glyph.c_str());
	  khtml_closeelem(&hr, 1);
	  normalText = "";
	  markupText = "";
	}
	else {
	  normalText += markupText;
	  markupText = "";
	}
	markupType = MarkupType::None;
      }
      break;
    }

    case '[':
      if(markupType == MarkupType::None) {
	//khtml_puts(&hr, (" [" + std::to_string(i) + "[ ").c_str());
	markupType = MarkupType::Mkup;
	markupText += c;
	markupStart = i;
      }
      break;

    case ']': {
      if(markupType == MarkupType::Mkup) {
	//khtml_puts(&hr, (" ]" + std::to_string(i) + "] ").c_str());
      	//khtml_puts(&hr, " ]INMARKUP] ");
	markupText += c;
	if(markupText == "[B]") {
	  if(normalText.size()) {
	    khtml_puts(&hr, normalText.c_str());
	  }
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "bold", KATTR__MAX);
	  normalText = "";
	  markupText = "";
	}
	else if(markupText == "[/B]") {
	  if(normalText.size()) {
	    khtml_puts(&hr, normalText.c_str());
	  }
	  khtml_closeelem(&hr, 1);      	
	  normalText = "";
	  markupText = "";
	}
	else if(markupText == "[I]") {
	  if(normalText.size()) {
	    khtml_puts(&hr, normalText.c_str());
	  }
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "italic", KATTR__MAX);
	  normalText = "";
	  markupText = "";
	}
	else if(markupText == "[/I]") {
	  if(normalText.size()) {
	    khtml_puts(&hr, normalText.c_str());
	  }
	  khtml_closeelem(&hr, 1);      	
	  normalText = "";
	  markupText = "";
	}
	else {
	  i = markupStart;
	  normalText += '[';
	  //normalText="";
	  markupText="";
	}
	markupType = MarkupType::None;
      } else {	
	//khtml_puts(&hr, " ]NOTINMARKUP] ");
	normalText += c;
      }
      break;
    }

    default:
      if(markupType == MarkupType::None) {
	normalText += c;
      } else {
	markupText += c;
      }
    }
  }
  if(normalText.size()) {
    khtml_puts(&hr, normalText.c_str());
  }
}


 
void PrintPilotCard(struct khtmlreq &hr, const PageToken& pt, const Pilot& pilot, float scale) {
  PtL ptl = pt.GetPtL();
  std::string style = "transform: scale(" + std::to_string(scale) + ");";
  std::string pcClass;
  switch(pilot.GetFac()) {
  case Fac::Rebel:      pcClass = "pc_CardReb"; break;
  case Fac::Imperial:   pcClass = "pc_CardImp"; break;
  case Fac::Scum:       pcClass = "pc_CardScu"; break;
  case Fac::Resistance: pcClass = "pc_CardRes"; break;
  case Fac::FirstOrder: pcClass = "pc_CardFir"; break;
  case Fac::Republic:   pcClass = "pc_CardRep"; break;
  case Fac::Separatist: pcClass = "pc_CardSep"; break;
  default: pcClass = "";
  }

  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, pcClass.c_str(), KATTR_STYLE, style.c_str(), KATTR__MAX);
  { // Artwork (maneuver dial)
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Artwork", KATTR__MAX); 
    PrintManeuvers(hr, pilot.GetShip().GetManeuvers());
    khtml_closeelem(&hr, 1);
  }
  { // Initiative
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Init", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Initiative", KATTR__MAX);
    khtml_puts(&hr, std::to_string(pilot.GetModInitiative()).c_str());
    khtml_closeelem(&hr, 2);
  }
  { // Name
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Name", KATTR__MAX);
    if(pilot.GetLimited()) {
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR__MAX);
      for(int i=0; i<pilot.GetLimited(); i++) {
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::unique).c_str());
      }
      khtml_closeelem(&hr, 1);
    }
    khtml_puts(&hr, pilot.GetName().c_str());
    khtml_closeelem(&hr, 1);
  }
  { // Subtitle
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Subtitle", KATTR__MAX);
    khtml_puts(&hr, pilot.GetSubtitle().c_str());
    khtml_closeelem(&hr, 1);
  }
  { // Faction
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Faction", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, pilot.GetFaction().GetName().c_str(), KATTR__MAX);
    khtml_puts(&hr, IconGlyph::GetGlyph(pilot.GetFac()).c_str());
    khtml_closeelem(&hr, 2);
  }
  { // Text
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_TextCt", KATTR__MAX);
    // print the text
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, pilot.HasAbility() ? "pc_Text" : "pc_FText", KATTR__MAX);

    std::vector<std::string> segments = pilot.GetText().GetMarkedSegments();
    for(const std::string& s : segments) {
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, (pilot.HasAbility() ? "pc_Text" : "pc_FText"), KATTR__MAX);
      IconifyText(hr, s);
      khtml_closeelem(&hr, 1);
    }
    khtml_closeelem(&hr, 1); // div (text)
    // if there is a ship ability...
    std::optional<ShipAbility> sa = pilot.GetShipAbility();
    if(sa) {
      // add an HR
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Text", KATTR__MAX);
      khtml_attr(&hr, KELEM_HR, KATTR__MAX);
      khtml_closeelem(&hr, 1); // div (hr)
      // and print the ship ability
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Text", KATTR__MAX);
      // with a bold title
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "bold italic", KATTR__MAX);
      khtml_puts(&hr, (sa->GetName() + ":").c_str());
      khtml_closeelem(&hr, 1); // span
      // and regular text
      IconifyText(hr, sa->GetText().GetMarkedText());
      khtml_closeelem(&hr, 1); // div
    }
    khtml_closeelem(&hr, 1); // div (container)
  }
  { // Actions
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Actions", KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_ActionsTab", KATTR__MAX);
    for(const std::list<SAct>& sas : pilot.GetNatActions()) {
      std::string actClass;
      switch(pilot.GetFac()) {
      case Fac::Rebel:      actClass = "pc_ActionsReb"; break;
      case Fac::Imperial:   actClass = "pc_ActionsImp"; break;
      case Fac::Scum:       actClass = "pc_ActionsScu"; break;
      case Fac::Resistance: actClass = "pc_ActionsRes"; break;
      case Fac::FirstOrder: actClass = "pc_ActionsFir"; break;
      case Fac::Republic:   actClass = "pc_ActionsRep"; break;
      case Fac::Separatist: actClass = "pc_ActionsSep"; break;
      default: actClass = "";
      }
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, actClass.c_str(), KATTR__MAX);
      bool next = false;
      khtml_attr(&hr, KELEM_DIV, KATTR_STYLE, "pc_Action", KATTR__MAX);
      for(SAct sa : sas) {
	if(next) {
	  khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Linked to", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::linked).c_str());
	  khtml_closeelem(&hr, 1);
	}
	std::string dif;
	switch(sa.difficulty) {
	case Dif::White:  dif = "pc_actionW"; break;
	case Dif::Red:    dif = "pc_actionR"; break;
	case Dif::Purple: dif = "pc_actionP"; break;
	default:          dif = "";        break;
	}
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, dif.c_str(), KATTR_TITLE, Action::GetAction(sa.action).GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(sa.action).c_str());
	khtml_closeelem(&hr, 1);
	next = true;
      }
      khtml_closeelem(&hr, 3); // div td tr
    }
    khtml_closeelem(&hr, 2); // table div
  }
  { // Stats
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Stats", KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    // attack
    for(const PriAttack& pa : pilot.GetNatAttacks()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatAtkI", KATTR_TITLE, FiringArc::GetFiringArc(pa.arc).GetName().c_str(), KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(pa.arc).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatAtkV", KATTR__MAX);
      khtml_puts(&hr, std::to_string(pa.value).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_closeelem(&hr, 2); // table td
    }
    // agility
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatAgiI", KATTR_TITLE, "Agility", KATTR__MAX);
    khtml_puts(&hr, IconGlyph::GetGlyph(Ico::agility).c_str());
    khtml_closeelem(&hr, 1); // span
    khtml_closeelem(&hr, 2); // td tr
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatAgiV", KATTR__MAX);
    khtml_puts(&hr, std::to_string(pilot.GetNatAgility()).c_str());
    khtml_closeelem(&hr, 1); // span
    khtml_closeelem(&hr, 2); // td tr
    khtml_closeelem(&hr, 2); // table td
    // hull
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatHulI", KATTR_TITLE, "Hull", KATTR__MAX);
    khtml_puts(&hr, IconGlyph::GetGlyph(Ico::hull).c_str());
    khtml_closeelem(&hr, 1); // span
    khtml_closeelem(&hr, 2); // td tr
    khtml_attr(&hr, KELEM_TR, KATTR__MAX);
    khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatHulV", KATTR__MAX);
    khtml_puts(&hr, std::to_string(pilot.GetNatHull()).c_str());
    khtml_closeelem(&hr, 1); // span
    khtml_closeelem(&hr, 2); // td tr
    khtml_closeelem(&hr, 2); // table td
    // shield
    if(pilot.GetNatShield().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatShiI", KATTR_TITLE, "Shield", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::shield).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatShiV", KATTR__MAX);
      khtml_puts(&hr, std::to_string(pilot.GetNatShield().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(pilot.GetNatShield().GetRecurring() == 1) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatShiI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      else if(pilot.GetNatShield().GetRecurring() == 2) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatShiI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::doublerecurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 2); // td tr
      khtml_closeelem(&hr, 2); // table td
    }
    // force
    if(pilot.GetNatForce().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatForI", KATTR_TITLE, "Force", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::forcecharge).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatForV", KATTR__MAX);
      khtml_puts(&hr, std::to_string(pilot.GetNatForce().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(pilot.GetNatForce().GetRecurring() == 1) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatForI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 2); // td tr
      khtml_closeelem(&hr, 2); // table td
    }
    // charge
    if(pilot.GetNatCharge().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatChaI", KATTR_TITLE, "Charge", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::charge).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatChaV", KATTR__MAX);
      khtml_puts(&hr, std::to_string(pilot.GetNatCharge().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(pilot.GetNatCharge().GetRecurring()) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatChaI", KATTR__MAX);
	Ico gly;
	switch(pilot.GetNatCharge().GetRecurring()) {
	case  1: gly = Ico::recurring; break;
	case  2: gly = Ico::doublerecurring; break;
	case  3: gly = Ico::triplerecurring; break;
	case -1: gly = Ico::negativerecurring; break;
	default: gly = Ico::recurring; break;
	}
	khtml_puts(&hr, IconGlyph::GetGlyph(gly).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 2); // td tr
      khtml_closeelem(&hr, 2); // table td
    }
    // energy
    if(pilot.GetNatEnergy().GetCapacity()) {
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "pc_StatsTab", KATTR__MAX);
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatEneI", KATTR_TITLE, "Energy", KATTR__MAX);
      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::energy).c_str());
      khtml_closeelem(&hr, 1); // span
      khtml_closeelem(&hr, 2); // td tr
      khtml_attr(&hr, KELEM_TR, KATTR__MAX);
      khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "pc_StatsTD", KATTR__MAX);
      khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatEneV", KATTR__MAX);
      khtml_puts(&hr, std::to_string(pilot.GetNatEnergy().GetCapacity()).c_str());
      khtml_closeelem(&hr, 1); // span
      if(pilot.GetNatEnergy().GetRecurring() == 1) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatEneI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      else if(pilot.GetNatEnergy().GetRecurring() == 2) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "pc_StatEneI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::doublerecurring).c_str());
	khtml_closeelem(&hr, 1); // span
      }
      khtml_closeelem(&hr, 2); // td tr
      khtml_closeelem(&hr, 2); // table td
    }
    khtml_closeelem(&hr, 3); // tr table div
  }
  { // Ship (Icon)
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_ShipIcon", KATTR__MAX);
    khtml_puts(&hr, ShipGlyph::GetGlyph(pilot.GetShp()).c_str());
    khtml_closeelem(&hr, 1);
  }
  { // Ship (Name)
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_ShipName", KATTR__MAX);
    khtml_puts(&hr, pilot.GetShip().GetName().c_str());
    khtml_closeelem(&hr, 1);
  }
  { // Upgrades
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Upgrades", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR__MAX);
    //std::string upgradeStr;
    std::optional<UpgradeBar> ub = pilot.GetNatUpgradeBar(ptl);
    if(ub) {
      for(const UpgradeSlot& us : *ub) {
	if(!us.IsMulti()) {
	  UpT ut = us.GetTypes()[0];
	  khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, UpgradeType::GetUpgradeType(ut).GetName().c_str(), KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(ut).c_str());
	  khtml_closeelem(&hr, 1);
	}
      }
    }
    //khtml_puts(&hr, upgradeStr.c_str());
    khtml_closeelem(&hr, 2);
  }
  { // Cost
    std::string costStr = pilot.GetNatCost(ptl) ? std::to_string(*(pilot.GetNatCost(ptl))) : "???";
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "pc_Cost", KATTR__MAX);
    khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Cost", KATTR__MAX);
    khtml_puts(&hr, costStr.c_str());
    khtml_closeelem(&hr, 2);
  }
  khtml_closeelem(&hr, 1); // div
}



void PrintUpgradeCard(struct khtmlreq &hr, const PageToken& pt, const Upgrade& upgrade, float scale) {
  PtL ptl = pt.GetPtL();
  std::string style = "transform: scale(" + std::to_string(scale) + ");";
  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Card", KATTR_STYLE, style.c_str(), KATTR__MAX);

  // define 2 divs - side and main
  std::string dSide;
  std::string dMain;
  // set their classes based on which side they are on
  switch(upgrade.GetSlots()[0]) {
  case UpT::Config:
  case UpT::Hardpoint:
    dSide = "uc_SideCtR";
    dMain = "uc_MainCtL";
    break;
  default:
    dSide = "uc_SideCtL";
    dMain = "uc_MainCtR";
    break;
  }

  { // side div
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, dSide.c_str(), KATTR__MAX);
    { // slots
      for(UpT upt : upgrade.GetSlots()) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_Slot", KATTR_TITLE, UpgradeType::GetUpgradeType(upt).GetName().c_str(), KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(upt).c_str());
	khtml_closeelem(&hr, 1);
      }
    }
    { // restriction
      std::string restrictionStr = upgrade.GetRestriction().GetText();
      if(restrictionStr != "") {
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Rest", KATTR__MAX);
	IconifyText(hr, upgrade.GetRestriction().GetText());
	khtml_closeelem(&hr, 1);
      }
    }
    khtml_closeelem(&hr, 1);
  }
  { // main div
    bool hasStats = upgrade.GetAttackStats().has_value()
                  | upgrade.GetModifier().GetAttackMod().val
                  | upgrade.GetModifier().GetAgility()
                  | upgrade.GetModifier().GetHull()
                  | upgrade.GetModifier().GetShield()
                  | upgrade.GetModifier().GetEnergy()
                  | upgrade.GetCharge().GetCapacity()
                  | upgrade.GetForce().GetCapacity()
                  | upgrade.GetModifier().GetActAdd().size();
    khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, dMain.c_str(), KATTR__MAX);
    { // picture
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Pic", KATTR__MAX);
      // not sure if anything will go here or not...
      khtml_closeelem(&hr, 1);
    }
    { // name
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Name", KATTR__MAX);
      if(upgrade.GetLimited()) {
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "fnt_icon", KATTR__MAX);
	for(int i=0; i<upgrade.GetLimited(); i++) {
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::unique).c_str());
	}
	khtml_closeelem(&hr, 1);
      }
      khtml_puts(&hr, upgrade.GetTitle().c_str());
      khtml_closeelem(&hr, 1);
    }
    { // text
      std::string cl;
      if(hasStats) { cl = "uc_TextCtP"; }
      else         { cl = "uc_TextCtF"; }
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, (cl).c_str(), KATTR__MAX);
      std::vector<std::string> segments = upgrade.GetText().GetMarkedSegments();
      for(const std::string& s : segments) {
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, (upgrade.HasAbility() ? "uc_Text" : "uc_Text uc_TextI"), KATTR__MAX);
	IconifyText(hr, s);
	khtml_closeelem(&hr, 1);
      }
      // if there is a ship ability...
      std::optional<ShipAbility> sa = upgrade.GetShipAbility();
      if(sa) {
	// add an HR
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Text", KATTR__MAX);
	khtml_attr(&hr, KELEM_HR, KATTR__MAX);
	khtml_closeelem(&hr, 1); // div (hr)
	// and print the ship ability
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_Text", KATTR__MAX);
	// with a bold title
	khtml_attr(&hr, KELEM_SPAN, KATTR_STYLE, "font-weight:bold;", KATTR__MAX);
	khtml_puts(&hr, (sa->GetName() + ":").c_str());
	khtml_closeelem(&hr, 1); // span
	khtml_attr(&hr, KELEM_BR, KATTR__MAX);
	// and regular text
	IconifyText(hr, sa->GetText().GetMarkedText());
	khtml_closeelem(&hr, 1); // div
      }
      khtml_closeelem(&hr, 1); // container
    }
    { // cost
      std::string cl = "uc_Cost";
      if(hasStats) { cl += " uc_CostP"; }
      else         { cl += " uc_CostF"; }
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, cl.c_str(), KATTR__MAX);
      std::optional<Cost> cost = upgrade.GetCost(ptl);
      if(cost) {
	switch(cost->GetCsT()) {
	// CONST
	case CsT::Const:
	  khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Cost", KATTR__MAX);
	  khtml_puts(&hr, std::to_string(cost->GetCosts()[0]).c_str());
	  khtml_closeelem(&hr, 1);
	  break;
	// BASE
	case CsT::Base:
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_CostB", KATTR_TITLE, "Small", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::base_small).c_str());
	  khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, std::to_string(cost->GetCosts()[0]).c_str());
	  khtml_puts(&hr, " ");
  	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_CostB", KATTR_TITLE, "Medium", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::base_medium).c_str());
	  khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, std::to_string(cost->GetCosts()[1]).c_str());
	  khtml_puts(&hr, " ");
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_CostB", KATTR_TITLE, "Large", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::base_large).c_str());
	  khtml_closeelem(&hr, 1);
	  khtml_puts(&hr, std::to_string(cost->GetCosts()[2]).c_str());
	  break;
	// AGI
	case CsT::Agi:
	  for(int a=0; a<4; a++) {
	    if(a>0) { khtml_puts(&hr, " "); }
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_CostA", KATTR_TITLE, "Agility", KATTR__MAX);
	    khtml_puts(&hr, std::to_string(a).c_str());
	    khtml_closeelem(&hr, 1);
	    khtml_puts(&hr, std::to_string(cost->GetCosts()[a]).c_str());
	  }
	  break;
	// INIT
	case CsT::Init:
	  for(int i=0; i<7; i++) {
	    if(i>0) { khtml_puts(&hr, " "); }
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_CostI", KATTR_TITLE, "Initiative", KATTR__MAX);
	    khtml_puts(&hr, std::to_string(i).c_str());
	    khtml_closeelem(&hr, 1);
	    khtml_puts(&hr, std::to_string(cost->GetCosts()[i]).c_str());
	  }
	  break;
	}
      }
      else {
	khtml_attr(&hr, KELEM_SPAN, KATTR_TITLE, "Cost", KATTR__MAX);
	khtml_puts(&hr, "???");
	khtml_closeelem(&hr, 1);
      }
      khtml_closeelem(&hr, 1);
    }
    // stats (if applicable)
    if(hasStats) {
      khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_StatsCt", KATTR__MAX);
      khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "uc_StatsTab", KATTR__MAX);
      // attack (secondary)
      if(upgrade.GetAttackStats().has_value()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_AttCt", KATTR__MAX);
	khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "uc_AttTab", KATTR__MAX);
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_AttTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_AttTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttArc", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(upgrade.GetAttackStats()->arc).c_str());
	khtml_closeelem(&hr, 1); // span
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttVal", KATTR__MAX);
	khtml_puts(&hr, std::to_string(upgrade.GetAttackStats()->value).c_str());
	khtml_closeelem(&hr, 1); // span
	khtml_closeelem(&hr, 2); // td tr
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_AttTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_AttTd", KATTR__MAX);
	if(upgrade.GetAttackStats()->ordnance) {
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttOrd", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::rangebonusindicator).c_str());
	  khtml_closeelem(&hr, 1); // span
	}
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttRan", KATTR__MAX);
	std::string rangeStr;
	if(upgrade.GetAttackStats()->minRange == upgrade.GetAttackStats()->maxRange) {
	  rangeStr = std::to_string(upgrade.GetAttackStats()->minRange);
	} else {
	  rangeStr = std::to_string(upgrade.GetAttackStats()->minRange) + "-" + std::to_string(upgrade.GetAttackStats()->maxRange);;
	}
	khtml_puts(&hr, rangeStr.c_str());
	khtml_closeelem(&hr, 1); // span
	khtml_closeelem(&hr, 2); // td tr
	khtml_closeelem(&hr, 1); // table
	khtml_closeelem(&hr, 3); // div, td, tr
      }
      // attack (mod)
      if(upgrade.GetModifier().GetAttackMod().val) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttArc", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(upgrade.GetModifier().GetAttackMod().arc).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_AttVal", KATTR__MAX);
	if(upgrade.GetModifier().GetAttackMod().val > 0) { khtml_puts(&hr, "+"); }
	khtml_puts(&hr, std::to_string(upgrade.GetModifier().GetAttackMod().val).c_str());
	khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 2); // td tr
      }
      // agility
      if(upgrade.GetModifier().GetAgility()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModAgiI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::agility).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModAgiV", KATTR__MAX);
	if(upgrade.GetModifier().GetAgility() > 0) { khtml_puts(&hr, "+"); }
	khtml_puts(&hr, std::to_string(upgrade.GetModifier().GetAgility()).c_str());
	khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 2); // td tr
      }
      // hull
      if(upgrade.GetModifier().GetHull()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModHulI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::hull).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModHulV", KATTR__MAX);
	if(upgrade.GetModifier().GetHull() > 0) { khtml_puts(&hr, "+"); }
	khtml_puts(&hr, std::to_string(upgrade.GetModifier().GetHull()).c_str());
	khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 2); // td tr
      }
      // shield
      if(upgrade.GetModifier().GetShield()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModShiI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::shield).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModShiV", KATTR__MAX);
	if(upgrade.GetModifier().GetShield() > 0) { khtml_puts(&hr, "+"); }
	khtml_puts(&hr, std::to_string(upgrade.GetModifier().GetShield()).c_str());
	khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 2); // td tr
      }
      // energy
      if(upgrade.GetModifier().GetEnergy()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModEneI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::energy).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModEneV", KATTR__MAX);
	if(upgrade.GetModifier().GetEnergy() > 0) { khtml_puts(&hr, "+"); }
	khtml_puts(&hr, std::to_string(upgrade.GetModifier().GetEnergy()).c_str());
	khtml_closeelem(&hr, 1);
	khtml_closeelem(&hr, 2); // td tr
      }
      // charge
      if(upgrade.GetCharge().GetCapacity()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModChaI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::charge).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModChaV", KATTR__MAX);
	khtml_puts(&hr, std::to_string(upgrade.GetCharge().GetCapacity()).c_str());
	khtml_closeelem(&hr, 1);
	if(upgrade.GetCharge().GetRecurring()) {
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModChaI", KATTR__MAX);
	  Ico gly;
	  switch(upgrade.GetCharge().GetRecurring()) {
	  case  1: gly = Ico::recurring; break;
	  case  2: gly = Ico::doublerecurring; break;
	  case  3: gly = Ico::triplerecurring; break;
	  case -1: gly = Ico::negativerecurring; break;
	  default: gly = Ico::recurring; break;
	  }
	  khtml_puts(&hr, IconGlyph::GetGlyph(gly).c_str());
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 2); // td tr
      }
      // force
      if(upgrade.GetForce().GetCapacity()) {
	khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModForI", KATTR__MAX);
	khtml_puts(&hr, IconGlyph::GetGlyph(Ico::forcecharge).c_str());
	khtml_closeelem(&hr, 1);
	khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModForV", KATTR__MAX);
	khtml_puts(&hr, ("+" + std::to_string(upgrade.GetForce().GetCapacity())).c_str());
	khtml_closeelem(&hr, 1);
	if(upgrade.GetForce().GetRecurring()) {
	  khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, "uc_ModForI", KATTR__MAX);
	  khtml_puts(&hr, IconGlyph::GetGlyph(Ico::recurring).c_str());
	  khtml_closeelem(&hr, 1);
	}
	khtml_closeelem(&hr, 2); // td tr
      }
      // actions
      if(upgrade.GetModifier().GetActAdd().size()) {
	for(const std::list<SAct>& sas : upgrade.GetModifier().GetActAdd()) {
	  khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_ActCTd", KATTR__MAX);
	  khtml_attr(&hr, KELEM_DIV, KATTR_CLASS, "uc_ActCt", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TABLE, KATTR_CLASS, "uc_ActTab", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TR, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	  khtml_attr(&hr, KELEM_TD, KATTR_CLASS, "uc_StatsTd", KATTR__MAX);
	  bool next = false;
	  for(SAct sa : sas) {
	    if(next) {
	      khtml_attr(&hr, KELEM_SPAN, KATTR_STYLE, "linked", KATTR_TITLE, "Linked to", KATTR__MAX);
	      khtml_puts(&hr, IconGlyph::GetGlyph(Ico::linked).c_str());
	      khtml_closeelem(&hr, 1);
	    }
	    std::string dif;
	    switch(sa.difficulty) {
	    case Dif::White:  dif = "uc_actionW"; break;
	    case Dif::Red:    dif = "uc_actionR"; break;
	    case Dif::Purple: dif = "uc_actionP"; break;
	    default:          dif = "";        break;
	    }
	    khtml_attr(&hr, KELEM_SPAN, KATTR_CLASS, dif.c_str(), KATTR_TITLE, Action::GetAction(sa.action).GetName().c_str(), KATTR__MAX);
	    khtml_puts(&hr, IconGlyph::GetGlyph(sa.action).c_str());
	    khtml_closeelem(&hr, 1);
	    next = true;
	  }
	  khtml_closeelem(&hr, 6); // td tr table div td tr
	}
      }
      
      khtml_closeelem(&hr, 2); // table, div
    }
    khtml_closeelem(&hr, 1);
  }

  khtml_closeelem(&hr, 1); // div
}



//
// CSS
//

CSS::CSS() {
  // font stuff
  this->ss << "@font-face { font-family:XWingShips; src:url(/fonts/xwing-miniatures-ships.ttf);}"  << std::endl; 
  this->ss << "@font-face { font-family:XWingIcons; src:url(/fonts/xwing-miniatures.ttf); }"       << std::endl;
  this->ss << "@font-face { font-family:XWingNames; src:url(/fonts/SquarishSansCTRegularSC.ttf);}" << std::endl;
  this->ss << "@font-face { font-family:XWingStats; src:url(/fonts/xwstats.ttf);}"                 << std::endl;
  this->ss << ".fnt_icon { font-family:XWingIcons; }" << std::endl;
  this->ss << ".fnt_ship { font-family:XWingShips; }" << std::endl;
  this->ss << ".fnt_stat { font-family:XWingStats; }" << std::endl;
  this->ss << ".bold { font-weight:bold; }" << std::endl;
  this->ss << ".italic { font-style:italic; }" << std::endl;

  // breadcrumb bar (needs to be here due to the a: overrides)
  this->ss << ".topBar      {width:100%; text-align:center; }" << std::endl;
  this->ss << ".breadcrumbs {float:left; color:lime; }" << std::endl;
  this->ss << ".breadcrumbs a:link    { color:green; }" << std::endl;
  this->ss << ".breadcrumbs a:visited { color:green; }" << std::endl;
  this->ss << ".curptl      { color:lime; display:inline-block; margin:0 auto; padding:0; }" << std::endl;
  this->ss << ".curptl a:link    { color:green; }" << std::endl;
  this->ss << ".curptl a:visited { color:green; }" << std::endl;
  this->ss << ".built       { margin:0; padding:0; }" << std::endl;
  this->ss << ".lastbuild   { color:lime; font-size:12px; margin:0px; padding:0px; }" << std::endl;
  this->ss << ".lastbuildbc { color:lime; font-size:12px; float:right; }" << std::endl;

  // overall look
  this->ss << "body  { background-color:black; color:white; }" << std::endl;
  this->ss << "table { background-color:black; border-collapse:collapse; border:1px solid white; color:white; margin:5px; }" << std::endl;
  this->ss << "tr    { background-color:#001100; }" << std::endl;
  this->ss << "th    { background-color:green; color:black; border:1px solid darkgreen; }" << std::endl;
  this->ss << "td    { border: 1px solid darkgreen; }" << std::endl;
  this->ss << "a:link    { color: white; }" << std::endl;
  this->ss << "a:visited { color: grey; }" << std::endl;
  this->ss << ".notice table { background-color:black; border-collapse:collapse; border:1px solid red; color:red; margin:5px; }" << std::endl;
  this->ss << ".notice tr    { background-color:#001100; }" << std::endl;
  this->ss << ".notice th    { background-color:darkred; color:black; border:1px solid darkred; }" << std::endl;
  this->ss << ".notice td    { border: 1px solid darkred; color:red; }" << std::endl;

  // container/floater
  ss << ".container { display:block; width:100%; height:100%; }" << std::endl;
  ss << ".floater   { margin:10px; display: inline-block; vertical-align:top; }" << std::endl;

  // other common things
  this->ss << ".key { color:forestgreen; font-weight:bold; }" << std::endl;
}

static std::string unreleased() {
  std::stringstream ss;  
  ss << ".unr { background-color:#330000; color:#AAAAAA; }" << std::endl;
  return ss.str();
}

static std::string tdShipIcon() {
  std::stringstream ss;
  ss << ".shipIcon { text-align:center; }" << std::endl;
  ss << "td.shipIcon span { font-family:XWingShips; font-size:23px; display:inline-block; padding-top:5px; }" << std::endl;
  return ss.str();
}

static std::string tdFactionIcons() {
  std::stringstream ss;
  ss << ".tdFaction { text-align:center; }" << std::endl;
  for(Fac fac : Faction::GetAllFacs()) {
    Faction faction = Faction::GetFaction(fac);
    ss << ".icon_" << faction.Get3Letter()
       << " { font-family:XWingIcons; font-size:16px; display:inline-block; padding-top:2px; color:#" << Color::GetFacFg(faction.GetFac()).GetHex()
       << "; background-color:#" << Color::GetFacBg(faction.GetFac()).GetHex() << "; }" << std::endl;
  }
  return ss.str();
}

static std::string tdFactionHeaders() {
  std::stringstream ss;
  for(Fac fac : Faction::GetAllFacs()) {
    Faction faction = Faction::GetFaction(fac);
    ss << ".icon_" << faction.Get3Letter()
       << " { font-family:XWingIcons; text-align:center; padding-top:5px; color:#" << Color::GetFacFg(faction.GetFac()).GetHex()
       << "; background-color:#" << Color::GetFacBg(faction.GetFac()).GetHex() << "; }" << std::endl;
  }
  ss << ".headerSize { font-size:100px; }" << std::endl;
  return ss.str();
}

static std::string actionBar() {
  std::stringstream ss;
  ss << ".actionW     { color:#" + Color::GetDif(Dif::White).GetHex() + "; font-family:XWingIcons; font-size:16px; }" << std::endl;
  ss << ".actionR     { color:#" + Color::GetDif(Dif::Red).GetHex() + "; font-family:XWingIcons; font-size:16px; }" << std::endl;
  ss << ".actionP     { color:#" + Color::GetDif(Dif::Purple).GetHex() + "; font-family:XWingIcons; font-size:16px; }" << std::endl;
  ss << ".actionGroup { margin-left:7px; margin-right:7px; }" << std::endl;
  ss << ".linked      { color:#" + Color::GetColor(Clr::Neutral).GetHex() + "; font-family:XWingIcons; font-size:12px; }" << std::endl;
  return ss.str();
}

static std::string upgradeBar() {
  std::stringstream ss;
  ss << ".upgradeBar  { text-align:right; }" << std::endl;
  ss << ".upgrade     { font-family:XWingIcons; font-size:20px; display:inline-block; margin-top:4px; }" << std::endl;
  return ss.str();
}

static std::string titleText() {
  std::stringstream ss;
  ss << ".titleText { border:0px; font-family:XWingNames; font-size:50px; }" << std::endl;
  return ss.str();
}

static std::string shipHeader() {
  std::stringstream ss;
  ss << ".shipHeader { font-family:XWingShips; font-size:60px; text-align:center; }" << std::endl;
  return ss.str();
}


static std::string upgradeHeader() {
  std::stringstream ss;
  ss << ".upgHeader { font-size:60px; text-align:center; padding-top:15px; font-family:XWingIcons; }" << std::endl;
  return ss.str();
}

static std::string indexBar() {
  std::stringstream ss;
  ss << ".indexBar { width:40px; height:40px; }" << std::endl;
  ss << ".indexTbl { table-layout:fixed; }" << std::endl;
  ss << ".indexSpan { display:inline-block; font-size:35px; }" << std::endl;
  ss << "#index a:link    { color:inherit; text-decoration: none; }" << std::endl;
  ss << "#index a:visited { color:inherit; text-decoration: none; }" << std::endl;
  ss << "#index a:hover   { color:inherit; text-decoration: none; }" << std::endl;
  ss << "#index a:active  { color:inherit; text-decoration: none; }" << std::endl;
  return ss.str();
}

  void CSS::MainMenu() {
  this->ss << ".mainmenu { text-align:center; }" << std::endl;
}

void CSS::DamageDeck() {
  this->ss << ".attackArc   { font-family:XWingIcons; }" << std::endl;
}

void CSS::Maneuvers() {
  this->ss << ".mnvDiv   { display:inline-block; background-color:black; border: 1px solid black; }" << std::endl;
  this->ss << ".mnvTab   { border-collapse: collapse; border: 1px solid black; }" << std::endl;
  this->ss << ".mnvSpd   { border:1px solid gray; width:24px; height:22px; padding:0; padding-top:2px; margin:0px; text-align:center; background-color:black; font-family:XWingStats; color:white; }" << std::endl;
  this->ss << ".mnvDif_B { border:1px solid gray; width:24px; height:22px; padding:0; padding-top:2px; margin:0px; text-align:center; background-color:black; font-family:XWingIcons; color:#" + Color::GetDif(Dif::Blue).GetHex() + "; }" << std::endl;
  this->ss << ".mnvDif_W { border:1px solid gray; width:24px; height:22px; padding:0; padding-top:2px; margin:0px; text-align:center; background-color:black; font-family:XWingIcons; color:#" + Color::GetDif(Dif::White).GetHex() + "; }" << std::endl;
  this->ss << ".mnvDif_R { border:1px solid gray; width:24px; height:22px; padding:0; padding-top:2px; margin:0px; text-align:center; background-color:black; font-family:XWingIcons; color:#" + Color::GetDif(Dif::Red).GetHex() + "; }" << std::endl;
  this->ss << ".mnvDif_P { border:1px solid gray; width:24px; height:22px; padding:0; padding-top:2px; margin:0px; text-align:center; background-color:black; font-family:XWingIcons; color:#" + Color::GetDif(Dif::Purple).GetHex() + "; }" << std::endl;
}

void CSS::ShipTitle() {
  this->ss << ".shipHdrIcon { border:0px; font-family:XWingShips; font-size:80px; }" << std::endl;
  this->ss << titleText();
}

void CSS::ShipRow() {
  this->ss << tdFactionIcons();
  this->ss << tdShipIcon();
  this->ss << ".dialId   { text-align:center; font-family:monospace; }" << std::endl;
  this->ss << ".shipName { vertical-align:middle; white-space:nowrap; }" << std::endl;
}

void CSS::PilotRow() {
  this->ss << unreleased();
  this->ss << tdShipIcon();
  this->ss << tdFactionIcons();
  this->ss << ".init        { text-align:right; }" << std::endl;
  this->ss << "td.init span { color:#" + Color::GetColor(Clr::Initiative).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".pilotName   { vertical-align:middle; white-space:nowrap; }" << std::endl;
  this->ss << ".cost        { text-align:right; }" << std::endl;
  this->ss << "td.cost span { font-family:XWingStats; font-size:20px; color:#" + Color::GetColor(Clr::Cost).GetHex() + "; }" << std::endl;
  this->ss << ".statTable   { width:100%; height:100%; margin:0px; border:none; padding:0; }" << std::endl;
  this->ss << ".statTd      { vertical-align:middle; padding-left:3px; padding-right:3px; border:none; }" << std::endl;
  this->ss << ".textAb      { font-weight:bold; }" << std::endl;
  this->ss << ".textFl      { font-style:italic; }" << std::endl;
  this->ss << ".attackStat  { color:#" + Color::GetColor(Clr::Attack).GetHex() + "; float:left; font-size:20px; display:table-cell; }" << std::endl;
  this->ss << ".attackArc   { font-family:XWingIcons; }" << std::endl;
  this->ss << ".attackVal   { font-family:XWingStats; }" << std::endl;
  this->ss << ".agilityStat { color:#" + Color::GetColor(Clr::Agility).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".hullStat    { color:#" + Color::GetColor(Clr::Hull).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".shieldStat  { color:#" + Color::GetColor(Clr::Shield).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".forceStat   { color:#" + Color::GetColor(Clr::Force).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".chargeStat  { color:#" + Color::GetColor(Clr::Charge).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".energyStat  { color:#" + Color::GetColor(Clr::Energy).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".shieldIcon  { color:#" + Color::GetColor(Clr::Shield).GetHex() + "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".forceIcon   { color:#" + Color::GetColor(Clr::Force).GetHex() + "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".chargeIcon  { color:#" + Color::GetColor(Clr::Charge).GetHex() + "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".energyIcon  { color:#" + Color::GetColor(Clr::Energy).GetHex() + "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << actionBar();
  this->ss << upgradeBar();
}

void CSS::UpgradeRow() {
  this->ss << unreleased();
  this->ss << upgradeBar();
  this->ss << ".center        { text-align:center; }" << std::endl;
  this->ss << ".upgCost { font-family:XWingStats; font-size:20px; color:#" + Color::GetColor(Clr::Cost).GetHex() + "; }" << std::endl;

  this->ss << ".agiStat  { color:#" + Color::GetColor(Clr::Agility).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;
  this->ss << ".initStat  { color:#" + Color::GetColor(Clr::Initiative).GetHex() + "; font-family:XWingStats; font-size:20px; }" << std::endl;

  this->ss << ".textAb      { font-weight:bold; }" << std::endl;
  this->ss << ".textFl      { font-style:italic; }" << std::endl;
}

void CSS::Pilots() {
  this->ss << unreleased();
  this->ss << tdFactionHeaders();
  this->ss << shipHeader();
  this->ss << indexBar();
}

void CSS::PilotInfo() {
  this->ss << unreleased();
  this->ss << actionBar();
  this->ss << upgradeBar();
}

void CSS::Upgrades() {
  this->ss << upgradeHeader();
  this->ss << unreleased();
  this->ss << tdFactionIcons();
}

void CSS::UpgradeInfo() {
  this->ss << unreleased();
  this->ss << actionBar();
  this->ss << upgradeBar();
}

  void CSS::Releases() {
  this->ss << unreleased();
}

void CSS::ReleaseInfo() {
  this->ss << titleText();
}

void CSS::QuickBuilds() {
  this->ss << tdFactionHeaders();
  this->ss << shipHeader();
}

void CSS::DialComp() {
  this->Maneuvers();
  this->ss << ".dcIcon { text-align: center; font-family:XWingShips; font-size:80px; } " << std::endl;
  // this is for the first edition maneuver chart generator
  this->ss << ".div_Maneuvers    { display:inline-block; background-color:black; border: 1px solid black; }" << std::endl;
  this->ss << ".tab_Maneuvers    { border-collapse: collapse; border: 1px solid black; }" << std::endl;
  this->ss << ".tab_Maneuvers td { border: 1px solid gray; width: 24px; height: 22px; padding: 0; padding-top: 2px; margin: 0px; text-align: center; }" << std::endl;
  this->ss << ".td_Maneuvers     { background-color:black; border: 1px solid black; }" << std::endl;
  this->ss << ".td_ManeuverSpeed { background-color:black; font-family:XWingStats; color:white; }" << std::endl;
  this->ss << ".td_ManeuverRed   { background-color:black; font-family:XWingIcons; color:red;   }" << std::endl;
  this->ss << ".td_ManeuverWhite { background-color:black; font-family:XWingIcons; color:white; }" << std::endl;
  this->ss << ".td_ManeuverGreen { background-color:black; font-family:XWingIcons; color:green; }" << std::endl;
}

void CSS::Search() {
  this->ss << ".sym { text-decoration:none; }" << std::endl;
  this->ss << ".typeHdr { color:darkgreen; font-size:24px; text-decoration:underline; margin-bottom:0; }" << std::endl;
}

void CSS::Costs() {
  this->ss << ".noWrap { white-space:nowrap; }" << std::endl;
  this->ss << ".pad  { width:1px; }" << std::endl;
  this->ss << ".vpad { width:1px; height:1px; }" << std::endl;
  this->ss << shipHeader();
  this->ss << ".wentDown { color:#9F9FFF; }" << std::endl;
  this->ss << ".wentUp   { color:#FF5F5F; }" << std::endl;
  this->ss << upgradeBar();
  this->ss << upgradeHeader();
}

void CSS::Inventory() {
  this->ss << ".invCount { font-weight: bold; padding-left: 5px; padding-right: 5px; font-size: 20px; color: green; }" << std::endl;
}

void CSS::Ai() {
  this->ss << ".aiAction { font-size:20px; font-family:XWingIcons; }" << std::endl;
}

std::string CSS::GetCSS() { return this->ss.str(); }

void CSS::PilotCard() {
  this->ss << ".cardcontainer { display: inline-block; }" << std::endl;
  this->ss << ".cardimage { text-align: center; }" << std::endl;
  this->ss << ".keyword { background:#333300; color:#BBBB00; border: 1px solid #BBBB00; padding:1px; margin-right:5px; }" << std::endl;
  this->ss << ".pc_CardReb   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Rebel).GetHex()      << "; " << "background:#" << Color::GetFacBg(Fac::Rebel).GetHex()      << "; }" << std::endl;
  this->ss << ".pc_CardImp   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Imperial).GetHex()   << "; " << "background:#" << Color::GetFacBg(Fac::Imperial).GetHex()   << "; }" << std::endl;
  this->ss << ".pc_CardScu   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Scum).GetHex()       << "; " << "background:#" << Color::GetFacBg(Fac::Scum).GetHex()       << "; }" << std::endl;
  this->ss << ".pc_CardRes   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Resistance).GetHex() << "; " << "background:#" << Color::GetFacBg(Fac::Resistance).GetHex() << "; }" << std::endl;
  this->ss << ".pc_CardFir   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::FirstOrder).GetHex() << "; " << "background:#" << Color::GetFacBg(Fac::FirstOrder).GetHex() << "; }" << std::endl;
  this->ss << ".pc_CardRep   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Republic).GetHex()   << "; " << "background:#" << Color::GetFacBg(Fac::Republic).GetHex()   << "; }" << std::endl;
  this->ss << ".pc_CardSep   { position:relative; display:inline-block; width:325px; height:450px; color:#" << Color::GetFacFg(Fac::Separatist).GetHex() << "; " << "background:#" << Color::GetFacBg(Fac::Separatist).GetHex() << "; }" << std::endl;

  this->ss << ".pc_Artwork   { position:absolute; display:flex;         width:323px; height:148px; top:  1px; left:  1px; color:#FFFFFF; background:#000000; align-items:center; justify-content:center; }" << std::endl;
  this->ss << ".pc_Init      { position:absolute; display:inline-block; width: 35px; height: 40px; top:150px; left:  1px; color:#" << Color::GetColor(Clr::Initiative).GetHex() << "; background:#000000; text-align:center; line-height: 44px; font-family:XWingStats; font-size:32px; }" << std::endl;
  this->ss << ".pc_Name      { position:absolute; display:inline-block; width:246px; height: 22px; top:150px; left: 37px; color:#FFFFFF; background:#000000; text-align:center; line-height: 25px; font-family:XWingNames; font-size:14px; }" << std::endl;
  this->ss << ".pc_Subtitle  { position:absolute; display:inline-block; width:246px; height: 17px; top:173px; left: 37px; color:#FFFFFF; background:#000000; text-align:center; line-height: 18px;                         font-size:12px; font-style:italic; }" << std::endl;
  this->ss << ".pc_Faction   { position:absolute; display:inline-block; width: 40px; height: 40px; top:150px; left:284px;                                    text-align:center; line-height:40px; font-family:XWingIcons; font-size:40px; }" << std::endl;
  this->ss << ".pc_TextCt    { position:absolute; display:table;        width:244px; height:144px; top:191px; left:  1px; padding:3px; background:#ffede4; justify-content:center; align-items:center; }" << std::endl;
  this->ss << ".pc_Text," << std::endl;
  this->ss << ".pc_FText     { display:table-row; width:100%;                                              text-align:center; font-size:12px; color:#000000; }" << std::endl;
  this->ss << ".pc_FText     { font-style:italic; }" << std::endl;

  this->ss << ".pc_Actions   { position:absolute; display:inline-block; width: 72px; height:217px; top:191px; left:252px; background:#000000; text-align:center; font-family:XWingIcons; font-size:16px; }" << std::endl;
  this->ss << ".pc_ActionsTab{ width:100%; height:100%; margin:0; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsReb{ border:1px solid #" << Color::GetFacFg(Fac::Rebel).GetHex()      << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsImp{ border:1px solid #" << Color::GetFacFg(Fac::Imperial).GetHex()   << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsScu{ border:1px solid #" << Color::GetFacFg(Fac::Scum).GetHex()       << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsRes{ border:1px solid #" << Color::GetFacFg(Fac::Resistance).GetHex() << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsFir{ border:1px solid #" << Color::GetFacFg(Fac::FirstOrder).GetHex() << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsRep{ border:1px solid #" << Color::GetFacFg(Fac::Republic).GetHex()   << "; background:#000000; }" << std::endl;
  this->ss << ".pc_ActionsSep{ border:1px solid #" << Color::GetFacFg(Fac::Separatist).GetHex() << "; background:#000000; }" << std::endl;
  this->ss << ".pc_actionW   { color:#" << Color::GetDif(Dif::White).GetHex()  << "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".pc_actionR   { color:#" << Color::GetDif(Dif::Red).GetHex()    << "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".pc_actionP   { color:#" << Color::GetDif(Dif::Purple).GetHex() << "; font-family:XWingIcons; font-size:20px; }" << std::endl;
  this->ss << ".pc_actionGrp { margin-left:7px; margin-right:7px; }" << std::endl;
  this->ss << ".pc_linked    { color:#" << Color::GetColor(Clr::Neutral).GetHex() + "; font-family:XWingIcons; font-size:12px; }" << std::endl;

  this->ss << ".pc_Stats     { position:absolute; display:inline-block;         width:250px; height:66px; top:342px; left:  1px; background:#231f20; text-align:center; line-height: 20px;                         font-size:24px; }" << std::endl;
  this->ss << ".pc_StatsTab  { width:100%; height:100%; margin:0; background:#000000; border:0px; }" << std::endl;
  this->ss << ".pc_StatsTD   { background:#000000; border:0px; }" << std::endl;
  this->ss << ".pc_StatAtkI  { border:0px; color:#" << Color::GetColor(Clr::Attack).GetHex()  << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatAtkV  { border:0px; color:#" << Color::GetColor(Clr::Attack).GetHex()  << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatAgiI  { border:0px; color:#" << Color::GetColor(Clr::Agility).GetHex() << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatAgiV  { border:0px; color:#" << Color::GetColor(Clr::Agility).GetHex() << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatHulI  { border:0px; color:#" << Color::GetColor(Clr::Hull).GetHex()    << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatHulV  { border:0px; color:#" << Color::GetColor(Clr::Hull).GetHex()    << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatShiI  { border:0px; color:#" << Color::GetColor(Clr::Shield).GetHex()  << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatShiV  { border:0px; color:#" << Color::GetColor(Clr::Shield).GetHex()  << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatForI  { border:0px; color:#" << Color::GetColor(Clr::Force).GetHex()   << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatForV  { border:0px; color:#" << Color::GetColor(Clr::Force).GetHex()   << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatChaI  { border:0px; color:#" << Color::GetColor(Clr::Charge).GetHex()  << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatChaV  { border:0px; color:#" << Color::GetColor(Clr::Charge).GetHex()  << "; font-family:XWingStats; }" << std::endl;
  this->ss << ".pc_StatEneI  { border:0px; color:#" << Color::GetColor(Clr::Energy).GetHex()  << "; font-family:XWingIcons; }" << std::endl;
  this->ss << ".pc_StatEneV  { border:0px; color:#" << Color::GetColor(Clr::Energy).GetHex()  << "; font-family:XWingStats; }" << std::endl;

  this->ss << ".pc_ShipIcon  { position:absolute; display:inline-block; width: 40px; height: 40px; top:409px; left:  1px; color:#FFFFFF; background:#000000; text-align:center; line-height: 45px; font-family:XWingShips; font-size:40px; }" << std::endl;
  this->ss << ".pc_ShipName  { position:absolute; display:inline-block; width:221px; height: 18px; top:409px; left: 42px; color:#FFFFFF; background:#000000; text-align:center; line-height: 20px; font-family:XWingNames; font-size:10px; }" << std::endl;
  this->ss << ".pc_Upgrades  { position:absolute; display:inline-block; width:221px; height: 21px; top:428px; left: 42px; color:#FFFFFF; background:#000000; text-align:center; line-height: 22px; font-family:XWingIcons; font-size:18px; }" << std::endl;
  this->ss << ".pc_Cost      { position:absolute; display:inline-block; width: 60px; height: 40px; top:409px; left:264px; color:#" << Color::GetColor(Clr::Cost).GetHex() << "; background:#000000; text-align:center; line-height: 45px; font-family:XWingStats; font-size:32px; }" << std::endl;
}



void CSS::UpgradeCard() {
  this->ss << ".cardcontainer { display: inline-block; }" << std::endl;
  this->ss << ".cardimage { text-align: center; }" << std::endl;
  this->ss << ".uc_Card    { position:relative; display:inline-block; width:450px; height:325px; color:#FFFFFF; background:#444444; }" << std::endl;
  this->ss << ".uc_SideCtL,.uc_SideCtR { position:absolute; display:inline-block; width:150px; height:325px; color:#FFFFFF; background:#444444; text-align:center; }" << std::endl;
  this->ss << ".uc_SideCtL { left:  0px; }" << std::endl;
  this->ss << ".uc_SideCtR { left:300px; }" << std::endl;
  this->ss << ".uc_Slot    { position:relative; display:inline-block; color:#FFFFFF; font-family:XWingIcons; font-size:96px; }" << std::endl;
  this->ss << ".uc_Rest    { position:absolute; display:inline-block; color:#000000; background:#FFFFFF; top:264px; left:25px; width:100px; height:60px; font-size:12px; }" << std::endl;

  this->ss << ".uc_MainCtR,.uc_MainCtL { position:relative; display:inline-block; top:1px; width:290px; height:323px; color:#FFFFFF; background:#000000; text-align:center; }" << std::endl;
  this->ss << ".uc_MainCtR             { left:150px; }" << std::endl;
  this->ss << ".uc_MainCtL             { left: 10px; }" << std::endl;

  this->ss << ".uc_Pic  { position:absolute; display:inline-block; color:#000000; background:#222222; top:  0px; left:0px; width:290px; height:125px; }" << std::endl;
  this->ss << ".uc_Name { position:absolute; display:inline-block; color:#FFFFFF; background:#000000; top:125px; left:0px; width:290px; height: 25px; font-family:XWingNames; line-height:25px; font-size:12px;}" << std::endl;
  this->ss << ".uc_TextCtF { position:absolute; display:table; width:282px; height:134px; top:151px; left:  1px; padding:3px; background:#ffede4; justify-content:center; align-items:center; }" << std::endl;
  this->ss << ".uc_TextCtP { position:absolute; display:table; width:204px; height:134px; top:151px; left:  1px; padding:3px; background:#ffede4; justify-content:center; align-items:center; }" << std::endl;
  this->ss << ".uc_Text    { display:table-row; width:100%; text_align:center; font-size:12px; color:#000000; }" << std::endl;
  this->ss << ".uc_TextI   { font-style:italic; }" << std::endl;
  this->ss << ".uc_Cost  { position:absolute; display:inline-block; color:#" << Color::GetColor(Clr::Cost).GetHex() << "; height: 30px; top:292px; left:1px; text-align:center; font-family:XWingStats; line-height:35px; font-size:20px; }" << std::endl;
  this->ss << ".uc_CostF { width:288px; }" << std::endl;
  this->ss << ".uc_CostP { width:210px; }" << std::endl;
  this->ss << ".uc_CostB { font-family:XWingIcons; color:#FFFFFF; }" << std::endl;
  this->ss << ".uc_CostA { color:#"<< Color::GetColor(Clr::Agility).GetHex() <<"; }" << std::endl;
  this->ss << ".uc_CostI { color:#"<< Color::GetColor(Clr::Initiative).GetHex() <<"; }" << std::endl;
    
  this->ss << ".uc_StatsCt  { position:absolute; display:inline-block; color:#000000; background:#000000; top:151px; left:212px; width:77px; height:171px; }" << std::endl;
  this->ss << ".uc_StatsTab { width:100%; height:100%; margin:0; background:#000000; border:0px black;}" << std::endl;
  this->ss << ".uc_StatsTd { background-color:#000000; border:0px black; vertical-align:top; }" << std::endl;

  this->ss << ".uc_AttTab { width:100%; height:100%; margin:0; background:#000000; border-bottom: 1px solid #"<< Color::GetColor(Clr::Attack).GetHex() <<"; border-left: 1px dotted #"<< Color::GetColor(Clr::Attack).GetHex() <<"; border-right: 1px dotted #"<< Color::GetColor(Clr::Attack).GetHex() <<"; border-top: 1px solid black; }" << std::endl;
  this->ss << ".uc_AttCt  { display:inline-block; color:#000000; background:#330000; top:0px; left:6px; width:65px; height:60px; }" << std::endl;
  this->ss << ".uc_AttTd  { background-color:#000000; border:1px red; vertical-align:top; }" << std::endl;

  this->ss << ".uc_AttArc { color:#" << Color::GetColor(Clr::Attack).GetHex()  << "; padding:5px; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_AttVal { color:#" << Color::GetColor(Clr::Attack).GetHex()  << "; padding:5px; font-family:XWingStats; font-size:24px; }" << std::endl;
  this->ss << ".uc_AttOrd { color:#" << Color::GetColor(Clr::Attack).GetHex()  << "; padding:2px; font-family:XWingIcons; font-size:14px; }" << std::endl;
  this->ss << ".uc_AttRan { color:#" << Color::GetColor(Clr::Neutral).GetHex() << "; padding:2px; font-family:XWingStats; font-size:18px; }" << std::endl;
  this->ss << ".uc_ModAgiI { color:#" << Color::GetColor(Clr::Agility).GetHex() << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModAgiV { color:#" << Color::GetColor(Clr::Agility).GetHex() << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;
  this->ss << ".uc_ModHulI { color:#" << Color::GetColor(Clr::Hull).GetHex()    << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModHulV { color:#" << Color::GetColor(Clr::Hull).GetHex()    << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;
  this->ss << ".uc_ModShiI { color:#" << Color::GetColor(Clr::Shield).GetHex()  << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModShiV { color:#" << Color::GetColor(Clr::Shield).GetHex()  << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;
  this->ss << ".uc_ModEneI { color:#" << Color::GetColor(Clr::Energy).GetHex()  << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModEneV { color:#" << Color::GetColor(Clr::Energy).GetHex()  << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;

  this->ss << ".uc_ActCTd { background-color:#000000; border:0px black; text-align:right; vertical-align:middle; }" << std::endl;
  this->ss << ".uc_ActCt  { display:inline-block; text-align:center; color:#000000; background:#330000; top:0px; left:6px; width:65px; height:30px; font-family:XWingIcons; font-size:16px; }" << std::endl;
  this->ss << ".uc_ActTab { width:100%; height:100%; margin:0; background:#000000; border: 1px solid #FFFFFF; border-right: 1px dotted black; }" << std::endl;
  this->ss << ".uc_actionW   { color:#" << Color::GetDif(Dif::White).GetHex()  << "; font-family:XWingIcons; line-height:25px; font-size:16px; }" << std::endl;
  this->ss << ".uc_actionR   { color:#" << Color::GetDif(Dif::Red).GetHex()    << "; font-family:XWingIcons; line-height:25px; font-size:16px; }" << std::endl;
  this->ss << ".uc_actionP   { color:#" << Color::GetDif(Dif::Purple).GetHex() << "; font-family:XWingIcons; line-height:25px; font-size:16px; }" << std::endl;
  
  this->ss << ".uc_ModChaI { color:#" << Color::GetColor(Clr::Charge).GetHex() << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModChaV { color:#" << Color::GetColor(Clr::Charge).GetHex() << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;
  this->ss << ".uc_ModForI { color:#" << Color::GetColor(Clr::Force).GetHex()  << "; font-family:XWingIcons; font-size:24px; }" << std::endl;
  this->ss << ".uc_ModForV { color:#" << Color::GetColor(Clr::Force).GetHex()  << "; font-family:XWingStats; font-size:24px; padding-left:10px; }" << std::endl;
  this->ss << ".uc_effect  { width:440px; border:1px solid darkgreen; padding:5px; color:gray; }" << std::endl;
}
  
}
